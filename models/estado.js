/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var Estado =  sequelize.define('estado', {
    ID: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    acronimo: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
        Estado.hasMany(models.cidade); 
      }
      },
    paranoid: true,  
    tableName: 'Estado'
  });
  return Estado;
};