/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var Device =  sequelize.define('device', {
    idDevice: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    os :{
        type: Sequelize.INTEGER,
        allowNull : false
    },
    uuid:{
        type: Sequelize.STRING,
        allowNull : false
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
         Device.belongsTo(models.usuario);
      },insert: function (parameters, callback) {
          Device
            .build(parameters)
            .save()
            .then(function (device) {
              return callback(device,null);
            }).catch(function (error) {
              return callback(null,error);
            });
        },
        get: function (parameters, callback) {
          Device.findOne(parameters)
            .then(function(device){
              return callback(device,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        updateData: function (whereParameter,parameters, callback) {
          Device.update(parameters, { where: whereParameter } ).then(function(updatedDevice){
             return callback(updatedDevice, null);
          }).catch(function(error){
             return callback(null,error);
          });
        }
      },
    paranoid: true,  
    tableName: 'Device'
  });
  return Device;
};