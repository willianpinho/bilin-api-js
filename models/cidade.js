/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var Cidade =  sequelize.define('cidade', {
    ID: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
          Cidade.belongsTo(models.estado);
          Cidade.hasOne(models.produto);
      }
      },
    paranoid: true, 
    tableName: 'Cidade'
  });
  return Cidade;
};