/* jshint indent: 2 */
"use strict";
var bcrypt = require('bcryptjs');

//Model que descreve uma distribuidora

module.exports = function(sequelize, Sequelize) {
  var Distribuidora =  sequelize.define('distribuidora', {
    idDistribuidora: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    senha: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    cnpj: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate : {
        len: [12, 17],
        //is : /^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/
      }
    },
    imagemUrl: {
      type: Sequelize.STRING,
      allowNull: true
    },
     role: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: "dist"
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false
    }
  }, {
    instanceMethods: {
        validatePassword: function (pass) {
          return bcrypt.compareSync(pass, this.senha);
        }
      },
      classMethods: {
        associate : function (models){  
        Distribuidora.hasMany(models.venda); 
        Distribuidora.hasMany(models.pedido);
        Distribuidora.hasMany(models.telefone);
        Distribuidora.hasMany(models.entregador); 
        Distribuidora.hasMany(models.produto); 
        Distribuidora.hasOne(models.endereco);
      }, 
      generateHash: function (passwd) {
          return bcrypt.hashSync(passwd, bcrypt.genSaltSync(8), null);
      },
      insert: function (parameters, callback) {
          Distribuidora
            .build(parameters)
            .save()
            .then(function (distribuidora) {
              delete distribuidora.dataValues["senha"];
              return callback(distribuidora,null);
            }).catch(function (error) {
              return callback(null,error);
            });
        },
        get: function (parameters, callback) {
          Distribuidora.findOne(parameters)
            .then(function(distribuidora){
              return callback(distribuidora,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        getAll: function (parameters, callback) {
          Distribuidora.findAll(parameters)
            .then(function(distribuidora){
              return callback(distribuidora,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        updateData: function (whereParameter,parameters, callback) {
          Distribuidora.update(parameters,{where: whereParameter}).then(function(updatedDistribuidora){
             return callback(updatedDistribuidora,null);
          }).catch(function(error){
             return callback(null,error);
          });
        }
      },
       hooks: {
        beforeCreate: function (user, options) {
          user.senha = bcrypt.hashSync(user.senha, bcrypt.genSaltSync(8), null);
        }
      },
    paranoid: true,  
    tableName: 'Distribuidora'
  });
  return Distribuidora;
};
