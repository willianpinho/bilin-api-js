/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var Promocao =  sequelize.define('promocao', {
    idPromocao: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    codigo: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    dataInicio: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    dataFim: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    valor:{
      type: Sequelize.FLOAT,
      allowNull: true,
      isFloat: true
    },
    percentual:{
      type: Sequelize.FLOAT,
      allowNull: true,
      isFloat: true
    },
    count:{
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue : 0
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
        Promocao.belongsToMany(models.usuario, {through: 'usuarioPromo'});
      }
      },
    paranoid: true,  
    tableName: 'Promocao'
  });
  return Promocao;
};
