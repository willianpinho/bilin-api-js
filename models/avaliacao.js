/* jshint indent: 2 */

module.exports = function (sequelize, Sequelize) {
  var Avaliacao = sequelize.define('avaliacao', {
    idAvaliacao: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    avaliacao: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    from: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    estrelas: {
      type: Sequelize.FLOAT,
      allowNull: false
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate: function (models) {
          Avaliacao.belongsTo(models.usuario);
          Avaliacao.belongsTo(models.entregador);
          Avaliacao.belongsTo(models.pedido);
        }, insert: function (parameters, callback) {
          Avaliacao
            .build(parameters)
            .save()
            .then(function (avaliacao) {
              return callback(avaliacao, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        get: function (parameters, callback) {
          Avaliacao.findOne(parameters)
            .then(function (avaliacao) {
              return callback(avaliacao, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        getOrCreate: function (whereParameter,parameters, callback) {
          Avaliacao.findOrCreate({ where: whereParameter, defaults : parameters})
            .then(function (avaliacao) {
              return callback(avaliacao, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        }, getAll: function (parameters, callback) {
          Avaliacao.findAll(parameters)
            .then(function (avaliacao) {
              return callback(avaliacao, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        updateData: function (whereParameter, parameters, callback) {
          Avaliacao.update(parameters, { where: whereParameter }).then(function (updatedavaliacao) {
            return callback(updatedavaliacao, null);
          }).catch(function (error) {
            return callback(null, error);
          });
        }
      },
      paranoid: true,
      tableName: 'Avaliacao'
    });
  return Avaliacao;
};
