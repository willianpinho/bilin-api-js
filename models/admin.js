/* jshint indent: 2 */

var bcrypt = require('bcryptjs');

module.exports = function (sequelize, Sequelize) {
  var Admin = sequelize.define('admin', {
    idAdmin: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    senha: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [4, 25]
      },
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: true,
      validate: {
        is: ["^[a-zA-Zà-úÀ-Ú ]+$"]
      }
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    role: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: "admin"
    },
    resetPasswordToken: {
      type: Sequelize.STRING,
      allowNull: true
    },
    resetPasswordExpires: {
      type: Sequelize.DATE,
      allowNull: true
    }
  }, {
      instanceMethods: {
        validatePassword: function (pass) {
          return bcrypt.compareSync(pass, this.senha);
        }
      },
      classMethods: {
        associate: function (models) {
        },
        generateHash: function (passwd) {
          return bcrypt.hashSync(passwd, bcrypt.genSaltSync(8), null);
        },
        get: function (parameters, callback) {
          Admin.findOne(parameters)
            .then(function (admin) {
              return callback(admin, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        }
      },
      hooks: {
        beforeCreate: function (user, options) {
          user.senha = bcrypt.hashSync(user.senha, bcrypt.genSaltSync(8), null);
        }
      },
      paranoid: true,
      tableName: 'Admin'
    });
  return Admin;
};
