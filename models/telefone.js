/* jshint indent: 2 */
module.exports = function (sequelize, Sequelize) {
    var Telefone = sequelize.define('telefone', {
        idTelefone: {
            type: Sequelize.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        descricao: {
            type: Sequelize.STRING,
            allowNull: true
        },
        numero: {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                len: [10, 20],
                is: /^1\d\d(\d\d)?$|^0800 ?\d{3} ?\d{4}$|^(\(0?([1-9a-zA-Z][0-9a-zA-Z])?[1-9]\d\) ?|0?([1-9a-zA-Z][0-9a-zA-Z])?[1-9]\d[ .-]?)?(9|9[ .-])?[2-9]\d{3}[ .-]?\d{4}$/gm
            }
        },
        ativo: {
            type: Sequelize.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
    }, {
            classMethods: {
                associate: function (models) {
                    Telefone.belongsTo(models.entregador);
                    Telefone.belongsTo(models.distribuidora);
                },
                insert: function (parameters, callback) {
                    Telefone
                        .build(parameters)
                        .save()
                        .then(function (number) {
                            return callback(number, null);
                        }).catch(function (error) {
                            return callback(null, error);
                        });
                },
                get: function (parameters, callback) {
                    Telefone.findOne(parameters)
                        .then(function (number) {
                            return callback(number, null);
                        }).catch(function (error) {
                            return callback(null, error);
                        });
                },
                getAll: function (parameters, callback) {
                    Telefone.findAll(parameters)
                        .then(function (number) {
                            return callback(number, null);
                        }).catch(function (error) {
                            return callback(null, error);
                        });
                },
                updateData: function (whereParameter, parameters, callback) {
                    Telefone.update(parameters, { where: whereParameter }).then(function (updatedNumber) {
                        return callback(updatedNumber, null);
                    }).catch(function (error) {
                        return callback(null, error);
                    });
                }
            },
            paranoid: true,
            tableName: 'Telefone'
        });
    return Telefone;
};
