/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
    var Pedido = sequelize.define('pedido', {
        idPedido: {
            type: Sequelize.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        quantidade: {
            type: Sequelize.INTEGER,
            allowNull: false,
            isInt: true
        },
        data: {
            type: Sequelize.DATE,
            allowNull: false
        },
        tipoPagamento: {
            type: Sequelize.INTEGER,
            allowNull: false,
            isInt: true
        },
        statusPagamento: {
            type: Sequelize.INTEGER,
            allowNull: false,
            isInt: true
        },
        valorPagamento: {
            type: Sequelize.FLOAT,
            allowNull: true
        },
        troco: {
            type: Sequelize.FLOAT,
            allowNull: true,
            isFloat: true
        },
        statusPedidoUsuario: {
            type: Sequelize.INTEGER,
            allowNull: false,
            isInt: true
        },
        statusPedidoEntregador: {
            type: Sequelize.INTEGER,
            allowNull: false,
            isInt: true
        },
        tipoCartao: {
            type: Sequelize.INTEGER,
            allowNull: true,
            isInt: true
        },
        observacaoUsuario: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        orderSellId: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        orderSellHash: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        observacaoEntregador: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        valorPedido: {
            type: Sequelize.FLOAT,
            allowNull: false,
            isFloat: true
        },
        valorDesconto: {
            type: Sequelize.FLOAT,
            allowNull: true,
            defaultValue: 0,
            isFloat: true
        },
        valorFinal: {
            type: Sequelize.FLOAT,
            allowNull: false,
            isFloat: true
        },
        usoCredito: {
            type: Sequelize.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
        pesarGas: {
            type: Sequelize.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
        instalarGas: {
            type: Sequelize.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        promoCode: {
            type: Sequelize.STRING,
            allowNull: true
        },
        valorCredito: {
            type: Sequelize.FLOAT,
            allowNull: true,
            isFloat: true
        },
        ativo: {
            type: Sequelize.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },avaliadoUser: {
            type: Sequelize.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },avaliadoEntregador: {
            type: Sequelize.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
        entregadorIdEntregadorListaTentativas:{
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {
            classMethods: {
                associate: function(models) {
                    Pedido.belongsTo(models.usuario);
                    Pedido.belongsTo(models.entregador);
                    Pedido.belongsTo(models.distribuidora);
                    Pedido.belongsTo(models.endereco);
                }, insert: function(parameters, callback) {
                    Pedido
                        .build(parameters)
                        .save()
                        .then(function(pedido) {
                          return callback(pedido, null);
                        }).catch(function(error) {
                            return callback(null, error);
                        });
                },
                get: function(parameters, callback) {
                    Pedido.findOne(parameters)
                        .then(function(pedido) {
                            return callback(pedido, null);
                        }).catch(function(error) {
                            return callback(null, error);
                        });
                }, getAll: function(parameters, callback) {
                    Pedido.findAll(parameters)
                        .then(function(pedido) {
                            return callback(pedido, null);
                        }).catch(function(error) {
                            return callback(null, error);
                        });
                },
                updateData: function(whereParameter, parameters, callback) {
                    Pedido.update(parameters, { where: whereParameter }).then(function(updatedPedido) {
                        return callback(updatedPedido, null);
                    }).catch(function(error) {
                        return callback(null, error);
                    });
                },
                cancelAndDestroy : function(idPedidoDestroy,callback){
                    Pedido.destroy({where : {idPedido : idPedidoDestroy }})
                    .then(function(destroyedPedido){
                        return callback(destroyedPedido, null);
                    }).catch(function(error) {
                        return callback(null, error);
                    });
                }
            },
            paranoid: true,
            tableName: 'Pedido'
        });
    return Pedido;
};
