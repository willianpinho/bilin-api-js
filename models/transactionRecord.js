/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var TransactionRecord =  sequelize.define('transactionRecord', {
    idTransaction: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    record: {
      type: Sequelize.JSON,
      allowNull: true,
      validate: {
        notEmpty : true,
      },
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
        TransactionRecord.belongsTo(models.pedido);
        TransactionRecord.belongsTo(models.usuario);
      },insert: function (parameters, callback) {
          TransactionRecord
            .build(parameters)
            .save()
            .then(function (transaction) {
              return callback(transaction, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        get: function (parameters, callback) {
          TransactionRecord.findOne(parameters)
            .then(function (transaction) {
              return callback(transaction, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        }, getAll: function (parameters, callback) {
          TransactionRecord.findAll(parameters)
            .then(function (transaction) {
              return callback(transaction, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        updateData: function (whereParameter, parameters, callback) {
          TransactionRecord.update(parameters, { where: whereParameter }).then(function (updatedTransaction) {
            return callback(updatedTransaction, null);
          }).catch(function (error) {
            return callback(null, error);
          });
        }
      },
    paranoid: true,  
    tableName: 'TransactionRecord'
  });
  return TransactionRecord;
};
