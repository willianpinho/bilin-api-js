/* jshint indent: 2 */

module.exports = function (sequelize, Sequelize) {
  var Endereco = sequelize.define('endereco', {
    idEndereco: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    titulo: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    endereco: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    numero: {
      type: Sequelize.STRING,
      allowNull: true,
       validate: {
        isNumeric: true
      }
    },
    complemento: {
      type: Sequelize.STRING,
      allowNull: true
    },
    bairro: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    cep: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        is: /^[0-9]{5}-[0-9]{3}$/
      },
    },
    dono: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    referencia: {
      type: Sequelize.STRING,
      allowNull: true
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate: function (models) {
          Endereco.belongsTo(models.cidade);
          Endereco.belongsTo(models.usuario);
        },
         insert: function (parameters, callback) {
          Endereco
            .build(parameters)
            .save()
            .then(function (endereco) {
              return callback(endereco,null);
            }).catch(function (error) {
              return callback(null,error);
            });
        },
        get: function (parameters, callback) {
          Endereco.findOne(parameters)
            .then(function(endereco){
              return callback(endereco,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },getAll: function (parameters, callback) {
          Endereco.findAll(parameters)
            .then(function(endereco){
              return callback(endereco,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        updateData: function (whereParameter,parameters, callback) {
          Endereco.update(parameters,{where: whereParameter}).then(function(updatedendereco){
             return callback(updatedendereco,null);
          }).catch(function(error){
             return callback(null,error);
          });
        }
      },
      paranoid: true,
      tableName: 'Endereco'
    });
  return Endereco;
};