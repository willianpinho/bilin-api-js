/* jshint indent: 2 */

module.exports = function (sequelize, Sequelize) {
  var EntregadorData = sequelize.define('entregadorData', {
    idEntregadorData: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    point: {
      type: Sequelize.GEOMETRY('POINT'),
      allowNull: true
    },
    botijoes: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    disponivel: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      instanceMethods: {

      },
      classMethods: {
        associate: function (models) {
          EntregadorData.belongsTo(models.entregador);
        },
        insert: function (parameters, callback) {
          EntregadorData
            .build(parameters)
            .save()
            .then(function (data) {
              return callback(data, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        get: function (parameters, callback) {
          EntregadorData.findOne(parameters)
            .then(function (data) {
              return callback(data, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        updateData: function (whereParameter, parameters, callback) {
          EntregadorData.update(parameters, { where: whereParameter }).then(function (data) {
            console.log(data);
            return callback(data, null);
          }).catch(function (error) {
            return callback(null, error);
          });
        },
        getAround: function (latitude, longitude, radius, callback) {
          //Query 
          const query =
            'SELECT "idEntregadorData", "botijoes", "entregadorIdEntregador", ST_Distance_Sphere(ST_MakePoint(:latitude, :longitude), "point") AS distance FROM public."EntregadorData" WHERE ST_DWithin(Geography(point),Geography(ST_GeographyFromText(\'SRID=4326;POINT('+latitude+' '+longitude+')\'  )),20000) AND disponivel = true ORDER BY point DESC';

          //Realizar a query
          sequelize.query(query, {
            replacements: {
              latitude: parseFloat(latitude),
              longitude: parseFloat(longitude),
              maxDistance: radius * 1000 //A unidade do radius é em KM
            },
            type: sequelize.QueryTypes.SELECT
          })
            .then(function (result) {
              callback(result, null);
            }).catch(function (error) {
              callback(null, error);
            })
        },

        getAroundNotIn: function (latitude, longitude, radius,notIn, callback) {
            //Query
            const query =
                'SELECT "idEntregadorData", "botijoes", "entregadorIdEntregador", ST_Distance_Sphere(ST_MakePoint(:latitude, :longitude), "point") AS distance FROM public."EntregadorData" WHERE ST_DWithin(Geography(point),Geography(ST_GeographyFromText(\'SRID=4326;POINT('+latitude+' '+longitude+')\')),20000) AND disponivel = true AND "entregadorIdEntregador" NOT IN('+notIn+') ORDER BY point DESC';

            //Realizar a query
            sequelize.query(query, {
                replacements: {
                    latitude: parseFloat(latitude),
                    longitude: parseFloat(longitude),
                    maxDistance: radius * 1000 //A unidade do radius é em KM
                },
                type: sequelize.QueryTypes.SELECT
            })
                .then(function (result) {
                    callback(result, null);
                }).catch(function (error) {
                callback(null, error);
            })
        }
      },
      paranoid: true,
      tableName: 'EntregadorData'
    });
  return EntregadorData;
};