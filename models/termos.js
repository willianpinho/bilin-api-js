/* jshint indent: 2 */

module.exports = function (sequelize, Sequelize) {
  var Termo = sequelize.define('termo', {
    idTermo: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    texto: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    versao: {
      type: Sequelize.FLOAT,
      allowNull: true
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
    classMethods: {
      associate: function (models) {},
      insert: function (parameters, callback) {
        Termo
          .build(parameters)
          .save()
          .then(function (produto) {
            return callback(produto, null);
          })
          .catch(function (error) {
            return callback(null, error);
          });
      },
      get: function (parameters, callback) {
        Termo
          .findOne(parameters)
          .then(function (produto) {
            return callback(produto, null);
          })
          .catch(function (error) {
            return callback(null, error);
          });
      },
      getAll: function (parameters, callback) {
        Termo
          .findAll(parameters)
          .then(function (termo) {
            return callback(termo, null);
          })
          .catch(function (error) {
            return callback(null, error);
          });
      },
      updateData: function (whereParameter, parameters, callback) {
        Termo
          .update(parameters, {where: whereParameter})
          .then(function (updatedTermo) {
            return callback(updatedTermo, null);
          })
          .catch(function (error) {
            return callback(null, error);
          });
      }
    },
    paranoid: true,
    tableName: 'Termo'
  });
  return Termo;
};
