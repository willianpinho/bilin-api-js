/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var UserPromo =  sequelize.define('userPromo', {
    idUserPromo: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    idReferral: {
      type: Sequelize.BIGINT,
      allowNull: true
    },
    codigo: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty : true
      },
    },
    dataInicio: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    dataFim: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    valor:{
      type: Sequelize.FLOAT,
      allowNull: true,
      isFloat: true
    },
    percentual:{
      type: Sequelize.FLOAT,
      allowNull: true,
      isFloat: true
    },
    count:{
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue : 0,
      max: 1
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
        UserPromo.belongsTo(models.usuario);
      }
      },
    paranoid: true,  
    tableName: 'UserPromo'
  });
  return UserPromo;
};
