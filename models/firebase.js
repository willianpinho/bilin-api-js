/* jshint indent: 2 */

//Define a model que irá tratar os tokens do FCM

module.exports = function(sequelize, Sequelize) {
  var FBaseCloud =  sequelize.define('FBaseCloud', {
    idFBaseCloud: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    token:{
        type: Sequelize.STRING,
        allowNull : false
    },
    os :{
        type: Sequelize.INTEGER,
        allowNull : false
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
         FBaseCloud.belongsTo(models.usuario);
         FBaseCloud.belongsTo(models.entregador);
      },insert: function (parameters, callback) {
          FBaseCloud
            .build(parameters)
            .save()
            .then(function (device) {
              return callback(device,null);
            }).catch(function (error) {
                console.log(error);
              return callback(null,error);
            });
        },
        get: function (parameters, callback) {
          FBaseCloud.findOne(parameters)
            .then(function(device){
              return callback(device,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        getByUserId: function (userId, callback) {
          var parameters = {
            where : {
              usuarioIdUsuario : userId
            }
          }
          FBaseCloud.findAll(parameters)
            .then(function(device){
              return callback(device,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        getByEntregadorId: function (entregadorId, callback) {
          var parameters = {
            where : {
              entregadorIdEntregador : entregadorId
            }
          }
          FBaseCloud.findAll(parameters)
            .then(function(device){
              return callback(device,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        getAll: function (parameters, callback) {
          FBaseCloud.findAll(parameters)
            .then(function(device){
              return callback(device,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        updateData: function (whereParameter,parameters, callback) {
          FBaseCloud.update(parameters, { where: whereParameter } ).then(function(updatedDevice){
             return callback(updatedDevice, null);
          }).catch(function(error){
             return callback(null,error);
          });
        }
      },
    paranoid: true,  
    tableName: 'FBaseCloud'
  });
  return FBaseCloud;
};