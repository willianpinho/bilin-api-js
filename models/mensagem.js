/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var Mensagem =  sequelize.define('mensagem', {
    idMensagem: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    mensagem: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    from: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
        Mensagem.belongsTo(models.pedido);
        Mensagem.belongsTo(models.usuario);
        Mensagem.belongsTo(models.entregador);
      },insert: function (parameters, callback) {
          Mensagem
            .build(parameters)
            .save()
            .then(function (mensagem) {
              return callback(mensagem, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        get: function (parameters, callback) {
          Mensagem.findOne(parameters)
            .then(function (mensagem) {
              return callback(mensagem, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        }, getAll: function (parameters, callback) {
          Mensagem.findAll(parameters)
            .then(function (mensagem) {
              return callback(mensagem, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        updateData: function (whereParameter, parameters, callback) {
          Mensagem.update(parameters, { where: whereParameter }).then(function (updatedMensagem) {
            return callback(updatedMensagem, null);
          }).catch(function (error) {
            return callback(null, error);
          });
        }
      },
    paranoid: true,  
    tableName: 'Mensagem'
  });
  return Mensagem;
};
