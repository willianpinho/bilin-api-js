/* jshint indent: 2 */

"use strict";

var bcrypt = require('bcryptjs');
const cpf = require('node-cpf-cnpj');

//Model que descreve o entregador

module.exports = function (sequelize, Sequelize) {
  var Entregador = sequelize.define('entregador', {
    idEntregador: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    senha: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    cpf: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: true,
        isValidCpf: function (value, next) {
          var formatted = cpf.format(value);
          //Validar o cpf
          if (cpf.isValid(formatted)) {
            return next();
          } else {
            return next('invalid.cpf');
          }
        }
      }
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: true,
      validate: {
        is: ["^[a-zA-Zà-úÀ-Ú ]+$"]
      }
    },
    imagemUrl: {
      type: Sequelize.STRING,
      allowNull: true
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    isAutonomo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    imgCpf: {
      type: Sequelize.STRING,
      allowNull: true
    },
    imgRg: {
      type: Sequelize.STRING,
      allowNull: true
    },
    imgCnh: {
      type: Sequelize.STRING,
      allowNull: true
    },
    imgCrv: {
      type: Sequelize.STRING,
      allowNull: true
    },
    role: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: "entregador"
    },
    veiculoNome: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [2, 25]
      }
    },
    veiculoPlaca: {
      type: Sequelize.STRING,
      allowNull: false,
      validatePlaca: function (value) {
        if (/!^[A-Z]{3}\d{4}$/.test(value)) {
          throw new Error('Formato de placa invalido')
        }
      }
    },
    veiculoMarca: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        is: ["^[a-zA-Zà-úÀ-Ú ]+$"],
        notEmpty: true,
        len: [2, 30]
      }
    },
    resetPasswordToken: {
      type: Sequelize.STRING,
      allowNull: true
    },
    resetPasswordExpires: {
      type: Sequelize.DATE,
      allowNull: true
    }
  }, {
    instanceMethods: {
      validatePassword: function (pass) {
        return bcrypt.compareSync(pass, this.senha);
      }
    },
    classMethods: {
      associate: function (models) {
        Entregador.hasMany(models.venda);
        Entregador.hasMany(models.pedido);
        Entregador.hasMany(models.mensagem);
        Entregador.hasMany(models.telefone);
        Entregador.hasMany(models.produto);
        Entregador.hasMany(models.avaliacao);
        Entregador.hasOne(models.entregadorData);
        Entregador.belongsTo(models.distribuidora);
      },
      generateHash: function (passwd) {
        return bcrypt.hashSync(passwd, bcrypt.genSaltSync(8), null);
      },
      insert: function (parameters, callback) {
        Entregador
          .build(parameters)
          .save()
          .then(function (user) {
            delete user.dataValues["senha"];
            return callback(user, null);
          })
          .catch(function (error) {
            console.log(error);
            return callback(null, error);
          });
      },
      get: function (parameters, callback) {
        Entregador
          .findOne(parameters)
          .then(function (user) {
            return callback(user, null);
          })
          .catch(function (error) {
            return callback(null, error);
          });
      },
      updateData: function (whereParameter, parameters, callback) {
        Entregador
          .update(parameters, {where: whereParameter})
          .then(function (updatedUser) {
            return callback(updatedUser, null);
          })
          .catch(function (error) {
            return callback(null, error);
          });
      },
      getProfile: function (idEntregador, callback) {
        //Query
        const query = 'SELECT "entregador"."idEntregador", "entregador"."nome", "entregador"."imagemUrl' +
            '","entregador"."veiculoNome","entregador"."veiculoMarca","entregador"."veiculoPlaca", AVG("estrelas") AS "avaliacaos.estrelasMedia" FROM "Entregador" AS "entregado' +
            'r" LEFT OUTER JOIN  "Avaliacao" AS "avaliacaos" ON "entregador"."idEntregador" = "aval' +
            'iacaos"."entregadorIdEntregador" AND("avaliacaos"."deletedAt" IS NULL AND "avali' +
            'acaos"."from" = :fromParameter) WHERE ("entregador"."deletedAt" IS NULL AND "ent' +
            'regador"."idEntregador" = :idEntregador) GROUP BY "entregador"."idEntregador";'

        //Realizar a query
        sequelize.query(query, {
          replacements: {
            idEntregador: idEntregador,
            fromParameter: 'usuario'
          },
            type: sequelize.QueryTypes.SELECT
          })
          .then(function (result) {
            callback(result, null);
          })
          .catch(function (error) {
            callback(null, error);
          })
      },
      activateEntregador: function (idEntregador, callback) {
        //Query
        const query = 'UPDATE "Entregador" SET "resetPasswordToken"=:passToken, ativo = true , "updated' +
            'At"= CURRENT_TIMESTAMP WHERE "idEntregador" = :idEntregador';
        //Realizar a query
        sequelize.query(query, {
          replacements: {
            idEntregador: idEntregador,
            passToken: ''
          },
            type: sequelize.QueryTypes.SELECT
          })
          .then(function (result) {
            callback(result, null);
          })
          .catch(function (error) {
            callback(null, error);
          })
      }
    },
    hooks: {
      beforeCreate: function (user, options) {
        user.senha = bcrypt.hashSync(user.senha, bcrypt.genSaltSync(8), null);
      }
    },
    paranoid: true,
    tableName: 'Entregador'
  });
  return Entregador;
};
