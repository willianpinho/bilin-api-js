/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var Produto =  sequelize.define('produto', {
    idProduto: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    descricao: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty : true,
      },
    },
    valor:{
        type: Sequelize.FLOAT,
        allowNull : false
    },
    marca: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue : ''
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
        Produto.belongsToMany(models.pedido, {through: 'produtoPedido'});
        Produto.belongsTo(models.entregador);
        Produto.belongsTo(models.distribuidora);
        //Produto.hasMany(models.cidade);
        Produto.belongsTo(models.cidade);
      },
      insert: function (parameters, callback) {
          Produto
            .build(parameters)
            .save()
            .then(function (produto) {
              return callback(produto, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        get: function (parameters, callback) {
          Produto.findOne(parameters)
            .then(function (produto) {
              return callback(produto, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        }, getAll: function (parameters, callback) {
          Produto.findAll(parameters)
            .then(function (produto) {
              return callback(produto, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        updateData: function (whereParameter, parameters, callback) {
          Produto.update(parameters, { where: whereParameter }).then(function (updatedProduto) {
            return callback(updatedProduto, null);
          }).catch(function (error) {
            return callback(null, error);
          });
        }
      },
    paranoid: true,  
    tableName: 'Produto'
  });
  return Produto;
};