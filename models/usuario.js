/* jshint indent: 2 */
"use strict";
var bcrypt = require('bcryptjs');
const cpf = require('node-cpf-cnpj');



//Model que descreve o usuário

module.exports = function (sequelize, Sequelize) {
  var Usuario = sequelize.define('usuario', {
    idUsuario: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    senha: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    nome: {
      type: Sequelize.STRING,
      allowNull: true,
      validate: {
        is: ["^[a-zA-Zà-úÀ-Ú ]+$"]
      }
    },
    telefone: {
      type: Sequelize.STRING,
      allowNull: true,
      validate: {
        len: [10, 20],
        is: /^1\d\d(\d\d)?$|^0800 ?\d{3} ?\d{4}$|^(\(0?([1-9a-zA-Z][0-9a-zA-Z])?[1-9]\d\) ?|0?([1-9a-zA-Z][0-9a-zA-Z])?[1-9]\d[ .-]?)?(9|9[ .-])?[2-9]\d{3}[ .-]?\d{4}$/gm
      }
    },
    imagemUrl: {
      type: Sequelize.STRING,
      allowNull: true
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    genero: {
      type: Sequelize.STRING,
      allowNull: true
    },
    cpf: {
      type: Sequelize.STRING,
      allowNull: true,
      unique: true,
      validate: {
        notEmpty: true,
        isValidCpf: function (value, next) {
          var formatted = cpf.format(value);
          //Validar o cpf
          if (cpf.isValid(formatted)) {
            return next();
          } else {
            return next('invalid.cpf');
          }
        }
      }
    },
    facebookId: {
      type: Sequelize.STRING,
      allowNull: true
    },
    notificar: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    promocao: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    creditos: {
      type: Sequelize.FLOAT,
      allowNull: true,
      defaultValue: 0,
      isFloat: true
    },
    tokenPromo: {
      type: Sequelize.STRING,
      allowNull: true
    },
    provider: {
      type: Sequelize.STRING,
      allowNull: true
    },
    role: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: "usuario"
    },
    resetPasswordToken: {
      type: Sequelize.STRING,
      allowNull: true
    },
    resetPasswordExpires: {
      type: Sequelize.DATE,
      allowNull: true
    }
  }, {
      instanceMethods: {
        validatePassword: function (pass) {
          return bcrypt.compareSync(pass, this.senha);
        }
      },
      classMethods: {
        associate: function (models) {
          Usuario.hasMany(models.device);
          Usuario.hasMany(models.endereco);
          Usuario.hasMany(models.pedido);
          Usuario.hasMany(models.mensagem);
          Usuario.hasMany(models.avaliacao);
        },
        generateHash: function (passwd) {
          return bcrypt.hashSync(passwd, bcrypt.genSaltSync(8), null);
        },
        insert: function (parameters, callback) {
          Usuario
            .build(parameters)
            .save()
            .then(function (user) {
              delete user.dataValues["senha"];
              return callback(user, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        get: function (parameters, callback) {
          Usuario.findOne(parameters)
            .then(function (user) {
              return callback(user, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
          getAll: function (parameters, callback) {
          Usuario.findAll(parameters)
            .then(function (user) {
              return callback(user, null);
            }).catch(function (error) {
              return callback(null, error);
            });
        },
        updateData: function (whereParameter, parameters, callback) {
          Usuario.update(parameters, { where: whereParameter }).then(function (updatedUser) {
            return callback(updatedUser, null);
          }).catch(function (error) {
            return callback(null, error);
          });
        },
        activateUser : function(idUsuario,callback){
          //Query 
           const query = 'UPDATE "Usuario" SET "resetPasswordToken"=:passToken, ativo = true , "updatedAt"= CURRENT_TIMESTAMP WHERE "idUsuario" = :idUsuario';
          //Realizar a query
          sequelize.query(query, {
            replacements: {
              idUsuario: idUsuario,
              passToken : ''
            },
            type: sequelize.QueryTypes.SELECT
          })
            .then(function (result) {
              callback(result, null);
            }).catch(function (error) {
              callback(null, error);
            })
        }
      },
      hooks: {
        beforeCreate: function (user, options) {
          user.senha = bcrypt.hashSync(user.senha, bcrypt.genSaltSync(8), null);
        }
      },
      paranoid: true,
      tableName: 'Usuario'
    });
  return Usuario;
};
