/* jshint indent: 2 */

module.exports = function(sequelize, Sequelize) {
  var Venda =  sequelize.define('venda', {
    idVenda: {
      type: Sequelize.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    status: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    valor: {
      type: Sequelize.FLOAT,
      allowNull: false,
      isFloat: true
    },
    ativo: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }
  }, {
      classMethods: {
        associate : function (models){
        Venda.belongsTo(models.pedido); 
        Venda.belongsTo(models.entregador);
        Venda.belongsTo(models.distribuidora);
      },insert: function (parameters, callback) {
          Venda
            .build(parameters)
            .save()
            .then(function (venda) {
              return callback(venda,null);
            }).catch(function (error) {
              return callback(null,error);
            });
        },
        get: function (parameters, callback) {
          Venda.findOne(parameters)
            .then(function(venda){
              return callback(venda,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        getAll: function (parameters, callback) {
          Venda.findAll(parameters)
            .then(function(venda){
              return callback(venda,null);
            }).catch(function(error){
               return callback(null,error);
            });
        },
        updateData: function (whereParameter,parameters, callback) {
          Venda.update(parameters, { where: whereParameter } ).then(function(updatedVenda){
             return callback(updatedVenda, null);
          }).catch(function(error){
             return callback(null,error);
          });
        }
      },
    paranoid: true,  
    tableName: 'Venda'
  });
  return Venda;
};