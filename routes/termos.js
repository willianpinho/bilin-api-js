"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var errorUtil = require('../utils/errorUtils');
var roles = require('../utils/roles');
var pedidoController = require('../utils/pedidoController');
var passport = require('passport');
var jwt = require('jsonwebtoken');


/**
 * @swagger
 * definitions:
 *   Termo:
 *     properties:
 *       idTermo:
 *         type: integer
 *       texto:
 *         type: string
 *       versao:
 *         type: float
 *       ativo:
 *         type: boolean
 */


/**
 * @swagger
 * /termos:
 *   get:
 *     tags:
 *       - Termos
 *     description: Buscar termos de uso
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Texto do termo de uso
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Termo'
 */

//Buscar termos de uso
router.get('/', function(req, res) {
    var params = {
        attributes : ['idTermo','texto','versao','createdAt'],
        order: [['createdAt', 'DESC']],
        limit: 1,
    }
    models.termo.get(params, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "terms.get.error", 500));
        }
        return res.status(200).json(data);
    });
})


module.exports = router;
