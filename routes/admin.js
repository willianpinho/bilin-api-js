"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var roles = require('../utils/roles');
var emailUtils = require('../utils/emails');
var errorUtil = require('../utils/errorUtils');
var nconf = require('nconf');
var path = require("path");
var env = process.env.NODE_ENV || "development";
var config = path.join(__dirname, 'config.json');
nconf.argv().env();
nconf.file({ file: config });



//Gerar Token de Autenticação
var generateToken = function (res, user) {
    var expiresIn = (1 * (60 * 60)); //Valido por uma hora
    var currentDate = new Date();
    var expiresDate = new Date(currentDate.getTime() + expiresIn);
    var payload = {
        id: user.dataValues.idAdmin,
        mail: user.dataValues.email,
        role: user.dataValues.role
    }

    //Retornar o token
    var token = jwt.sign(payload, nconf.get('secret'), { // Gerando um token baseado nos dados do usuário
        expiresIn: expiresIn // in seconds
    });
    delete user.dataValues["senha"];
    res.status(200).json({ success: true, expires: expiresDate, token: 'JWT ' + token, user: user.dataValues });
}


/**
 * @swagger
 * definitions:
 *   Administrador:
 *     properties:
 *       idAdmin:
 *         type: integer
 *       nome:
 *         type: string
 *       email:
 *         type: string
 *       senha:
 *         type: string
 *       provider:
 *         type: string
 *       ativo:
 *         type: boolean
 */



//____________________________________ ADMINISTRADOR _____________________________________________________

/**
 * @swagger
 * /admin/auth:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Autenticar Administrador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *       - name: senha
 *         type: string
 *         description: Senha obrigatório
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do Administrador, token JWT
 *       401:
 *         description: Usuário ou senha incorretas
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Administrador'
 */

//Autenticação de Administrador
router.post('/auth', function (req, res) {
    if (!req.body.email || !req.body.senha) {
        return res.status(400).json({ success: false, "message": "email.or.password.not.sent", "code": 66 });
    }
    var parameters = {
        attributes: ['idAdmin', 'nome', 'email', 'senha', 'role', 'ativo'],
        where: {
            email: req.body.email
        }
    }
    models.admin.get(parameters, function (user, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "unable.to.auth", 500));
        }
        if (user) {
            if (user.ativo == false) {
                return res.status(401).json({ success: false, title: "inactive.admin", message: "inactive.admin", "code": 160 });
            }
            if (user.validatePassword(req.body.senha)) {
                generateToken(res, user);
            } else {
                return res.status(401).json({ success: false, title: "unable.to.login", message: "incorrect.username.password", "code": 52 });
            }
        } else {
            return res.status(401).json({ success: false, title: "admin.not.found", message: "admin.not.found", "code": 50 });
        }
    })
});


//____________________________________ ENTREGADOR ____________________________________________________________

/**
 * @swagger
 * /admin/entregadores:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Buscar dados de entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cpf
 *         type: integer
 *         description: CPF do entregador, obrigatório
 *         in: query
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do entregador
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Buscar dados do entregador pelo cpf

router.get('/entregadores', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.query.cpf) {
        return res.status(400).json({ success: false, "message": "CPF.was.not.sent", "code": 63 });
    }
    var params = {
        where: {
            cpf: req.query.cpf
        }
    }

    models.entregador.get(params, function (entregador, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.get.entregador.error", 500));
        }
        return res.status(200).json(entregador);
    })

});

/**
 * @swagger
 * /admin/entregadores/{idEntregador}/activate:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Ativar conta do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Ativar conta do entregador
router.post('/entregadores/:idEntregador/activate', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idEntregador || req.body.ativo) {
        return res.status(400).json({ success: false, "message": "idEntregador.or.ativo.was.not.sent", "code": 63 });
    }
    models.entregador.activateEntregador(req.params.idEntregador, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.activate.entregador.error", 500));
        }
        //Se tudo certo, vamos enviar um email para o entregador
        models.entregador.get({ attributes: ['email', 'nome'], where: { idEntregador: req.params.idEntregador } }, function (data, error) {
            if (error) {
                return res.status(500).json(errorUtil.jsonFromError(error, "admin.activate.entregador.error", 500));
            }
            //Enviar email de ativacao
            emailUtils.sendEntregadorActivatedSucessEmail(data.email, data.nome, function (status, error) {
                if (error) {
                    return res.status(200).json({ success: false, "message": "entregador.activated.email.was.sent" });
                }
                return res.status(200).json({ success: true, "message": " email.was.sent" });
            })
        })
    })
});


/**
 * @swagger
 * /admin/entregadores/{idEntregador}:
 *   put:
 *     tags:
 *       - Administrador
 *     description: Alterar status do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: ativo
 *         type: boolean
 *         description: Status da conta do entregador
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Alterar status do entregador
router.put('/entregadores/:idEntregador', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idEntregador || !req.body.ativo) {
        return res.status(400).json({ success: false, "message": "idEntregador.or.ativo.was.not.sent", "code": 63 });
    }

    var params = {
        ativo: req.body.ativo
    }

    var whereParams = {
        idEntregador: req.params.idEntregador
    }

    models.entregador.updateData(whereParams, params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.get.entregador.error", 500));
        }
        return res.status(200).json(data);
    })
});

//_______________________________________ USUÁRIO _____________________________________________________


/**
 * @swagger
 * /admin/usuarios/list:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Listar usuarios, com paginação
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10, o Limite é 50.
 *         in: query
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados dos usuarios
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */
//Listar usuarios
router.get('/usuarios/list', passport.authenticate('jwt', { session: false }), function (req, res) {
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }

    var params = {
        attributes: ['idUsuario', 'nome', 'email', 'imagemUrl', 'telefone', 'cpf'],
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'endereco', 'bairro', 'cep', 'referencia'
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    }
                ]
            }]
            ,
            limit : limit,
            offset : offset

    }
    models.usuario.getAll(params, function (user, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.get.usuario.error", 500));
        }
        return res.status(200).json(user);
    })
})




/**
 * @swagger
 * /admin/usuarios:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Buscar dados do usuário pelo email
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         type: string
 *         description: Email do usuário, obrigatório
 *         in: query
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do usuario
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Buscar dados do usuário pelo email

router.get('/usuarios', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.query.email) {
        return res.status(400).json({ success: false, "message": " email.was.not.sent", "code": 63 });
    }

    var params = {
        where: {
            email: req.query.email
        }
    }

    models.usuario.get(params, function (user, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.get.usuario.error", 500));
        }
        return res.status(200).json(user);
    })

});


/**
 * @swagger
 * /admin/usuarios:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Buscar dados do usuário pelo cpf
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cpf
 *         type: string
 *         description: CPF, somente numeros
 *         in: query
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do usuario
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Buscar dados do usuário pelo email

router.get('/usuarios', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.query.cpf) {
        return res.status(400).json({ success: false, "message": " cpf.was.not.sent", "code": 63 });
    }

    var params = {
        where: {
            cpf: req.query.cpf
        }
    }

    models.usuario.get(params, function (user, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "admin.get.usuario.cpf.error", 400));
        }
        return res.status(200).json(user);
    })

});


/**
 * @swagger
 * /admin/usuarios/{idUsuario}/status:
 *   put:
 *     tags:
 *       - Administrador
 *     description: Alterar status do usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: string
 *         description: idUsuario, obrigatório
 *         in: path
 *         required: true
 *       - name: ativo
 *         type: boolean
 *         description: Status da conta do usuário
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Alterar status do usuário
router.put('/usuarios/:idUsuario/status', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idUsuario || !req.body.ativo) {
        return res.status(400).json({ success: false, "message": "idUsuario.or.ativo.was.not.sent", "code": 63 });
    }

    var params = {
        ativo: req.body.ativo
    }

    var whereParams = {
        idUsuario: req.params.idUsuario
    }

    models.usuario.updateData(whereParams, params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.update.usuario.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /admin/usuarios/{idUsuario}:
 *   put:
 *     tags:
 *       - Administrador
 *     description: Alterar créditos de um usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: string
 *         description: idUsuario obrigatório
 *         in: path
 *         required: true
 *       - name: creditos
 *         type: string
 *         description: valor dos creditos obrigatório
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Alterar créditos do usuário
router.put('/usuarios/:idUsuario', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idUsuario || !req.body.creditos) {
        return res.status(400).json({ success: false, "message": "idUsuario.or.creditos.was.not.sent", "code": 63 });
    }
    models.usuario.updateData({ idUsuario: req.body.idUsuario }, { creditos: parseFloat(req.body.creditos) }, function (data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "admin.update.usuario.credito.error", 400));
        }
        return res.status(200).json(data);
    })
})


//_____________________________________________ DISTRIBUIDORA ______________________________________

/**
 * @swagger
 * /admin/distribuidoras:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Cadastrar Distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: nome
 *         type: string
 *         description: Nome da distribuidora, obrigatório
 *         in: formData
 *         required: true
 *       - name: email
 *         type: string
 *         description: Email da distribuidora, obrigatório
 *         in: formData
 *         required: true
 *       - name: senha
 *         type: string
 *         description: Senha da distribuidora, obrigatório
 *         in: formData
 *         required: true
 *       - name: cnpj
 *         type: string
 *         description: CNPJ da distribuidora, obrigatório, somente numeros
 *         in: formData
 *         required: true
 *       - name: imagemUrl
 *         type: string
 *         description: URL da imagem da distribuidora, obrigatório
 *         in: formData
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da distribuidora cadastrada
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Cadastrar distribuidora
router.post('/distribuidoras', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.body.nome || !req.body.cnpj) {
        return res.status(400).json({ success: false, "message": "nome.or.cnpj.was.not.sent", "code": 63 });
    }

    var params = {
        nome: req.body.nome,
        email: req.body.email,
        cnpj: req.body.cnpj,
        imagemUrl: req.body.imagemUrl,
        senha : req.body.senha,
        ativo : true
    }

    models.distribuidora.insert(params, function (data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "admin.create.distribuidora.error", 400));
        }
        return res.status(200).json(data);
    });
});

/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}:
 *   put:
 *     tags:
 *       - Administrador
 *     description: Alterar dados da  Distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: ativo
 *         type: boolean
 *         description: Status da conta da distribuidora
 *         in: formData
 *         required: false
 *       - name: nome
 *         type: string
 *         description: Nome da distribuidora
 *         in: formData
 *         required: false
 *       - name: senha
 *         type: string
 *         description: Senha da distribuidora, obrigatório
 *         in: formData
 *         required: false
 *       - name: imagemUrl
 *         type: string
 *         description: URL da imagem da distribuidora
 *         in: formData
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */
//Alterar uma distribuidora
router.put('/distribuidoras/:idDistribuidora', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var updateFields = {};

    //Se o usuario informou o nome
    if (req.body.nome) {
        updateFields['nome'] = req.body.nome;
    }
    //Se o usuario informou o status
    if (req.body.ativo) {
        updateFields['ativo'] = req.body.ativo;
    }
    //Se o usuario informou a imagemUrl
    if (req.body.imagemUrl) {
        updateFields['imagemUrl'] = req.body.imagemUrl;
    }
    //Se o usuario quiser atualizar a senha
    if (req.body.senha) {
        var pass = models.distribuidora.generateHash(req.body.senha);
        updateFields['senha'] = pass;
    }

    models.distribuidora.updateData({ idDistribuidora: req.params.idDistribuidora }, updateFields, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.insert.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })

});


/**
 * @swagger
 * /admin/distribuidoras:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Buscar Distribuidora pelo CNPJ
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cnpj
 *         type: string
 *         description: CNPJ da distribuidora, obrigatório
 *         in: query
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da distribuidora cadastrada
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

router.get('/distribuidoras', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.query.cnpj) {
        return res.status(400).json({ success: false, "message": "CNPJ.was.not.sent", "code": 63 });
    }

    var params = {
        where: {
            cnpj: req.query.cnpj
        }
    }

    models.distribuidora.get(params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.get.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })
})


/**
 * @swagger
 * /admin/distribuidoras/list:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Listar distribuidoras, com paginaçāo
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10 o Limite é 90.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados da distribuidoras
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

router.get('/distribuidoras/list', roles.checkRole("admin", "idAdmin"), function (req, res) {
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 90) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var params = {
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'endereco', 'bairro', 'cep', 'referencia'
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    }
                ]
            }, {
                model: models.telefone,
                required: false
            }
        ],
        offset: offset,
        limit: limit
    }

    models.distribuidora.getAll(params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.getAll.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })
})



/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}/entregadores:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Vincular Entregador a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora obrigatório
 *         in: path
 *         required: true
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador obrigatório
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Vincular entregador a distribuidora
router.post('/distribuidoras/:idDistribuidora/entregadores', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idDistribuidora || !req.body.idEntregador) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idEntregador.was.not.sent", "code": 63 });
    }
    models.entregador.updateData({ idEntregador: req.body.idEntregador }, { distribuidoraIdDistribuidora: req.params.idDistribuidora }, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.set.distribuidora.entregador.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}/endereco:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Cadastrar endereço do da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idCidade
 *         type: string
 *         description: idCidade da Cidade do Endereço
 *         in: formData
 *         required: true
 *       - name: titulo
 *         type: string
 *         description: Titulo do endereço
 *         in: formData
 *         required: true
 *       - name: endereco
 *         type: string
 *         description: Endereço 
 *         in: formData
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Numero da rua, se houver.
 *         in: formData
 *         required: false
 *       - name: complemento
 *         type: string
 *         description: Complemento do endereço
 *         in: formData
 *         required: false
 *       - name: bairro
 *         type: string
 *         description: Bairro do endereço
 *         in: formData
 *         required: true
 *       - name: cep
 *         type: string
 *         description: CEP do Endereço, Ex - 70130-070
 *         in: formData
 *         required: true
 *       - name: referencia
 *         type: string
 *         description: Referência do Endereço
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do endereço cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou houve falha na validação
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Cadastrar endereco da distribuidora
router.post('/distribuidoras/:idDistribuidora/endereco', roles.checkRole("admin", "idAdmin"), function (req, res, next) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    if (!req.body.idCidade) {
        return res.status(400).json({ success: false, "message": "idCidade.was.not.sent", "code": 63 });
    }
    var parameters = {
        titulo: req.body.titulo,
        endereco: req.body.endereco,
        bairro: req.body.bairro,
        cep: req.body.cep,
        referencia: req.body.referencia,
        cidadeID: req.body.idCidade,
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        numero: req.body.numero,
        complemento: req.body.complemento,
        dono: "distribuidora"
    }

    models.endereco.insert(parameters, function (data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "insert.endereco.error", 500));
        }
        return res.status(200).json(data);
    })

});

/**
* @swagger
* /admin/distribuidoras/{idDistribuidora}/endereco/{idEndereco}:
*   put:
*     tags:
*       - Administrador
*     description: Alterar endereço da distribuidora
*     produces:
*       - application/json
*     parameters:
*       - name: idDistribuidora
*         type: string
*         description: idDistribuidora, obrigatório
*         in: path
*         required: true
*       - name: idEndereco
*         type: string
*         description: idEndereco do endereco a ser alterado
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*       - name: idCidade
*         type: string
*         description: idCidade da Cidade do Endereço
*         in: formData
*         required: false
*       - name: titulo
*         type: string
*         description: Titulo do endereço
*         in: formData
*         required: false
*       - name: endereco
*         type: string
*         description: Endereço 
*         in: formData
*         required: false
*       - name: numero
*         type: string
*         description: Numero da rua, se houver.
*         in: formData
*         required: false
*       - name: complemento
*         type: string
*         description: Complemento do endereço
*         in: formData
*         required: false
*       - name: bairro
*         type: string
*         description: Bairro do endereço
*         in: formData
*         required: false
*       - name: cep
*         type: string
*         description: CEP do Endereço, Ex - 70130-070
*         in: formData
*         required: false
*       - name: referencia
*         type: string
*         description: Referência do Endereço
*         in: formData
*         required: false
*     responses:
*       200:
*         description: Status da operação
*       400:
*         description: Faltaram parametros na requisição
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Endereco'
*/

//Alterar endereco da distribuidora
router.put('/distribuidoras/:idDistribuidora/endereco/:idEndereco', roles.checkRole("admin", "idAdmin"), function (req, res, next) {
    if (!req.params.idDistribuidora || !req.params.idEndereco) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idEndereco.was.not.sent", "code": 63 });
    }
    var updateFields = {};
    //Se o titulo foi informado
    if (req.body.titulo) {
        updateFields['titulo'] = req.body.titulo;
    }
    //Se o endereco foi passado
    if (req.body.endereco) {
        updateFields['endereco'] = req.body.endereco;
    }
    //Se o bairro foi passado
    if (req.body.bairro) {
        updateFields['bairro'] = req.body.bairro;
    }

    //Se o numero foi passado
    if (req.body.numero) {
        updateFields['numero'] = req.body.numero;
    }

    //Se o complemento foi passado
    if (req.body.complemento) {
        updateFields['complemento'] = req.body.complemento;
    }
    //Se a cidade foi informada
    if (req.body.idCidade) {
        updateFields['cidadeID'] = req.body.idCidade;
    }
    //Se o cep foi passado
    if (req.body.cep) {
        updateFields['cep'] = req.body.cep;
    }
    //Se a referencia foi passada
    if (req.body.referencia) {
        updateFields['referencia'] = req.body.referencia;
    }

    var whereParams = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        idEndereco: req.params.idEndereco,
        ativo: true
    }
    models.endereco.updateData(whereParams, updateFields, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "update.endereco.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}/telefones:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Cadastrar telefone da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Número do telefone com ddd
 *         in: formData
 *         required: true
 *       - name: descricao
 *         type: string
 *         description: descricao do telefone
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do telefone cadastrado
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */
//Adicionar telefone da distribuidora
router.post('/distribuidoras/:idDistribuidora/telefones', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var parameters = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        numero: req.body.numero,
        descricao: req.body.descricao
    }
    models.telefone.insert(parameters, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "insert.telefone.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}/telefones/{idTelefone}:
 *   put:
 *     tags:
 *       - Administrador
 *     description: Atualizar dados de um telefone da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: idTelefone
 *         type: string
 *         description: idTelefone, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Número do telefone com ddd
 *         in: formData
 *         required: true
 *       - name: descricao
 *         type: string
 *         description: descricao do telefone
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Status da atualizacao
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

//Alterar telefone
router.put('/distribuidoras/:idDistribuidora/telefones/:idTelefone', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idDistribuidora || !req.params.idTelefone) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idTelefone.was.not.sent", "code": 63 });
    }
    //Atualizando dados da distribuidora
    var whereParams = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        idTelefone: req.params.idTelefone
    }
    var updateFields = {};

    //Atualizar o numero
    if (req.body.numero) {
        updateFields['numero'] = req.body.numero;
    }
    //Atualizar a descricao
    if (req.body.descricao) {
        updateFields['descricao'] = req.body.descricao;
    }
    models.telefone.updateData(whereParams, updateFields, function (data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "update.telefone.distribuidora.error", 400));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}/telefones/{idTelefone}:
 *   delete:
 *     tags:
 *       - Administrador
 *     description: Deletar dados de um telefone da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: idTelefone
 *         type: string
 *         description: idTelefone, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da atualizacao
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

//Deletar telefone da distribuidora
router.delete('/telefones/:idEntregador/:idTelefone', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idDistribuidora || !req.params.idTelefone) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idTelefone.was.not.sent", "code": 63 });
    }
    //Atualizando dados do user
    var whereParams = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        idTelefone: req.params.idTelefone
    }
    var updateFields = {};
    updateFields['ativo'] = false;

    models.telefone.updateData(whereParams, updateFields, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "delete.telefone.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /admin/distribuidoras/{cnpj}/entregadores:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Buscar Entregadores vinculados a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cnpj
 *         type: string
 *         description: CNPJ da distribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da distribuidora cadastrada
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Buscar entregadores da distribuidora
router.get('/distribuidoras/:cnpj/entregadores', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.cnpj) {
        return res.status(400).json({ success: false, "message": "cnpj.was.not.sent", "code": 63 });
    }

    var params = {
        where: {
            cnpj: req.params.cnpj,
        },
        include: [{
            required: false,
            model: models.entregador,
            attributes: ['nome', 'imagemUrl', 'idEntregador', 'email', 'cpf']
        }]
    }

    models.distribuidora.getAll(params, function (user, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "admin.get.distribuidora.entregadores.error", 400));
        }
        return res.status(200).json(user);
    })
});

/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}/pedidos:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Buscar Pedidos vinculados a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: dataInicio
 *         type: string
 *         description: data de inicio, (Formato Iso) - DEFAULT ONTEM
 *         in: query
 *         required: false
 *       - name: dataFim
 *         type: string
 *         description: data do fim, (Formato Iso) - DEFAULT HOJE
 *         in: query
 *         required: false
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10 o Limite é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados dos pedidos
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */
//Buscar pedidos da distribuidora
router.get('/distribuidoras/:idDistribuidora/pedidos', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var startDate = Date.parse(req.query.dataInicio) || Date(new Date() - 24 * 60 * 60 * 1000);
    var endDate = Date.parse(req.query.dataFim) || new Date();
    var params = {
        where: {
            distribuidoraIdDistribuidora: req.params.idDistribuidora,
            createdAt: {
                $lt: endDate,
                $gt: startDate
            },
        },
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'endereco', 'bairro', 'cep', 'referencia'
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    }
                ]
            }, {
                model: models.entregador,
                attributes: ['nome', 'imagemUrl'],
                include: [{
                    model: models.entregadorData,
                    attributes: ['point'],
                    required: false
                }]
            }, {
                model: models.usuario,
                attributes: ['nome', 'idUsuario', 'email', 'imagemUrl'],
                required: false
            }

        ],
        offset: offset,
        limit: limit
    }
    models.pedido.getAll(params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.get.pedidos.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    });
})

/**
 * @swagger
 * /admin/distribuidoras/{idDistribuidora}/vendas:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Buscar Vendas vinculadas a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: dataInicio
 *         type: string
 *         description: data de inicio, (Formato Iso) - DEFAULT ONTEM
 *         in: query
 *         required: false
 *       - name: dataFim
 *         type: string
 *         description: data do fim, (Formato Iso) - DEFAULT HOJE
 *         in: query
 *         required: false
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10 o máximo é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados das vendas
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */
//Buscar vendas da distribuidora
router.get('/distribuidoras/:idDistribuidora/vendas', roles.checkRole("admin", "idAdmin"), function (req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var startDate = Date.parse(req.query.dataInicio) || Date(new Date() - 24 * 60 * 60 * 1000);
    var endDate = Date.parse(req.query.dataFim) || new Date();
    var params = {
        where: {
            distribuidoraIdDistribuidora: req.params.idDistribuidora,
            createdAt: {
                $lt: endDate,
                $gt: startDate
            },
        },
        include: [{
            model: models.entregador,
            attributes: ['nome', 'imagemUrl'],
            include: [{
                model: models.entregadorData,
                attributes: ['point'],
                required: false
            }]
        }],
        offset: offset,
        limit: limit
    }
    models.venda.getAll(params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "admin.get.vendas.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    });
})

//_______________________________________ PRODUTO ______________________________________


/**
 * @swagger
 * /admin/produtos:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Cadastrar produto 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idCidade
 *         type: string
 *         description: idCidade do produto
 *         in: formData
 *         required: true
 *       - name: nome
 *         type: string
 *         description: Nome do produto
 *         in: formData
 *         required: true
 *       - name: descricao
 *         type: string
 *         description: descricao do produto
 *         in: formData
 *         required: false
 *       - name: valor
 *         type: float
 *         description: Valor do produto
 *         in: formData
 *         required: true
 *       - name: marca
 *         type: string
 *         description: Marca do produto
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do produto cadastrado
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Produto'
 */

//Cadastrar produtos
router.post('/produtos', roles.checkRole("admin", "idAdmin"), function (req, res, next) {
    if (!req.body.idCidade) {
        return res.status(400).json({ success: false, "message": "idCidade.was.not.sent", "code": 63 });
    }
    var parameters = {
        nome: req.body.nome,
        descricao: req.body.descricao,
        valor: parseFloat(req.body.valor),
        marca: req.body.marca,
        cidadeID: req.body.idCidade
    }

    models.produto.insert(parameters, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "insert.produto.error", 500));
        }
        return res.status(200).json(data);
    })
});

/**
 * @swagger
 * /admin/produtos/{idProduto}:
 *   put:
 *     tags:
 *       - Administrador
 *     description: Alterar produto 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idCidade
 *         type: string
 *         description: idCidade do produto
 *         in: formData
 *         required: true
 *       - name: idProduto
 *         type: string
 *         description: id do Produto
 *         in: path
 *         required: true
 *       - name: nome
 *         type: string
 *         description: Nome do produto
 *         in: formData
 *         required: false
 *       - name: descricao
 *         type: string
 *         description: descricao do produto
 *         in: formData
 *         required: false
 *       - name: valor
 *         type: float
 *         description: Valor do produto
 *         in: formData
 *         required: false
 *       - name: marca
 *         type: string
 *         description: Marca do produto
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Status da operacão
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Produto'
 */

//Alterar produto
router.put('/produtos/:idProduto', roles.checkRole("admin", "idAdmin"), function (req, res, next) {
    if (!req.params.idProduto) {
        return res.status(400).json({ success: false, "message": "idProduto.was.not.sent", "code": 63 });
    }
    var updateFields = {};
    //Se o nome foi informado
    if (req.body.nome) {
        updateFields['nome'] = req.body.nome;
    }
    //Se a descricao foi passada
    if (req.body.descricao) {
        updateFields['descricao'] = req.body.descricao;
    }
    //Se a marca foi passada
    if (req.body.marca) {
        updateFields['marca'] = req.body.marca;
    }
    //Se o valor foi passado
    if (req.body.valor) {
        updateFields['valor'] = parseFloat(req.body.valor);
    }

    //Se o valor foi passado
    if (req.body.idCidade) {
        updateFields['cidadeID'] = req.body.idCidade;
    }

    var whereParams = {
        idProduto: req.params.idProduto,
        ativo: true
    }
    models.produto.updateData(whereParams, updateFields, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "update.produto.error", 500));
        }
        return res.status(200).json(data);
    })
});

/**
 * @swagger
 * /admin/produtos/list:
 *   get:
 *     tags:
 *       - Administrador
 *     description: Listar produtos, com paginação
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10, o Limite é 50.
 *         in: query
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados dos produtos
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Produto'
 */

router.get('/produtos/list', roles.checkRole("admin", "idAdmin"), function (req, res) {
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }

    var params = {
        attributes: ['idProduto', 'nome', 'descricao', 'marca', 'valor','cidadeID'],
        include: [{
            model: models.cidade,
            attributes: [
                'nome', 'estadoID'
            ],
            required: false
        }],
        limit : limit,
        offset : offset
    }
    models.produto.getAll(params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "produto.list.error", 500));
        }
        return res.status(200).json(data);
    });
})


//_______________________________ TERMOS _____________________________________________
/**
 * @swagger
 * /admin/termos:
 *   post:
 *     tags:
 *       - Administrador
 *     description: Cadastar termos de uso
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: texto
 *         type: string
 *         description: Texto dos termos de uso
 *         in: formData
 *         required: true
 *       - name: versao
 *         type: float
 *         description: versão dos termos de uso
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados dos termos de uso
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Termo'
 */

//Cadastar termos de uso
router.post('/termos', roles.checkRole("admin", "idAdmin"), function (req, res) {
    var params = {
        texto: req.body.texto,
        versao: parseFloat(req.body.versao)
    }
    models.termo.insert(params, function (data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "terms.insert.error", 500));
        }
        return res.status(200).json(data);
    });
});


module.exports = router;