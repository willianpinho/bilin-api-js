"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var errorUtil = require('../utils/errorUtils');
var roles = require('../utils/roles');



/**
 * @swagger
 * definitions:
 *   Device:
 *     properties:
 *       idDevice:
 *         type: integer
 *       os:
 *         type: integer
 *       uuid:
 *         type: string
 *       ativo:
 *         type: boolean
 */

/**
 * @swagger
 * definitions:
 *   FBaseCloud:
 *     properties:
 *       idFBaseCloud:
 *         type: integer
 *       os:
 *         type: integer
 *       token:
 *         type: string
 *       ativo:
 *         type: boolean
 */

/**
 * @swagger
 * /devices/usuarios/{idUsuario}:
 *   post:
 *     tags:
 *       - Device
 *     description: Cadastrar device do usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: string
 *         description: idUsuario, obrigatório
 *         in: path
 *         required: true
 *       - name: uuid
 *         type: string
 *         description: uuid do device, obrigatório
 *         in: formData
 *         required: true
 *       - name: os
 *         type: integer
 *         description: Sistema operacional do device, Obrigatório 1- Android 2 - Ios  3 - Outros
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do device cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Device'
 */

router.post('/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res) {
  if (!req.params.idUsuario || !req.body.uuid || !req.body.os) {
    return res.status(400).json({ success: false, "message": "idUsuario.or.uuid.or.os.was.not.sent", "code": 63 });
  }
  
  var osType = parseInt(req.body.os);
  if (osType < 0 || osType > 3) {
    return res.status(400).json({ success: false, "message": "invalid.os.type.value", "code": 63 });
  }

  var params = {
    usuarioIdUsuario: req.params.idUsuario,
    os: osType,
    uuid: req.body.uuid
  }

  models.device.insert(params, function (device, error) {
    if (error) {
      return res.status(400).json(errorUtil.jsonFromError(error, "insert.device.error", 400));
    }
    return res.status(200).json(device);
  })
});


/**
 * @swagger
 * /devices/entregadores/{idEntregador}:
 *   post:
 *     tags:
 *       - Device
 *     description: Cadastrar device do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: uuid
 *         type: string
 *         description: uuid do device, obrigatório
 *         in: formData
 *         required: true
 *       - name: os
 *         type: integer
 *         description: Sistema operacional do device, obrigatório 1- Android 2 - Ios  3 - Outros
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do device cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Device'
 */

router.post('/entregadores/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res) {
  if (!req.params.idEntregador || !req.body.uuid || !req.body.os) {
    return res.status(400).json({ success: false, "message": "idUsuario.or.uuid.or.os.was.not.sent", "code": 63 });
  }
  var osType = parseInt(req.body.os);
  if (osType < 0 || osType > 3) {
    return res.status(400).json({ success: false, "message": "invalid.os.type.value", "code": 63 });
  }
  var params = {
    entregadorIdEntregador: req.params.idEntregador,
    os: osType,
    uuid: req.body.uuid
  }

  models.device.insert(params, function (device, error) {
    if (error) {
      return res.status(400).json(errorUtil.jsonFromError(error, "insert.device.error", 400));
    }
    return res.status(200).json(device);
  })
});


/**
 * @swagger
 * /devices/firebase/usuarios/{idUsuario}:
 *   post:
 *     tags:
 *       - Device
 *     description: Cadastrar token do Firebase do usuario
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: string
 *         description: idUsuario, obrigatório
 *         in: path
 *         required: true
 *       - name: token
 *         type: string
 *         description: token gerado pelo firebase, obrigatório
 *         in: formData
 *         required: true
 *       - name: os
 *         type: integer
 *         description: Sistema operacional do device, obrigatório 1- Android 2 - Ios  3 - Outros
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do token cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/FBaseCloud'
 */

router.post('/firebase/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res) {
  if (!req.params.idUsuario || !req.body.token || !req.body.os) {
    return res.status(400).json({ success: false, "message": "idUsuario.or.token.or.os.was.not.sent", "code": 63 });
  }
  
  var osType = parseInt(req.body.os);
  if (osType < 0 || osType > 3) {
    return res.status(400).json({ success: false, "message": "invalid.os.type.value", "code": 63 });
  }

  var params = {
    usuarioIdUsuario: req.params.idUsuario,
    os: osType,
    token: req.body.token
  }
  models.FBaseCloud.getByUserId(req.params.idUsuario, function (device, error) {
    if (device.length == 0) {
      models.FBaseCloud.insert(params, function (device, error) {
        if (error) {
          return res.status(400).json(errorUtil.jsonFromError(error, "insert.firebase.token.usuario.error", 400));
        }
        return res.status(200).json(device);
      })
    } else {
      models.FBaseCloud.updateData({idFBaseCloud: device[0].idFBaseCloud},{token: req.body.token}, function (device, error) {
        if (error) {
          return res.status(400).json(errorUtil.jsonFromError(error, "insert.firebase.token.usuario.error", 400));
        }
        return res.status(200).json(device);
      })
    }
  });

});


/**
 * @swagger
 * /devices/firebase/entregadores/{idEntregador}:
 *   post:
 *     tags:
 *       - Device
 *     description: Cadastrar token do firebase do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: token
 *         type: string
 *         description: token do firebase, obrigatório
 *         in: formData
 *         required: true
 *       - name: os
 *         type: integer
 *         description: Sistema operacional do device, obrigatório
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do device cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/FBaseCloud'
 */

router.post('/firebase/entregadores/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res) {
  if (!req.params.idEntregador || !req.body.token || !req.body.os) {
    return res.status(400).json({ success: false, "message": "idEntregador.or.token.or.os.was.not.sent", "code": 63 });
  }
  var osType = parseInt(req.body.os);
  if (osType < 0 || osType > 2) {
    return res.status(400).json({ success: false, "message": "invalid.os.type.value", "code": 63 });
  }
  var params = {
    entregadorIdEntregador: req.params.idEntregador,
    os: osType,
    token: req.body.token
  }

  models.FBaseCloud.getByEntregadorId(req.params.idEntregador, function (device, error) {
    if (device.length == 0) {
      models.FBaseCloud.insert(params, function (device, error) {
        if (error) {
          return res.status(400).json(errorUtil.jsonFromError(error, "insert.firebase.token.usuario.error", 400));
        }
        return res.status(200).json(device);
      })
    } else {
      models.FBaseCloud.updateData({idFBaseCloud: device[0].idFBaseCloud},{token: req.body.token}, function (device, error) {
        if (error) {
          return res.status(400).json(errorUtil.jsonFromError(error, "insert.firebase.token.usuario.error", 400));
        }
        return res.status(200).json(device);
      })
    }
  });
});


module.exports = router;


