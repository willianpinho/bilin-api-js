'use strict';

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var roles = require('../utils/roles');
var emailUtils = require('../utils/emails');
var errorUtil = require('../utils/errorUtils');
var uploadUtils = require('../utils/uploadUtils');
var fbUtil = require('../utils/facebookRequest');
var nconf = require('nconf');
var path = require('path');
var env = process.env.NODE_ENV || 'development';
var config = path.join(__dirname, 'config.json');
nconf.argv().env();
nconf.file({ file: config });


//Gerar Token de Autenticação
var generateToken = function (res, user) {
  var expiresIn = 15778476000; //6 Meses
  var currentDate = new Date();
  var expiresDate = new Date(currentDate.getTime() + expiresIn);
  var payload = {
    id: user.dataValues.idUsuario,
    mail: user.dataValues.email,
    role: user.dataValues.role
  }

  //Retornar o token
  var token = jwt.sign(payload, nconf.get('secret'), { // Gerando um token baseado nos dados do usuário
    expiresIn: expiresIn // in seconds
  });
  delete user.dataValues['senha'];
  res.status(200).json({ success: true, expires: expiresDate, token: 'JWT ' + token, user: user.dataValues });
}



/**
 * @swagger
 * definitions:
 *   Usuario:
 *     properties:
 *       idUsuario:
 *         type: integer
 *       nome:
 *         type: string
 *       email:
 *         type: string
 *       senha:
 *         type: string
 *       facebookId:
 *         type: string
 *       imagemUrl:
 *         type: string
 *       telefone:
 *         type: string
 *       cpf:
 *         type: string
 *       genero:
 *         type: string
 *         description : Genero, por conta do bCash - M - F
 *       notificar:
 *         type: boolean
 *       promocao:
 *         type: boolean
 *       creditos:
 *         type: float
 *       provider:
 *         type: string
 *       ativo:
 *         type: boolean
 */


/**
 * @swagger
 * definitions:
 *   Endereco:
 *     properties:
 *       idEndereco:
 *         type: integer
 *       titulo:
 *         type: string
 *       endereco:
 *         type: string
 *       bairro:
 *         type: string
 *       cep:
 *         type: string
 *       dono:
 *         type: string
 *       referencia:
 *         type: string
 */


/**
 * @swagger
 * definitions:
 *   Telefone:
 *     properties:
 *       idTelefone:
 *         type: integer
 *       descricao:
 *         type: string
 *       numero:
 *         type: string
 *       ativo:
 *         type: boolean
 */



/**
 * @swagger
 * /usuarios:
 *   post:
 *     tags:
 *       - Usuario
 *     description: Cadastrar usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: nome
 *         type: string
 *         description: Nome obrigatório
 *         in: formData
 *         required: true
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *       - name: senha
 *         type: string
 *         description: Senha obrigatório
 *         in: formData
 *         required: true
 *       - name: imagemUrl
 *         type: string
 *         description: Url para imagem de usuário
 *         in: formData
 *         required: false
 *       - name: cpf
 *         type: string
 *         description: CPF , somente numeros
 *         in: formData
 *         required: false
 *       - name: telefone
 *         type: string
 *         description: Telefone com DDD.
 *         in: formData
 *         required: false
 *       - name: genero
 *         type: string
 *         description: Genero M-F.
 *         in: formData
 *         required: false
 *       - name: notificar
 *         type: boolean
 *         description: Se o usuário deseja ser notificado
 *         in: formData
 *         required: false
 *       - name: promocao
 *         type: boolean
 *         description: Se o usuário deseja ser notificado sobre promocoes
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do user cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Cadastrar usuarios
router.post('/', function (req, res, next) {
  var parameters = {
    nome: req.body.nome,
    senha: req.body.senha,
    email: req.body.email,
    provider: 'cadastro',
    notificar: req.body.notificar,
    promocao: req.body.promocao,
    telefone: req.body.telefone,
    imagemUrl : req.body.imagemUrl,
    genero : req.body.genero,
    cpf : req.body.cpf,
    ativo : true //Precisa confirmar cadastro
  }
  //Codigo promocional
  crypto.randomBytes(6, function (err, buf) {
    if (err) {
      return res.status(500).json(error);
    }
    var tokenPromo = buf.toString('hex');
    if (req.body.email) {
      parameters['tokenPromo'] = tokenPromo;
    }

    //Criar Codigo de confirmaçāo
    crypto.randomBytes(20, function (err, buf) {
      if (err) {
        callback(err, false)
      }
      var token = buf.toString('hex');
      var timeValid = Date.now() + 86400000;  //Tempo no qual o token estará valido' : 24 horas
      parameters['resetPasswordToken'] = token;
      parameters['resetPasswordExpires'] = timeValid;
     //Cadastrar o user de fato

      var parametersExists = {
          attributes: ['idUsuario'],
          where: {
              email: req.body.email
          }
      };

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

      models.usuario.get(parametersExists,function(user,error){

        if(user !== null){

            return res.status(500).json({'title' : 'Erro ao Cadastrar', 'message' : 'E-mail já cadastrado!' , 'code' : 500});

        }else{

          models.usuario.insert(parameters, function (user, error) {

              if (error) {
                return res.status(500).json({'title' : 'Erro ao Cadastrar', 'message' : capitalizeFirstLetter(error.errors[0]['path'])+ ' inválido!' , 'code' : 500});
              }

              delete user.senha;
              //Enviar email de confirmaçāo
              // emailUtils.sendConfirmationEmail(user.resetPasswordToken,user.email,req.headers.host,'usuarios/confirm/',user.nome,function(status,error){
              //     if(err){
              //         return res.status(500).json({'message' : 'user.registered.email.not.sent', 'status' : false});
              //     }
                  return res.status(200).json({'message' : 'user.registered.email.sent', 'status' : true});
              // })

          });

        }

      });



    })

  })
});



// //Confirmar cadastro

router.get('/confirm/:token', function (req, res) {
  var params = {
    attributes: ['resetPasswordToken', 'resetPasswordExpires', 'provider', 'idUsuario'],
    where: {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }
  }
  models.usuario.get(params, function (user, error) {
    if (error) {
      return res.render('index', {
        title: 'Erro ao confirmar cadastro',
        message: 'Erro interno do servidor' + error.message
      })
    }
    if (!user) {
      return res.render('index', {
        title: 'Erro ao confirmar cadastro',
        message: 'Token de ativação não enviado, ou a confirmaçāo já foi efetuada.'
      })
    }
    if (user.provider == 'facebook') {
      return res.render('index', {
        title: 'Erro ao confirmar cadastro',
        message: 'Cadastro vinculado a conta no Facebook, não é necessário ativação.'
      })
    }
    //Ativar o user
    models.usuario.activateUser(user.idUsuario, function (data, error) {
      if (error) {
        return res.render('index', {
          title: 'Erro ao confirmar cadastro',
          message: 'Erro interno do servidor' + error.message
        })
      }
      return res.render('index', {
        title: 'Cadastro confirmado',
        message: 'Você pode fazer login pelo App'
      })
    })
  })
});

/**
 * @swagger
 * /usuarios/auth:
 *   post:
 *     tags:
 *       - Usuario
 *     description: Autenticar usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *       - name: senha
 *         type: string
 *         description: Senha obrigatório
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do user cadastrado, token JWT
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Usuário ou senha incorretos
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Autenticação de usuário
router.post('/auth', function (req, res) {
  if (!req.body.email || !req.body.senha) {
    return res.status(400).json({ success: false, 'message': 'email.or.password.not.sent', 'code': 66 });
  }
  var parameters = {
    attributes: ['idUsuario', 'nome', 'telefone', 'creditos', 'provider', 'email', 'senha', 'role','ativo'],
    where: {
      email: req.body.email
    }
  }
  models.usuario.get(parameters, function (user, error) {
    if (error) {
      console.log(error)
      return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.auth', 500));
    }
    if (user.provider == 'facebook' || user.provider == 'google') {
      return res.status(401).json({ success: false, title: 'unable.to.login', message: 'social.login.provider', 'code': 47 });
    }
    if (user) {
      if(user.ativo == false){
        return res.status(401).json({ success: false, title: 'inactive.user', message: 'inactive.user', 'code': 150 });
      }
      if (user.validatePassword(req.body.senha)) {
        generateToken(res, user);
      } else {
        return res.status(401).json({ success: false, title: 'unable.to.login', message: 'incorrect.username.password', 'code': 52 });
      }
    } else {
      return res.status(401).json({ success: false, title: 'user.not.found', message: 'user.not.found', 'code': 50 });
    }
  })
});


/**
 * @swagger
 * /usuarios/auth/facebook/{code}:
 *   get:
 *     tags:
 *       - Usuario
 *     description: Cadastrar/Logar usuário usando o facebook
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: code
 *         type: string
 *         description: Código gerado pelo Facebook
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do user cadastrado, Token JWT
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Cadastrar/Logar usuário usando o facebook
router
  .get('/auth/facebook/:code', function (req, res) {
    if (!req.params.code) {
      return res
        .status(400)
        .json({success: false, 'message': 'code  was not sent', 'code': 63});
    }

    fbUtil
      .facebookMeRequest(req.params.code, function (fbUser, error) {

        if (!error) {
          var parameters = {
            attributes: [
              'idUsuario',
              'nome',
              'creditos',
              'provider',
              'email',
              'telefone',
              'senha',
              'role'
            ],
            where: {
              facebookId: fbUser.id
            }
          }
          models
            .usuario
            .get(parameters, function (user, error) {

              if (error) {
                return res
                  .status(error.code)
                  .json(errorUtil.jsonFromError(error, 'unable.to.authenticate.facebook', 500));
              }
              if (user) {
                generateToken(res, user);
              } else {
                //Cadastrar
                var name = fbUser.name;
                var id = fbUser.id;
                var params = {
                  nome: name,
                  senha: id.substr(0, 9),
                  email: fbUser.email || (name.replace(/\s/g, '') + '@facebook.com').toLowerCase(),
                  provider: 'facebook',
                  facebookId: id,
                  imagemUrl: fbUser.picture.data.url
                }
                models
                  .usuario
                  .insert(params, function (user, error) {
                    if (error) {
                      return res
                        .status(error.code || 400)
                        .json(errorUtil.jsonFromError(error, 'facebook.login.error', 400));
                    }
                    generateToken(res, user);
                    var auth = {
                      success: true,
                      expires: expiresDate,
                      token: 'JWT ' + token,
                      user: user.dataValues
                    };
                    return res
                      .status(200)
                      .json(auth);
                  })
              }
            })
        } else {
          return res
            .status(500)
            .json({success: false, title: 'unable.to.login', message: 'facebook.login.failed'});
        }
      })
  });





/**
 * @swagger
 * /usuarios/{idUsuario}:
 *   put:
 *     tags:
 *       - Usuario
 *     description: Atualizar dados do usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: string
 *         description: idUsuario, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: telefone
 *         type: string
 *         description: Alterar telefone do user
 *         in: formData
 *         required: false
 *       - name: cpf
 *         type: string
 *         description: CPF , somente numeros
 *         in: formData
 *         required: false
 *       - name: imagemUrl
 *         type: string
 *         description: Url para imagem de usuário
 *         in: formData
 *         required: false
 *       - name: nome
 *         type: string
 *         description: Alterar nome do user
 *         in: formData
 *         required: false
 *       - name: genero
 *         type: string
 *         description: Genero M-F.
 *         in: formData
 *         required: false
 *       - name: senha
 *         type: string
 *         description: Alterar a senha do user
 *         in: formData
 *         required: false
 *       - name: promocao
 *         type: boolean
 *         description: Se o usuário deseja receber promoçōes
 *         in: formData
 *         required: false
 *       - name: notificar
 *         type: boolean
 *         description: Se o usuário deseja ser notificado
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Atualizar dados
router.put('/:idUsuario', roles.checkRole('usuario', 'idUsuario'), function (req, res, next) {

  if (!req.params.idUsuario) {
    return res.status(400).json({ success: false, 'message': 'idUsuario was not sent', 'code': 63 });
  }
  //Atualizando dados do user
  var whereParams = {
    idUsuario: req.params.idUsuario,
    provider : 'cadastro'
  }
  var updateFields = {};

  //Se o usuario informou o nome
  if (req.body.nome) {
    updateFields['nome'] = req.body.nome;
  }
  //Se o usuario informou o telefone
  if (req.body.telefone) {
    updateFields['telefone'] = req.body.telefone;
  }

   //Se o usuario informou o cpf
  if (req.body.cpf) {
    updateFields['cpf'] = req.body.cpf;
  }

  //Se o usuario informou a imagem
  if (req.body.imagemUrl) {
    updateFields['imagemUrl'] = req.body.imagemUrl;
  }
  //Se o usuario quiser ser notificado
  if (req.body.notificar) {
    updateFields['notificar'] = req.body.notificar;
  }
  //Se o usuario informou o genero
  if (req.body.genero) {
    updateFields['genero'] = req.body.genero;
  }

  //Se o usuario quiser receber promocoes
  if (req.body.promocao) {
    updateFields['promocao'] = req.body.promocao;
  }

  //Se o usuario quiser atualizar a senha
  if(req.body.senha){
     var pass = models.usuario.generateHash(req.body.senha);
     updateFields['senha'] = pass;
  }

  models.usuario.updateData(whereParams, updateFields, function (data, error) {
    if (error) {
      return res.status(400).json(errorUtil.jsonFromError(error, 'unable.to.update.user', 400));
    }
    return res.status(200).json(data);

  })
});


/**
 * @swagger
 * /usuarios/forgot:
 *   post:
 *     tags:
 *       - Usuario
 *     description: Esqueci a senha, usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação, envio de email para o usuário
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Usuário não existente, usuário de login social tentando resetar senha
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */


//Esqueci a senha

router.post('/forgot', function (req, res) {
  if (!req.body.email) {
    return res.status(400).json({ success: false, 'message': 'email was not sent', 'code': 63 });
  }

  var params = {
    attributes: ['email', 'idUsuario', 'provider','ativo'],
    where: {
      email: req.body.email
    }
  }
  models.usuario.get(params, function (user, error) {
    if (error) {
      return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.forgot.pass', 500));
    }
    if (!user) {
      return res.status(401).json({ 'message': 'email.not.sent.server.error', 'success': false, 'code': 1 })
    }
    if (user.provider == 'facebook') {
      return res.status(401).json({ 'message': 'social.login.forgot.error', 'success': false, 'code': 2 })
    }
    if (user.ativo ==false) {
      return res.status(401).json({ success: false, title: 'inactive.user', message: 'inactive.user', 'code': 3 });
    }
    //Criando token
    crypto.randomBytes(20, function (err, buf) {
      if (err) {
        return res.status(500).json(error);
      }
      var token = buf.toString('hex');
      //Salvando dados no banco para resetar a senha posteriormente
      var timeValid = Date.now() + 3600000;  //Tempo no qual o token estará valido' : 1 hora
      models.usuario.updateData({ idUsuario: user.idUsuario }, {
        resetPasswordToken: token,
        resetPasswordExpires: timeValid
      }, function (updatedUser, error) {
        if (error) {
		  console.log(error);
          return res.status(500).json({ 'message': 'email.not.sent.server.error', 'success': false, 'code': 4 })
        }
		//Mandar email
        emailUtils.sendResetPassEmail(token, user.email, req.headers.host, 'usuarios', function (status, error) {
          if (error) {
			console.log(error);
            return res.status(500).json({ 'message': 'email.not.sent.mgError', 'success': false, 'code': 5 })
          }
          return res.status(200).json({ 'message': 'email.sent', 'success': true });
        })
      })
    })
  })
});

//Mostrar a pagina para resetar a senha
router.get('/reset/:token', function (req, res) {
  var params = {
    attributes: ['email', 'resetPasswordToken', 'resetPasswordExpires'],
    where: {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }
  }
  models.usuario.get(params, function (user, error) {
	console.log(error);
    if (error) {
      return res.render('index', {
        title: 'Erro ao alterar senha.',
        message: 'O token está incorreto, ou fora do prazo de validade'
      })
    }
	console.log(user);
    if (!user) {
      return res.render('index', {
        title: 'Erro ao alterar senha.',
        message: 'Usuário nāo encontrado.'
      })
    }
    return res.render('reset', {
      title: 'Resetar Senha'
    });
  })
});


//Resetar a senha de fato

router.post('/reset/:token', function (req, res) {
  if (!req.body.password || !req.body.confirm) {
    return res.render('index', {
      title: 'Erro ao alterar senha.',
      message: 'As senhas nāo foram enviadas'
    })
  }
  if (req.body.password != req.body.confirm) {
    return res.render('index', {
      title: 'Erro ao alterar senha.',
      message: 'As senhas digitadas nāo sāo iguais.'
    })
  }

  var params = {
    attributes: ['email', 'resetPasswordToken', 'resetPasswordExpires', 'nome'],
    where: {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }
  }
  models.usuario.get(params, function (user, error) {
    if (error) {
      return res.render('index', {
        title: 'Erro ao alterar senha.',
        message: 'O token está incorreto, ou fora do prazo de validade'
      })
    }
    if (!user) {
      return res.render('index', {
        title: 'Erro ao alterar senha.',
        message: 'Usuário nāo encontrado.'
      })
    }
    var whereParams = { email: user.email }
    var updateFields = {
      senha: models.usuario.generateHash(req.body.password),
      resetPasswordToken: ''
    }
    models.usuario.updateData(whereParams, updateFields, function (data, error) {
      if (error) {
        return res.render('index', {
          title: 'Erro ao alterar senha.',
          message: error.message
        })
      }
      //Renderizar página para sucesso
      //Mandar email
      emailUtils.sendResetSucessEmail(user.email, req.headers.host, user.nome, function (status, error) {
        if (error) {
          return res.render('index', {
            title: 'Senha alterada com sucesso!',
            message: 'A senha foi alterada, mas houveram problemas ao enviar o email de confirmaçāo.'
          })
        }
        //Renderizar página para sucesso
        return res.render('index', {
          title: 'Senha alterada com sucesso!',
          message: 'Tudo certo, agora você pode fazer login pelo aplicativo.'
        })
      })
    })
  })
});


/**
 * @swagger
 * /usuarios/{idUsuario}/enderecos:
 *   post:
 *     tags:
 *       - Usuario
 *     description: Cadastrar endereço do usuário
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: string
 *         description: idUsuario, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idCidade
 *         type: string
 *         description: idCidade da Cidade do Endereço
 *         in: formData
 *         required: true
 *       - name: titulo
 *         type: string
 *         description: Titulo do endereço
 *         in: formData
 *         required: true
 *       - name: endereco
 *         type: string
 *         description: Endereço
 *         in: formData
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Numero da rua, se houver.
 *         in: formData
 *         required: false
 *       - name: complemento
 *         type: string
 *         description: Complemento do endereço
 *         in: formData
 *         required: false
 *       - name: bairro
 *         type: string
 *         description: Bairro do endereço
 *         in: formData
 *         required: true
 *       - name: cep
 *         type: string
 *         description: CEP do Endereço, Ex - 70130-070
 *         in: formData
 *         required: true
 *       - name: referencia
 *         type: string
 *         description: Referência do Endereço
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do endereço cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Cadastrar endereco
router.post('/:idUsuario/enderecos', roles.checkRole('usuario', 'idUsuario'), function (req, res, next) {

  if (!req.params.idUsuario || !req.body.idCidade) {
    return res.status(400).json({ success: false, 'message': 'idUsuario or idCidade was not sent', 'code': 63 });
  }

  var parameters = {
    titulo: req.body.titulo,
    endereco: req.body.endereco,
    bairro: req.body.bairro,
    cep: req.body.cep,
    referencia: req.body.referencia,
    cidadeID: req.body.idCidade,
    usuarioIdUsuario: req.params.idUsuario,
    numero : req.body.numero,
    complemento : req.body.complemento,
    dono: 'usuario'
  }




  models.endereco.insert(parameters, function (data, error) {
    if (error) {
		function capitalizeFirstLetter(string) {
			return string.charAt(0).toUpperCase() + string.slice(1);
		}
		return res.status(403).json({'title' : 'Erro ao Cadastrar', 'message' : capitalizeFirstLetter(error.errors[0]['path'])+ ' inválido!' , 'code' : 500});
    }
    return res.status(200).json(data);
  })

});

/**
* @swagger
* /usuarios/{idUsuario}/enderecos/{idEndereco}:
*   put:
*     tags:
*       - Usuario
*     description: Alterar endereço do usuário
*     produces:
*       - application/json
*     parameters:
*       - name: idUsuario
*         type: string
*         description: idUsuario, obrigatório
*         in: path
*         required: true
*       - name: idEndereco
*         type: string
*         description: idEndereco do endereco a ser alterado
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*       - name: idCidade
*         type: string
*         description: idCidade da Cidade do Endereço
*         in: formData
*         required: false
*       - name: titulo
*         type: string
*         description: Titulo do endereço
*         in: formData
*         required: false
*       - name: endereco
*         type: string
*         description: Endereço
*         in: formData
*         required: false
*       - name: numero
*         type: string
*         description: Numero da rua, se houver.
*         in: formData
*         required: false
*       - name: complemento
*         type: string
*         description: Complemento do endereço
*         in: formData
*         required: false
*       - name: bairro
*         type: string
*         description: Bairro do endereço
*         in: formData
*         required: false
*       - name: cep
*         type: string
*         description: CEP do Endereço, Ex - 70130-070
*         in: formData
*         required: false
*       - name: referencia
*         type: string
*         description: Referência do Endereço
*         in: formData
*         required: false
*     responses:
*       200:
*         description: Dados do endereço alterado
*       400:
*         description: Faltaram parametros na requisição, ou erro na validacao
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Endereco'
*/

//Alterar endereco
router.put('/:idUsuario/enderecos/:idEndereco', roles.checkRole('usuario', 'idUsuario'), function (req, res, next) {
  if (!req.params.idUsuario || !req.params.idEndereco) {
    return res.status(400).json({ success: false, 'message': 'idUsuario or idEndereco was not sent', 'code': 63 });
  }
  var updateFields = {};
  //Se o titulo foi informado
  if (req.body.titulo) {
    updateFields['titulo'] = req.body.titulo;
  }
  //Se o endereco foi passado
  if (req.body.endereco) {
    updateFields['endereco'] = req.body.endereco;
  }
  //Se o bairro foi passado
  if (req.body.bairro) {
    updateFields['bairro'] = req.body.bairro;
  }

  //Se o numero foi passado
  if (req.body.numero) {
    updateFields['numero'] = req.body.numero;
  }

  //Se o complemento foi passado
  if (req.body.complemento) {
    updateFields['complemento'] = req.body.complemento;
  }

  //Se a cidade foi informada
  if (req.body.idCidade) {
    updateFields['cidadeID'] = req.body.idCidade;
  }
  //Se o cep foi passado
  if (req.body.cep) {
    updateFields['cep'] = req.body.cep;
  }
  //Se a referencia foi passada
  if (req.body.referencia) {
    updateFields['referencia'] = req.body.referencia;
  }

  var whereParams = {
    idEndereco: req.params.idEndereco,
    ativo: true
  }
  models.endereco.updateData(whereParams, updateFields, function (data, error) {
    if (error) {
      return res.status(400).json(errorUtil.jsonFromError(error, 'update.endereco.error', 400));
    }
    return res.status(200).json(data);
  })
});

/**
* @swagger
* /usuarios/{idUsuario}/enderecos/{idEndereco}:
*   delete:
*     tags:
*       - Usuario
*     description: Deletar endereço do usuário
*     produces:
*       - application/json
*     parameters:
*       - name: idUsuario
*         type: string
*         description: idUsuario, obrigatório
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*       - name: idEndereco
*         type: string
*         description: idEndereco do endereco a ser deletado
*         in: path
*         required: true
*     responses:
*       204:
*         description: Endereço deletado
*       400:
*         description: Faltaram parametros na requisição, ou erro na validacao
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Endereco'
*/

//Deletar endereco
router.delete('/:idUsuario/enderecos/:idEndereco', roles.checkRole('usuario', 'idUsuario'), function (req, res, next) {
  if (!req.params.idUsuario || !req.params.idEndereco) {
    return res.status(400).json({ success: false, 'message': 'idUsuario or idEndereco was not sent', 'code': 63 });
  }
  var updateField = { ativo: false }
  var whereParams = { idEndereco: req.params.idEndereco, usuarioIdUsuario: req.params.idUsuario }
  models.endereco.updateData(whereParams, updateField, function (data, error) {
    if (error) {
      return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.delete.endereco', 500));
    }
    return res.status(200).json(data);
  })
});


/**
* @swagger
* /usuarios/{idUsuario}/enderecos:
*   get:
*     tags:
*       - Usuario
*     description: Buscar endereços do usuário
*     produces:
*       - application/json
*     parameters:
*       - name: idUsuario
*         type: string
*         description: idUsuario, obrigatório
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*     responses:
*       200:
*         description: Endereços do usuário
*       400:
*         description: Faltaram parametros na requisição, ou erro na validacao
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Endereco'
*/

//Buscar Endereço
router.get('/:idUsuario/enderecos', roles.checkRole('usuario', 'idUsuario'), function (req, res) {
  var params = {
    where: {
      usuarioIdUsuario: req.params.idUsuario,
      ativo: true
    },
    order: [['createdAt', 'DESC']],
    include: [{
      model: models.cidade,
      attributes: ['ID', 'nome', 'estadoID'],
      include: [{ model: models.estado, attributes: ['nome', 'acronimo'] }]
    }]
  }
  models.endereco.getAll(params, function (data, error) {
    if (error) {
      return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.get.endereco.user', 500));
    }
    return res.status(200).json(data);
  })
});

/**
* @swagger
* /usuarios/{idUsuario}/entregadores:
*   get:
*     tags:
*       - Usuario
*     description: Buscar entregadores perto do raio de localizaçāo onde o usuário está
*     produces:
*       - application/json
*     parameters:
*       - name: idUsuario
*         type: string
*         description: idUsuario, obrigatório
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*       - name: latitude
*         type: float
*         description: Latitude do usuário
*         in: query
*         required: true
*       - name: longitude
*         type: float
*         description: Longitude do usuário
*         in: query
*         required: true
*       - name: radius
*         type: float
*         description: Raio de busca em KM, maximo 30
*         in: query
*         required: true
*     responses:
*       200:
*         description: Dados sobre os entregadores perto do Usuário
*       400:
*         description: Faltaram parametros na requisição, ou erro na validacao
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Usuario'
*/


//Buscar entregadores ao redor do user
router.get('/:idUsuario/entregadores', roles.checkRole('usuario', 'idUsuario'), function (req, res) {
  if (!req.query.latitude || !req.query.longitude || !req.query.radius) {
    return res.status(400).json({ success: false, 'message': 'latitude or longitude or radius was not sent', 'code': 63 });
  }
  if (!req.params.idUsuario) {
    return res.status(400).json({ success: false, 'message': 'idUsuario  was not sent', 'code': 63 });
  }

  if (req.query.radius > 30 || req.query.radius < 0) {
    return res.status(400).json({ success: false, 'message': 'invalid value for radius', 'code': 63 });
  }
  //Buscar
  models.entregadorData.getAround(req.query.latitude, req.query.longitude, req.query.radius, function (result, error) {
    if (error) {
      return res.status(500).json(errorUtil.jsonFromError(error, 'unable.to.query.location', 500));
    }
    return res.status(200).json(result);
  })
});

/**
* @swagger
* /usuarios/profiles/{idUsuario}:
*   get:
*     tags:
*       - Usuario
*     description: Buscar perfil de um usuário
*     produces:
*       - application/json
*     parameters:
*       - name: idUsuario
*         type: string
*         description: idUsuario, obrigatório
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*     responses:
*       200:
*         description: Perfil do usuário
*       400:
*         description: Faltaram parametros na requisição, ou erro na validacao
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Usuario'
*/


//Perfil do user
router.get('/profiles/:idUsuario', passport.authenticate('jwt', { session: false }), function (req, res) {
  if (!req.params.idUsuario) {
    return res.status(400).json({ success: false, 'message': 'idUsuario  was not sent', 'code': 63 });
  }
  var params = {
    attributes: ['idUsuario', 'nome', 'telefone'],
    where: {
      idUsuario: req.params.idUsuario
    }
  }

  models.usuario.get(params, function (user, error) {
    if (error) {
      return res.status(500).json(errorUtil.jsonFromError(error, 'get.usuario.profile.error', 500));
    }

    return res.status(200).json(user);
  })
});

/**
* @swagger
* /usuarios/{idUsuario}/creditos:
*   get:
*     tags:
*       - Usuario
*     description: Buscar valor dos créditos de um usuário
*     produces:
*       - application/json
*     parameters:
*       - name: idUsuario
*         type: string
*         description: idUsuario, obrigatório
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*     responses:
*       200:
*         description: Valor dos creditos do usuario
*       400:
*         description: Faltaram parametros na requisição, ou erro na validacao
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Usuario'
*/
//Buscar créditos
router.get('/:idUsuario/creditos',roles.checkRole('usuario', 'idUsuario'),function(req,res){
  if (!req.params.idUsuario) {
    return res.status(400).json({ success: false, 'message': 'idUsuario  was not sent', 'code': 63 });
  }
  var params = {
    attributes : ['idUsuario','creditos'],
    where : {
      idUsuario : req.params.idUsuario
    }
  }
  models.usuario.get(params,function(data,error){
    if (error) {
      return res.status(500).json(errorUtil.jsonFromError(error, 'get.usuario.creditos.error', 500));
    }
    return res.status(200).json(data);
  })
})


/**
 * @swagger
 * /usuarios/upload:
 *   post:
 *     tags:
 *       - Usuario
 *     description: Enviar fotos do usuario
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: file
 *         type: file
 *         description: Imagem a ser enviada, formato jpg, com compressao e tamanho 200x200
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Upload de imagens
router.post('/upload', function (req, res) {
  var path = req.headers.host + '/usuarios/images/';
  uploadUtils.handleUpload(req, res, path);
});


/**
 * @swagger
 * /usuarios/upload/base64:
 *   post:
 *     tags:
 *       - Usuario
 *     description: Enviar fotos do usuario em formato base64
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fileString
 *         type: string
 *         description: Imagem a ser enviada, formato string.
 *         in: formData
 *         required: true
 *       - name: imageName
 *         type: string
 *         description: Nome da imagem.
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Upload de imagens
router.post('/upload/base64', function (req, res) {
  var path = req.headers.host + '/usuarios/images/';
  uploadUtils.handleBase64ImageUpload(req,res,path);
});





/**
 * @swagger
 * /usuarios/images/{fileName}:
 *   get:
 *     tags:
 *       - Usuario
 *     description: Buscar imagem do usuario
 *     produces:
 *       - image/jpg
 *     parameters:
 *       - name: fileName
 *         type: string
 *         description: nome do arquivo com extensao jpg, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Imagem
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Download
router.get('/images/:fileName', passport.authenticate('jwt', {session: false}), function (req, res) {
  if (!req.params.fileName) {
    return res
      .status(400)
      .json({success: false, 'message': 'fileName.was.not.sent', 'code': 63});
  }
  uploadUtils.getFile(req, res, req.params.fileName);
});

module.exports = router;
