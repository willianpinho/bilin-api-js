"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var errorUtil = require('../utils/errorUtils');
var roles = require('../utils/roles');
var pedidoController = require('../utils/pedidoController');
var passport = require('passport');
var jwt = require('jsonwebtoken');


/**
 * @swagger
 * definitions:
 *   Produto:
 *     properties:
 *       nome:
 *         type: string
 *       descricao:
 *         type: string
 *       valor:
 *         type: float
 *       marca:
 *         type: string
 *       cidadeId:
 *         type: integer
 *       ativo:
 *         type: boolean
 */


/**
 * @swagger
 * /produtos/{idCidade}:
 *   get:
 *     tags:
 *       - Produto
 *     description: Buscar produto por cidade
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idCidade
 *         type: string
 *         description: idCidade, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do produto
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Produto'
 */
//Buscar produto por cidade
router.get('/:idCidade', passport.authenticate('jwt', { session: false }), function(req, res) {
    if (!req.params.idCidade) {
        return res.status(400).json({ success: false, "message": "idCidade was not sent", "code": 63 });
    }
    var params = {
        attributes : ['idProduto','nome','descricao','marca','valor'],
        where: {
            cidadeID: req.params.idCidade
        }
    }
    models.produto.get(params, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "produto.get.error", 500));
        }
        return res.status(200).json(data);
    });
})


module.exports = router;
