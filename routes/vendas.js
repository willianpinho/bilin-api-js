"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var roles = require('../utils/roles');
var pedidoController = require('../utils/pedidoController');


/*
Status da venda
0- Cancelada
1- Concretizada
*/



/**
 * @swagger
 * definitions:
 *   Venda:
 *     properties:
 *       idVenda:
 *         type: integer
 *       status:
 *         type: integer
 *       valor:
 *         type: float
 *       ativo:
 *         type: boolean
 */


/**
 * @swagger
 * /vendas/{idEntregador}/{idPedido}:
 *   post:
 *     tags:
 *       - Venda
 *     description: Cadastrar venda do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: string
 *         description: idPedido, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da venda cadastrada
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Venda'
 */


//Concretizar venda
router.post('/:idEntregador/:idPedido', roles.checkRole("entregador", "idEntregador"), function(req, res) {
    if (!req.params.idEntregador || !req.params.idPedido) {
        return res.status(400).json({ success: false, "message": "idEntregador or idPedido was not sent", "code": 63 });
    }

    //Buscar dados do pedido
    var queryPedido = {
        attributes: ['statusPedidoUsuario', 'statusPedidoEntregador'],
        where: {
            entregadorIdEntregador: req.params.idEntregador,
            idPedido: req.params.idPedido
        }
    }
    models.pedido.get(queryPedido, function(data, error) {
        //Verificando condicoes para realizar update
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "unable.to.get.pedido.entregador", 500));
        }
        //Se está tudo certo e o pedido já foi entregue
        if (data.statusPedidoUsuario == 7 && data.statusPedidoEntregador == 7) {
            //Criando parametros para inserir a venda
            var parameters = {
                entregadorIdEntregador: req.params.idEntregador,
                pedidoIdPedido: req.params.idPedido,
                valor: req.body.valor,
                status: 1
            }
            //Verificando se o entregador pertence a uma distribuidora
            pedidoController.getDistribuidoraId(req.params.idEntregador, function(status, data) {
                if (status) {
                    parameters['distribuidoraIdDistribuidora'] = data.distribuidoraIdDistribuidora;
                }
                //Cadastrar de fato a venda
                models.venda.insert(parameters, function(data, error) {
                    if (error) {
                        return res.status(400).json(errorUtil.jsonFromError(error, "insert.venda.entregador.error", 400));
                    }
                    return res.status(200).json(data);
                })
            })
        }
    })
});


/**
 * @swagger
 * /vendas/{idEntregador}/{idVenda}:
 *   put:
 *     tags:
 *       - Venda
 *     description: Atualizar dados de uma venda do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: idTelefone
 *         type: string
 *         description: idVenda, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: status
 *         type: string
 *         description: Status da venda. 0 - Cancelada, 1- Concretizada
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Status da atualizacao
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Venda'
 */

//Alterar status da venda
router.put('/:idEntregador/:idVenda', roles.checkRole("entregador", "idEntregador"), function(req, res) {
    if (!req.params.idEntregador || !req.params.idVenda || !req.body.status) {
        return res.status(400).json({ success: false, "message": "idEntregador or idVenda or status was not sent", "code": 63 });
    }
    //Atualizando dados do user
    var whereParams = {
        entregadorIdEntregador: req.params.idEntregador,
        idVenda: req.params.idVenda
    }
    var updateFields = {};

    //Atualizar o status
    var status = parseInt(req.body.status);

    if (status < 0 || status > 1) {
        return res.status(400).json({ success: false, "message": "invalid.status.for.venda", "code": 59 });
    }
    updateFields['status'] = status;

    models.venda.updateData(whereParams, updateFields, function(data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "update.venda.entregador.error", 400));
        }
        return res.status(200).json(data);
    })
});



/**
 * @swagger
 * /vendas/{idEntregador}:
 *   put:
 *     tags:
 *       - Venda
 *     description: Vendas do entregador, em um periodo específico
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: dataInicio
 *         type: string
 *         description: data de inicio, (Formato Iso) - DEFAULT ONTEM
 *         in: query
 *         required: false
 *       - name: dataFim
 *         type: string
 *         description: data do fim, (Formato Iso) - DEFAULT HOJE
 *         in: query
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 15, o Limite é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados das vendas
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Venda'
 */
//Vendas por entregador
router.get('/:idEntregador', function(req, res) {
    if (!req.params.idEntregador) {
        return res.status(400).json({ success: false, "message": "idEntregador  was not sent", "code": 63 });
    }
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var startDate = Date.parse(req.query.dataInicio) || Date(new Date() - 24 * 60 * 60 * 1000);
    var endDate = Date.parse(req.query.dataFim) || new Date();

    var params = {
        where: {
            createdAt: {
                $lt: endDate,
                $gt: startDate
            },
            entregadorIdEntregador: req.params.idEntregador
        },
        offset: offset,
        limit: limit
    }

    models.venda.getAll(params, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.vendas.entregador.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /vendas/{idUsuario}:
 *   put:
 *     tags:
 *       - Venda
 *     description: Vendas para um usuário, em um periodo específico
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: string
 *         description: idUsuario, obrigatório
 *         in: path
 *         required: true
 *       - name: dataInicio
 *         type: string
 *         description: data de inicio, (Formato Iso) - DEFAULT ONTEM
 *         in: path
 *         required: false
 *       - name: dataFim
 *         type: string
 *         description: data do fim, (Formato Iso) - DEFAULT HOJE
 *         in: path
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 15, o Limite é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados das vendas
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Venda'
 */

//Vendas para usuarios
router.get('/:idUsuario', function(req, res) {
    if (!req.params.idUsuario) {
        return res.status(400).json({ success: false, "message": "idUsuario  was not sent", "code": 63 });
    }

    var startDate = Date.parse(req.body.dataInicio) || Date(new Date() - 24 * 60 * 60 * 1000);
    var endDate = Date.parse(req.body.dataFim) || new Date();
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var params = {
        where: {
            createdAt: {
                $lt: endDate,
                $gt: startDate
            },
            usuarioIdUsuario: req.params.idUsuario
        },
        offset: offset,
        limit: limit,
    }

    models.venda.getAll(params, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.vendas.usuario.error", 500));
        }
        return res.status(200).json(data);
    })
});

module.exports = router;
