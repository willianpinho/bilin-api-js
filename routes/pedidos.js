"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var pedidoController = require('../utils/pedidoController');
var errorUtil = require('../utils/errorUtils');
var roles = require('../utils/roles');

/*

Status dos pedidos
 0 - Desconhecido
 1- Agendado
 2 - Pronto (Aguardando o entregador)
 3 - Aceito
 4- Nao Aceito
 5 - Cancelado por parte do entregador
 6 - Em transito
 7- Entregue
 8 - Nāo Entregue
 9- Cancelado por parte do usuário
 10 - Cancelado pelo sistema (Erro ao criar pedido)

*/

/*
Tipos de Pagamento
1 - Dinheiro
2 - Cartão de Débito
3 - Cartão de Crédito
*/

/*
Status do Pagamento
0 - Desconhecido
1- Processando
2- Aprovado
3- Nāo Aprovado
4 - Cancelado
*/

// /**
//  * @swagger
//  * definitions:
//  *   Pedido:
//  *     properties:
//  *       idPedido:
//  *         type: integer
//  *       quantidade:
//  *         type: integer
//  *       data:
//  *         type: string
//  *       tipoPagamento:
//  *         type: integer
//  *         description: 1 - Dinheiro, 2 - Cartão de Débito, 3 - Cartão de Crédito
//  *       statusPagamento:
//  *         type: integer
//  *         description: 0 - Desconhecido,1- Processando,2- Aprovado,3- Nāo
// Aprovado,4 - Cancelado
//  *       valorPagamento:
//  *         type: float
//  *       statusPedidoUsuario:
//  *         type: integer
//  *         description: 0 - Desconhecido, 1- Agendado, 2 - Pronto (Aguardando o entregador), 3 - Aceito, 4- Nao Aceito, 5 - Cancelado por parte do entregador, 6 - Em transito,7- Entregue,8 - Nāo Entregue,9- Cancelado por parte do usuário
//  *       statusPedidoEntregador:
//  *         type: integer
//  *         description: 0 - Desconhecido, 1- Agendado, 2 - Pronto (Aguardando o entregador), 3 - Aceito, 4- Nao Aceito, 5 - Cancelado por parte do entregador, 6 - Em transito,7- Entregue, 8 - Nāo Entregue,9- Cancelado por
// parte do usuário
//  *       paymentMethod:
//  *         type: string
//  *         description: Para o payU VISA,MASTERCARD,AMEX, DINERS,HIPERCARD,ELO
//  *       creditCard:
//  *         type: body
//  *         description: JSON com dados do Cartao
//  *       observacaoUsuario:
//  *         type: string
//  *       valorProduto:
//  *         type: float
//  *       valorDesconto:
//  *         type: float
//  *       valorFinal:
//  *         type: float
//  *       usoCredito:
//  *         type: boolean
//  *       promoCode:
//  *         type: string
//  *       valorCredito:
//  *         type: float
//  *       ativo:
//  *         type: boolean
//  *       troco:
//  *         type: float
//  * @swagger
//  * /pedidos/usuarios/{idUsuario}:
//  *   post:
//  *     tags:
//  *       - Pedido
//  *     description: Cadastrar Pedido
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: idUsuario
//  *         type: integer
//  *         description: Id do usuário
//  *         in: path
//  *         required: true
//  *       - name: idEntregador
//  *         type: integer
//  *         description: Id do entregador
//  *         in: formData
//  *         required: true
//  *       - name: idEndereco
//  *         type: integer
//  *         description: Id do endereco da entrega
//  *         in: formData
//  *         required: true
//  *       - name: idProduto
//  *         type: integer
//  *         description: Id do produto, para que seja possivel pegar o valor do gás
//  *         in: formData
//  *         required: true
//  *       - name: Authorization
//  *         type: string
//  *         description: Token de Autenticação
//  *         in: header
//  *         required: true
//  *       - name: quantidade
//  *         type: integer
//  *         description: Quantidade de produtos do pedido
//  *         in: formData
//  *         required: true
//  *       - name: data
//  *         type: string
//  *         description: Data do pedido formato ISO, Se for agendamento
//  *         in: formData
//  *         required: false
//  *       - name: tipoPagamento
//  *         type: integer
//  *         description: Tipo de pagamento do pedido - 1 - Dinheiro, 2 - Cartão de Débito, 3 - Cartão de Crédito
//  *         in: formData
//  *         required: true
//  *       - name: paymentMethod
//  *         type: string
//  *         description: Tipo de metodo de pagamento - Obrigatório para o Tipo de pagamento 2 VISA,MASTERCARD,AMEX ,DINERS,HIPERCARD,ELO
//  *         in: formData
//  *         required: false
//  *       - name: creditCard
//  *         type: string
//  *         description: Objeto JSON com os dados do cartão de crédito
//  *         in: formData
//  *         required: false
//  *       - name: creditValue
//  *         type: float
//  *         description: Caso for utilizar crédito, especificar o valor a ser
// utilizado.
//  *         in: formData
//  *         required: false
//  *       - name: agendamento
//  *         type: boolean
//  *         description: Se o pedido é agendamento
//  *         in: formData
//  *         required: false
//  *       - name: usoCredito
//  *         type: boolean
//  *         description: Se o usuário vai utilizar créditos
//  *         in: formData
//  *         required: false
//  *       - name: pesarGas
//  *         type: boolean
//  *         description: Se o usuário quer que pese o botijāo
//  *         in: formData
//  *         required: false
//  *       - name: instalarGas
//  *         type: boolean
//  *         description: Se o usuário quer que instale o botijāo
//  *         in: formData
//  *         required: false
//  *       - name: promoCode
//  *         type: string
//  *         description: Codigo promocional, caso o usuário queira usar.
//  *         in: formData
//  *         required: false
//  *       - name: observacaoUsuario
//  *         type: string
//  *         description: Observação sobre o pedido.Por parte do usuário.
//  *         in: formData
//  *         required: false
//  *       - name: troco
//  *         type: float
//  *         description: Troco necessário
//  *         in: formData
//  *         required: false
//  *     responses:
//  *       200:
//  *         description: Dados do pedido
//  *       400:
//  *         description: Faltaram parametros na requisição, ou erro na
// validacao
//  *       401:
//  *         description: Token Inválido
//  *       403:
//  *         description: Nível de acesso insuficiente, ou acesso a dados de
// usuário diferente
//  *       500:
//  *         description: Erro interno do servidor
//  *         schema:
//  *           $ref: '#/definitions/Pedido'  */

/**
 * @swagger
 * definitions:
 *   Pedido:
 *     properties:
 *       idPedido:
 *         type: integer
 *       quantidade:
 *         type: integer
 *       data:
 *         type: string
 *       tipoPagamento:
 *         type: integer
 *         description: 1 - Dinheiro, 2 - Cartão de Débito, 3 - Cartão de Crédito
 *       statusPagamento:
 *         type: integer
 *         description: 0 - Desconhecido,1- Processando,2- Aprovado,3- Nāo Aprovado,4 - Cancelado
 *       valorPagamento:
 *         type: float
 *       statusPedidoUsuario:
 *         type: integer
 *         description: 0 - Desconhecido, 1- Agendado, 2 - Pronto (Aguardando o entregador), 3 - Aceito, 4- Nao Aceito, 5 - Cancelado por parte do entregador, 6 - Em transito,7- Entregue,8 - Nāo Entregue,9- Cancelado por parte do usuário
 *       statusPedidoEntregador:
 *         type: integer
 *         description: 0 - Desconhecido, 1- Agendado, 2 - Pronto (Aguardando o entregador), 3 - Aceito, 4- Nao Aceito, 5 - Cancelado por parte do entregador, 6 - Em transito,7- Entregue,8 - Nāo Entregue,9- Cancelado por parte do usuário
 *       paymentMethod:
 *         type: integer
 *         description:  VISA - 1,MASTERCARD - 2,AMERICAN_EXPRESS - 37,AURA - 45,DINERS - 55,HIPERCARD - 56,ELO - 63
 *       creditCard:
 *         type: body
 *         description: JSON com dados do Cartao
 *       observacaoUsuario:
 *         type: string
 *       valorProduto:
 *         type: float
 *       valorDesconto:
 *         type: float
 *       valorFinal:
 *         type: float
 *       usoCredito:
 *         type: boolean
 *       promoCode:
 *         type: string
 *       valorCredito:
 *         type: float
 *       ativo:
 *         type: boolean
 *       troco:
 *         type: float
 */

/**
 * @swagger
 * /pedidos/usuarios/{idUsuario}:
 *   post:
 *     tags:
 *       - Pedido
 *     description: Cadastrar Pedido
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: formData
 *         required: true
 *       - name: idEndereco
 *         type: integer
 *         description: Id do endereco da entrega
 *         in: formData
 *         required: true
 *       - name: idProduto
 *         type: integer
 *         description: Id do produto, para que seja possivel pegar o valor do gás
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: quantidade
 *         type: integer
 *         description: Quantidade de produtos do pedido
 *         in: formData
 *         required: true
 *       - name: data
 *         type: string
 *         description: Data do pedido formato ISO, Se for agendamento
 *         in: formData
 *         required: false
 *       - name: tipoPagamento
 *         type: integer
 *         description: Tipo de pagamento do pedido - 1 - Dinheiro, 2 - Cartão de Débito, 3 - Cartão de Crédito
 *         in: formData
 *         required: true
 *       - name: paymentMethod
 *         type: string
 *         description: Tipo de metodo de pagamento - Obrigatório para o Tipo de pagamento 2, opções -> VISA - 1,MASTERCARD - 2,AMERICAN_EXPRESS - 37,AURA - 45,DINERS - 55,HIPERCARD - 56,ELO - 63
 *         in: formData
 *         required: false
 *       - name: creditCard
 *         type: body
 *         description: Objeto JSON com os dados do cartão de crédito //holder,number,securityCode,maturityMonth,maturityYear
 *         in: formData
 *         required: false
 *       - name: viewed
 *         type: boolean
 *         description: Caso o pagamento for via bCash (Tipo de pagamento 2 ), enviar se o usuário viu os termos de uso
 *         in: formData
 *         required: false
 *       - name: accepted
 *         type: boolean
 *         description: Caso o pagamento for via bCash (Tipo de pagamento 2 ), enviar se o usuário aceitou os termos de uso
 *         in: formData
 *         required: false
 *       - name: creditValue
 *         type: float
 *         description: Caso for utilizar crédito, especificar o valor a ser utilizado.
 *         in: formData
 *         required: false
 *       - name: agendamento
 *         type: boolean
 *         description: Se o pedido é agendamento
 *         in: formData
 *         required: false
 *       - name: usoCredito
 *         type: boolean
 *         description: Se o usuário vai utilizar créditos
 *         in: formData
 *         required: false
 *       - name: pesarGas
 *         type: boolean
 *         description: Se o usuário quer que pese o botijāo
 *         in: formData
 *         required: false
 *       - name: instalarGas
 *         type: boolean
 *         description: Se o usuário quer que instale o botijāo
 *         in: formData
 *         required: false
 *       - name: promoCode
 *         type: string
 *         description: Codigo promocional, caso o usuário queira usar.
 *         in: formData
 *         required: false
 *       - name: observacaoUsuario
 *         type: string
 *         description: Observação sobre o pedido.Por parte do usuário.
 *         in: formData
 *         required: false
 *       - name: troco
 *         type: float
 *         description: Troco necessário ao entregador.
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do pedido
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Pegar o preço do botijão antes
router.post('/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res, next) {
    if (!req.body.idProduto) {
        return res
            .status(400)
            .json({success: false, "message": "idProduto  was not sent", "code": 63});
    }
    var params = {
        attributes: ['valor'],
        where: {
            idProduto: req.body.idProduto
        }
    }
    models
        .produto
        .get(params, function (data, error) {
            if (error) {
                return res
                    .status(400)
                    .json(errorUtil.jsonFromError(error, "pedido.insert.error", 120));
            }
            req.body.valorProduto = data.valor;
            return next('route');
        })
});

//Cadastrar pedido após buscar preço do botijão
router.post('/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res, next) {

    if (!req.params.idUsuario || !req.body.tipoPagamento || !req.body.quantidade || !req.body.idEndereco) {
        return res
            .status(400)
            .json({
                success: false,
                "message": "idUsuario or idEndereco or tipoPagamento or quantidade  was not " +
                        "sent",
                "code": 63
            });
    }

    //Se o usuário quiser usar crédito e código promocional
    if (req.body.usoCredito && req.body.promoCode) {
        return res
            .status(400)
            .json({success: false, "message": "cannot.use.promoCode.and.credito", "code": 63});
    }
    var isUsed = false;
    //Se vamos utilizar créditos
    if (req.body.usoCredito) {
        isUsed = true;
        pedidoController.handlePedidoCreditos(req, res, function (pedido, paymentResponse, error) {

            if (error) {
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.insert.error.useCredit", 120));
            }
            if (pedido) {
                //Tudo certo, Completar dados do pedido
                pedidoController
                    .completePedido(pedido.idPedido, function (data, error) {
                        if (error) {
                            return res
                                .status(200)
                                .json({
                                    "pedido": pedido,
                                    "message": errorUtil.jsonFromError(error, "pedido.insert.complete.error", 120)
                                });
                        }
                        if (data) {
                            return res
                                .status(200)
                                .json({"pedido": data, "payment": paymentResponse})
                        } else {
                            return res
                                .status(200)
                                .json({"pedido": pedido, "payment": paymentResponse})
                        }
                    })
            } else {
                //Pedido cancelado por erro mo pagamento
                return res.status(400).json({"payment" : payment, "mensagem" : "pedido.create.error.payment"})
            }

        })
    } else if (req.body.promoCode && isUsed == false) {
        //Se vamos utilizar o código de desconto
        pedidoController
            .handlePedidoPromoCode(req, res, function (pedido, paymentResponse, error) {
                if (error) {
                    return res
                        .status(500)
                        .json(errorUtil.jsonFromError(error, "pedido.insert.error.usePromoCode", 120));
                }
                if (pedido) {
                    //Tudo certo, Completar dados do pedido
                    pedidoController
                        .completePedido(pedido.idPedido, function (data, error) {
                            if (error) {
                                return res
                                    .status(200)
                                    .json({
                                        "pedido": pedido,
                                        "message": errorUtil.jsonFromError(error, "pedido.insert.complete.error", 120)
                                    });
                            }
                            if (data) {
                                return res
                                    .status(200)
                                    .json({"pedido": data, "payment": paymentResponse})
                            } else {
                                return res
                                    .status(200)
                                    .json({"pedido": pedido, "payment": paymentResponse})
                            }
                        })
                } else {
                   //Pedido cancelado por erro mo pagamento
                   return res.status(400).json({"payment" : payment, "mensagem" : "pedido.create.error.payment"})
                }
            })

    } else {

        //Pedido normal sem Codigo ou desconto
        pedidoController
            .handlePedidoNormal(req, res, function (pedido, paymentResponse, error) {

                console.log("------pedido----------");
                console.log(pedido);
                console.log("------pedido----------");
                console.log("------paymentResponse----------");
                console.log(paymentResponse);
                console.log("------paymentResponse----------");
                console.log("------error----------");
                console.log(error);
                console.log("------error----------");

                if (error) {
                    return res
                        .status(500)
                        .json(errorUtil.jsonFromError(error, "pedido.insert.error", 120));
                }
                if (pedido) {
                    //Tudo certo, Completar dados do pedido
                    pedidoController
                        .completePedido(pedido.idPedido, function (data, error) {

			    if (error) {
                                return res
                                    .status(200)
                                    .json({
                                        "pedido": pedido,
                                        "message": errorUtil.jsonFromError(error, "pedido.insert.complete.error", 120)
                                    });
                            }
                            if (data) {
                                return res
                                    .status(200)
                                    .json({"pedido": data, "payment": paymentResponse})
                            } else {
                                return res
                                    .status(200)
                                    .json({"pedido": pedido, "payment": paymentResponse})
                            }
                        })
                } else {
                  //Pedido cancelado por erro mo pagamento
                  return res.status(400).json({"payment" : paymentResponse, "mensagem" : "pedido.create.error.payment"})
                }
            })
    }

});

/**
 * @swagger
 * /pedidos/usuarios/{idUsuario}/{idPedido}:
 *   put:
 *     tags:
 *       - Pedido
 *     description: Alterar status do pedido (Usuário)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido
 *         in: path
 *         required: true
 *       - name: status
 *         type: integer
 *         description: Status para o pedido - Valores válidos ->   7- Entregue,  8 - Nāo Entregue, 9- Cancelado por parte do usuário.
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Atualizacao do status do pedido
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Alterar status de pedido - cliente
router.put('/usuarios/:idUsuario/:idPedido', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.params.idUsuario || !req.params.idPedido) {
        return res
            .status(400)
            .json({success: false, "message": "idUsuario was not sent", "code": 63});
    }
    var status = parseInt(req.body.status);
    if (!(status > 6 && status < 10)) {
        return res
            .status(400)
            .json({success: false, "message": "invalid.status.for.usuario", "code": 59});
    }
    //Buscar dados do pedido
    var queryPedido = {
        attributes: [
            'statusPedidoUsuario', 'statusPedidoEntregador'
        ],
        where: {
            usuarioIdUsuario: req.params.idUsuario,
            idPedido: req.params.idPedido
        }
    }
    models
        .pedido
        .get(queryPedido, function (data, error) {
            //Verificando condicoes para realizar update
            if (error) {
                return res
                    .status(400)
                    .json(errorUtil.jsonFromError(error, "unable.to.update.pedido.usuario", 400));
            }
            if (data.statusPedidoUsuario == 7 || data.statusPedidoUsuario == 9) {
                return res
                    .status(400)
                    .json({success: false, "message": "invalid.update.status.usuario", "code": 60});
            } else {
                //Update
                var whereParams = {
                    usuarioIdUsuario: req.params.idUsuario,
                    idPedido: req.params.idPedido
                }
                var updateFields = {};
                updateFields['statusPedidoUsuario'] = status;
                // Se o pedido teve entrega confirmada pelo usuario, o entregador confirma
                // automaticamente
                if (status == 7) {
                    updateFields['statusPedidoEntregador'] = status;
                }
                models
                    .pedido
                    .updateData(whereParams, updateFields, function (data, error) {
                        if (error) {
                            return res
                                .status(400)
                                .json(errorUtil.jsonFromError(error, "unable.to.update.pedido.usuario", 400));
                        }
                        return res
                            .status(200)
                            .json(data);
                    })
            }
        })

});

/**
 * @swagger
 * /pedidos/entregadores/{idEntregador}/{idPedido}:
 *   put:
 *     tags:
 *       - Pedido
 *     description: Alterar status do pedido (Entregador)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido
 *         in: path
 *         required: true
 *       - name: status
 *         type: integer
 *         description: Status para o pedido - Valores válidos -> 3 - Aceito, 4- Nao Aceito, 5 - Cancelado por parte do entregador, 6 - Em transito, 7- Entregue, 8 - Nāo Entregue
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Atualizacao do status do pedido
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */
//Alterar status de pedido - entregador
router.put('/entregadores/:idEntregador/:idPedido', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idEntregador || !req.params.idPedido) {
        return res
            .status(400)
            .json({success: false, "message": "idEntregador was not sent", "code": 63});
    }

    var status = parseInt(req.body.status);
    if (!(status > 2 && status < 9)) {
        return res
            .status(400)
            .json({success: false, "message": "invalid.update.status.entregador", "code": 59});
    }

    //Buscar dados do pedido
    var queryPedido = {
        attributes: [
            'statusPedidoUsuario', 'statusPedidoEntregador'
        ],
        where: {
            entregadorIdEntregador: req.params.idEntregador,
            idPedido: req.params.idPedido
        }
    }
    models
        .pedido
        .get(queryPedido, function (data, error) {
            //Verificando condicoes para realizar update
            if (error) {
                return res
                    .status(400)
                    .json(errorUtil.jsonFromError(error, "unable.to.update.pedido.entregador", 400));
            }
            if (data.statusPedidoUsuario == 5 || data.statusPedidoEntregador == 5 || data.statusPedidoEntregador == 10) {
                return res
                    .status(400)
                    .json({success: false, "message": "invalid.update.status.entregador", "code": 60});
            } else {
                //Update
                var whereParams = {
                    entregadorIdEntregador: req.params.idEntregador,
                    idPedido: req.params.idPedido
                }
                var updateFields = {};
                updateFields['statusPedidoEntregador'] = status;
                models
                    .pedido
                    .updateData(whereParams, updateFields, function (resp, error) {
                        if (error) {
                            return res
                                .status(400)
                                .json(errorUtil.jsonFromError(error, "unable.to.update.pedido.entregador", 400));
                        }
                        //Informar via e-mail a mudanca do status do pedido
                        pedidoController.completePedido(req.params.idPedido,function(repons,error){
                            pedidoController.createMessageUsuario(status, data.usuarioIdUsuario, function(result,error){
                                return res.status(200).json(data);
                            });
                        })


                    })
            }
        })
});

/**
 * @swagger
 * /pedidos/entregadores/{idEntregador}:
 *   get:
 *     tags:
 *       - Pedido
 *     description: Pedidos do entregador, com data de inicio, e paginaçāo
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *       - name: data
 *         type: string
 *         description: Data inicial da busca. default 1 dia antes
 *         in: formData
 *         required: false
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 15, o Limite É 100.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados dos pedidos
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Pedidos do entregador
router.get('/entregadores/:idEntregador/:mainActivity', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idEntregador) {
        return res
            .status(400)
            .json({success: false, "message": "idEntregador was not sent", "code": 63});
    }
    var offset = parseInt(req.query['offset'])
        ? parseInt(req.query['offset'])
        : 0;
    var limit = parseInt(req.query['limit'])
        ? parseInt(req.query['limit'])
        : 10;
    if (limit > 100) {
        return res
            .status(400)
            .json({success: false, "message": "limit.out.of.bounds", "code": 63});
    }
    var date = new Date();
    date.setDate(date.getDate() - 1);
    var data = Date.parse(req.body.data) || date;

    if(req.params.mainActivity == 1){
        var parameters = {
            where: {
                entregadorIdEntregador: req.params.idEntregador,
                    data: {
                        $gte: Date.parse(data)
                    },
                usuarioIdUsuario: {
                    $ne: null
                }
            },
            order: [
                ['createdAt', 'DESC']
            ],
            offset: offset,
            limit: limit,
            include: [
                {
                    model: models.endereco,
                    required: false,
                    attributes: [
                        'endereco', 'numero','bairro', 'cep', 'referencia'
                    ],
                    include: [
                        {
                            model: models.cidade,
                            attributes: [
                                'nome', 'estadoID'
                            ],
                            required: false,
                            include: [
                                {
                                    model: models.estado,
                                    attributes: [
                                        'nome', 'acronimo'
                                    ],
                                    required: false
                                }
                            ]
                        }
                    ]
                }, {
                    model: models.usuario,
                    attributes: ['nome', 'telefone']
                }
            ]
        };
    }else{
        var parameters = {
            where: {
                entregadorIdEntregador: req.params.idEntregador,
                /*
                    data: {
                        $gte: Date.parse(data)
                    },
                */
                usuarioIdUsuario: {
                    $ne: null
                }
            },
            order: [
                ['createdAt', 'DESC']
            ],
            offset: offset,
            //limit: limit,
            include: [
                {
                    model: models.endereco,
                    required: false,
                    attributes: [
                        'endereco', 'numero', 'bairro', 'cep', 'referencia'
                    ],
                    include: [
                        {
                            model: models.cidade,
                            attributes: [
                                'nome', 'estadoID'
                            ],
                            required: false,
                            include: [
                                {
                                    model: models.estado,
                                    attributes: [
                                        'nome', 'acronimo'
                                    ],
                                    required: false
                                }
                            ]
                        }
                    ]
                }, {
                    model: models.usuario,
                    attributes: ['nome', 'telefone']
                }
            ]
        };
    }
    models
        .pedido
        .getAll(parameters, function (data, error) {
            if (error) {
                console.log(error);
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.entregador.get.error", 120));
            }
            return res
                .status(200)
                .json(data);
        })
});

/**
 * @swagger
 * /pedidos/entregadores/{idEntregador}:
 *   get:
 *     tags:
 *       - Pedido
 *     description: Pedidos do entregador, apenas com paginaçāo
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 15, o Limite É 100.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados dos pedidos
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Pedidos do entregador
router.get('/entregadores/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idEntregador) {
        return res
            .status(400)
            .json({success: false, "message": "idEntregador.was.not.sent", "code": 63});
    }

    var offset = parseInt(req.query['offset'])
        ? parseInt(req.query['offset'])
        : 0;
    var limit = parseInt(req.query['limit'])
        ? parseInt(req.query['limit'])
        : 10;
    if (limit > 100) {
        return res
            .status(400)
            .json({success: false, "message": "limit.out.of.bounds", "code": 63});
    }

    var parameters = {
        where: {
            entregadorIdEntregador: req.params.idEntregador
        },
        order: [
            ['createdAt', 'DESC']
        ],
        offset: offset,
        limit: limit,
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'idEndereco','endereco', 'numero', 'bairro', 'cep', 'referencia', 
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    }
                ]
            }, {
                model: models.usuario,
                attributes: ['nome', 'telefone']
            }
        ]
    }
    models
        .pedido
        .getAll(parameters, function (data, error) {
            if (error) {
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.entregador.get.error", 120));
            }
            return res
                .status(200)
                .json(data);
        })
});


/**
 * @swagger
 * /pedidos/entregadores/{idEntregador}/{idPedido}:
 *   get:
 *     tags:
 *       - Pedido
 *     description: Buscar informacoes sobre um pedido especifico
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: Dados dos pedidos
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Pedido especifico do entregador
router.get('/entregadores/:idEntregador/:idPedido', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idEntregador || !req.params.idPedido) {
        return res
            .status(400)
            .json({success: false, "message": "idEntregador.or.idPedido.was.not.sent", "code": 63});
    }


    var parameters = {
        where: {
            entregadorIdEntregador: req.params.idEntregador,
            idPedido : req.params.idPedido
        },
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'endereco', 'numero', 'bairro', 'cep', 'referencia', 
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    }
                ]
            }, {
                model: models.usuario,
                attributes: ['nome', 'telefone']
            }
        ]
    }
    models
        .pedido
        .get(parameters, function (data, error) {
            if (error) {
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.entregador.get.pedido.error", 120));
            }
            return res
                .status(200)
                .json(data);
        })
});


/**
 * @swagger
 * /pedidos/usuarios/{idUsuario}:
 *   get:
 *     tags:
 *       - Pedido
 *     description: Pedidos do usuário, com busca a partir de data, e com paginaçāo
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: data
 *         type: string
 *         description: Data inicial da busca. default 1 dia antes
 *         in: formData
 *         required: false
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 15, o Limite É 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados dos pedidos
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Pedidos do cliente
router.get('/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.params.idUsuario) {
        return res
            .status(400)
            .json({success: false, "message": "idUsuario.was.not.sent", "code": 63});
    }
    var offset = parseInt(req.query['offset'])
        ? parseInt(req.query['offset'])
        : 0;
    var limit = parseInt(req.query['limit'])
        ? parseInt(req.query['limit'])
        : 10;
    if (limit > 50) {
        return res
            .status(400)
            .json({success: false, "message": "limit.out.of.bounds", "code": 63});
    }
    var date = new Date();
    date.setDate(date.getDate() - 1);
    var data = Date.parse(data) || date;
    var parameters = {
        where: {
            usuarioIdUsuario: req.params.idUsuario
            /*,
            data: {
                $gte: Date.parse(data)
            }*/
        },
        order: [
            ['createdAt', 'DESC']
        ],
        offset: offset,
        limit: limit,
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'titulo','endereco', 'numero', 'bairro', 'cep', 'referencia'
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.produto,
                                attributes: [
                                    'nome', 'descricao','valor'
                                ],
                                required: false
                            },
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    },
                ]
            }, {
                model: models.entregador,
                attributes: ['nome', 'imagemUrl']
            },
        ]
    }
    models
        .pedido
        .getAll(parameters, function (data, error) {
            if (error) {
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.usuario.get.error", 120));
            }
            return res
                .status(200)
                .json(data);
        })
});

/**
 * @swagger
 * /pedidos/usuarios/{idUsuario}:
 *   get:
 *     tags:
 *       - Pedido
 *     description: Pedidos do usuário, apenas com paginaçāo
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 15, o Limite É 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados dos pedidos
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Pedidos do cliente
router.get('/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.params.idUsuario) {
        return res
            .status(400)
            .json({success: false, "message": "idUsuario.was.not.sent", "code": 63});
    }
    var offset = parseInt(req.query['offset'])
        ? parseInt(req.query['offset'])
        : 0;
    var limit = parseInt(req.query['limit'])
        ? parseInt(req.query['limit'])
        : 10;
    if (limit > 50) {
        return res
            .status(400)
            .json({success: false, "message": "limit.out.of.bounds", "code": 63});
    }
    var parameters = {
        where: {
            usuarioIdUsuario: req.params.idUsuario
        },
        order: [
            ['createdAt', 'DESC']
        ],
        offset: offset,
        limit: limit,
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'titulo','endereco', 'numero', 'bairro', 'cep', 'referencia','complemento'
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    }
                ]
            }, {
                model: models.entregador,
                attributes: ['nome', 'imagemUrl']
            }
        ]
    }
    models
        .pedido
        .getAll(parameters, function (data, error) {
            if (error) {
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.usuario.get.error", 120));
            }
            return res
                .status(200)
                .json(data);
        })
});


/**
 * @swagger
 * /pedidos/usuarios/{idUsuario}/{idPedido}:
 *   get:
 *     tags:
 *       - Pedido
 *     description: Pedido especifico do usuario
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do pedido
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */

//Pedido especifico do cliente
router.get('/usuarios/:idUsuario/:idPedido/:changeDeliveryMan/:lat?/:long?/:radius?', roles.checkRole("usuario", "idUsuario"), function (req, res) {

    if (!req.params.idUsuario || !req.params.idPedido || !req.params.idPedido) {
        return res
            .status(400)
            .json({success: false, "message": "idUsuario.or.idPedido.was.not.sent", "code": 63});
    }

    if(req.params.changeDeliveryMan == 0){

        var parameters = {
            where: {
                usuarioIdUsuario: req.params.idUsuario,
                idPedido : req.params.idPedido
            },
            include: [
                {
                    model: models.endereco,
                    required: false,
                    attributes: [
                        'endereco', 'numero', 'bairro', 'cep', 'referencia'
                    ],
                    include: [
                        {
                            model: models.cidade,
                            attributes: [
                                'nome', 'estadoID'
                            ],
                            required: false,
                            include: [
                                {
                                    model: models.estado,
                                    attributes: [
                                        'nome', 'acronimo'
                                    ],
                                    required: false
                                }
                            ]
                        }
                    ]
                }, {
                    model: models.entregador,
                    attributes: ['nome', 'imagemUrl']
                }
            ]
        };

        models
            .pedido
            .get(parameters, function (data, error) {
                if (error) {
                    return res
                        .status(500)
                        .json(errorUtil.jsonFromError(error, "pedido.usuario.get.pedido.error", 120));
                }
                return res
                    .status(200)
                    .json(data);
        });

    }else{

        if (!req.params.lat || !req.params.long || !req.params.radius) {
            return res
                .status(400)
                .json({success: false, "message": "Lat.or.Long.or.Radius.was.not.sent", "code": 63});
        }

        var parameters = {
            where: {
                idPedido : req.params.idPedido
            },
            include: [
                {
                    model: models.endereco,
                    required: false,
                    attributes: [
                        'endereco', 'numero', 'bairro', 'cep', 'referencia'
                    ],
                    include: [
                        {
                            model: models.cidade,
                            attributes: [
                                'nome', 'estadoID'
                            ],
                            required: false,
                            include: [
                                {
                                    model: models.estado,
                                    attributes: [
                                        'nome', 'acronimo'
                                    ],
                                    required: false
                                }
                            ]
                        }
                    ]
                }, {
                    model: models.entregador,
                    attributes: ['nome', 'imagemUrl']
                }
            ]
        };

        models
        .pedido
        .get(parameters, function (data, error) {
            if (error) {
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.usuario.get.pedido.error", 120));
            }

            var updateFields = {};

            var entregadorIdEntregadorListaTentativas = "";

            if(data.entregadorIdEntregadorListaTentativas == null || data.entregadorIdEntregadorListaTentativas.length == 0){
                if(data.entregadorIdEntregador != null)
                    entregadorIdEntregadorListaTentativas = data.entregadorIdEntregador;
            }else{
                entregadorIdEntregadorListaTentativas = data.entregadorIdEntregadorListaTentativas;
                if(data.entregadorIdEntregador != null)
                    entregadorIdEntregadorListaTentativas+=","+data.entregadorIdEntregador;
            }
            console.log(data.entregadorIdEntregador);
            updateFields['entregadorIdEntregadorListaTentativas'] = entregadorIdEntregadorListaTentativas;

            //Buscar
            models.entregadorData.getAroundNotIn(req.params.lat, req.params.long, req.params.radius,entregadorIdEntregadorListaTentativas, function (result, error) {

                if (error) {
                    return res.status(500).json(errorUtil.jsonFromError(error, "unable.to.query.location", 500));
                }

                if(result.length > 0){

                    updateFields['entregadorIdEntregador'] = result[0].entregadorIdEntregador;

                }else{
                    updateFields['entregadorIdEntregador'] = null;
                }

                //Update
                var whereParams = {
                    idPedido: req.params.idPedido
                };

                models
                    .pedido
                    .updateData(whereParams, updateFields, function (data, error) {

                        if (error) {
                            return res
                                .status(400)
                                .json(errorUtil.jsonFromError(error, "unable.to.update.pedido.usuario", 400));
                        }

                        models
                        .pedido
                        .get(parameters, function (data, error) {
                            if (error) {
                                return res
                                    .status(500)
                                    .json(errorUtil.jsonFromError(error, "pedido.usuario.get.pedido.error", 120));
                            }

                            return res
                                .status(200)
                                .json(data);


                        });

                    });

            })

        });

    }

});

/**
 * @swagger
 * /pedidos/promoCode/{idUsuario}:
 *   get:
 *     tags:
 *       - Pedido
 *     description: Receber dados do código promocional para estimar valor do pedido
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: promoCode
 *         type: string
 *         description: Codigo promocional
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do Codigo de desconto
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Pedido'
 */
//Estimar valor de desconto
router.get('/promoCode/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.params.idUsuario || !req.body.promoCode) {
        return res
            .status(400)
            .json({success: false, "message": "idUsuario or promoCode was not sent", "code": 63});
    }
    pedidoController
        .estimateCodeValue(req.body.promoCode, req.params.idUsuario, function (status, data, error) {
            if (error) {
                return res
                    .status(500)
                    .json(errorUtil.jsonFromError(error, "pedido.get.promocode.error", 120));
            }
            if (data) {
                return res
                    .status(200)
                    .json(data);
            }
        })
})

module.exports = router;
