"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var errorUtil = require('../utils/errorUtils');
var roles = require('../utils/roles');
var uploadUtils = require('../utils/uploadUtils');
var nconf = require('nconf');
var path = require("path");
var env = process.env.NODE_ENV || "development";
var config = path.join(__dirname, '..', 'config', 'config.json');
var crypto = require('crypto');
var bodyParser  = require('body-parser').json({limit:'100mb'});
var emailUtils = require('../utils/emails.js');
var fs = require('fs');
nconf
  .argv()
  .env();
nconf.file({file: config});

//Gerar Token de Autenticação para entregador
var generateToken = function (res, user) {
  var expiresIn = 15778476000; //6Meses
  var currentDate = new Date();
  var expiresDate = new Date(currentDate.getTime() + expiresIn);
  var payload = {
    id: user.dataValues.idEntregador,
    mail: user.dataValues.email,
    role: user.dataValues.role
  }

  //Retornar o token
  var token = jwt.sign(payload, nconf.get('secret'), { // Gerando um token baseado nos dados do usuário
    expiresIn: expiresIn // in seconds
  });
  delete user.dataValues["senha"];
  res
    .status(200)
    .json({
      success: true,
      expires: expiresDate,
      token: 'JWT ' + token,
      user: user.dataValues
    });
}

/**
 * @swagger
 * definitions:
 *   Entregador:
 *     properties:
 *       nome:
 *         type: string
 *       imagemUrl:
 *         type: string
 *       email:
 *         type: string
 *       senha:
 *         type: string
 *       imgCpf:
 *         type: string
 *       imgRg:
 *         type: string
 *       imgCnh:
 *         type: string
 *       imgCrv:
 *         type: string
 *       isAutonomo:
 *         type: boolean
 *       veiculoNome:
 *         type: string
 *       veiculoPlaca:
 *         type: string
 *       veiculoMarca:
 *         type: string
 *       ativo:
 *         type: boolean
 */

/**
 * @swagger
 * definitions:
 *   Veiculo:
 *     properties:
 *       marca:
 *         type: string
 *       modelo:
 *         type: string
 *       placa:
 *         type: string
 *       ativo:
 *         type: boolean
 */

/**
 * @swagger
 * /entregadores:
 *   post:
 *     tags:
 *       - Entregador
 *     description: Cadastrar entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: nome
 *         type: string
 *         description: Nome obrigatório
 *         in: formData
 *         required: true
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *       - name: cpf
 *         type: string
 *         description: CPF obrigatório, somente numeros
 *         in: formData
 *         required: true
 *       - name: veiculoNome
 *         type: string
 *         description: Nome do Veiculo do entregador
 *         in: formData
 *         required: true
 *       - name: veiculoMarca
 *         type: string
 *         description: Marca do Veiculo do entregador
 *         in: formData
 *         required: true
 *       - name: veiculoPlaca
 *         type: string
 *         description: Placa do Veiculo do entregador
 *         in: formData
 *         required: true
 *       - name: senha
 *         type: string
 *         description: Senha obrigatório
 *         in: formData
 *         required: true
 *       - name: imagemUrl
 *         type: string
 *         description: Link para a imagem do user
 *         in: formData
 *         required: false
 *       - name: isAutonomo
 *         type: boolean
 *         description: Se o entregador é autônomo ou trabalha para uma empresa
 *         in: formData
 *         required: false
 *       - name: idDistribuidora
 *         type: integer
 *         description: Id da distribuidora, se ele nao for autônomo
 *         in: formData
 *         required: false
 *       - name: imgCpf
 *         type: string
 *         description: URL da imagem do CPF (Realizar Upload na rota de upload)
 *         in: formData
 *         required: false
 *       - name: imgRg
 *         type: string
 *         description: URL da imagem do RG (Realizar Upload na rota de upload)
 *         in: formData
 *         required: false
 *       - name: imgCnh
 *         type: string
 *         description: URL da imagem do CNH(Realizar Upload na rota de upload)
 *         in: formData
 *         required: false
 *       - name: imgCrv
 *         type: string
 *         description: URL da imagem do CRV (Realizar Upload na rota de upload)
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do entregador cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Cadastrar entregador
router.post('/', function (req, res, next) {
  var parameters = {
    nome: req.body.nome,
    senha: req.body.senha,
    email: req.body.email,
    cpf: req.body.cpf,
    imagemUrl: req.body.imagemUrl,
    imgCnh: req.body.imgCnh,
    imgCpf: req.body.imgCpf,
    imgCrv: req.body.imgCrv,
    imgRg: req.body.imgRg,
    provider: "cadastro",
    isAutonomo: req.body.isAutonomo,
    veiculoMarca: req.body.veiculoMarca,
    veiculoPlaca: req.body.veiculoPlaca,
    veiculoNome: req.body.veiculoNome,
    ativo: true
  };

  if (req.body.idDistribuidora) {
    parameters['distribuidoraIdDistribuidora'] = req.body.idDistribuidora;
  }

  function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
  }

  models
    .entregador
    .insert(parameters, function (user, error) {

      if (error) {

        console.log("__________________");
        var message = error.errors[0]['message'];
        var path = error.errors[0]['path'];

        if(message == 'Validation len failed'){
          if(path == 'senha'){
              message = 'O campo '+path+' deve ter entre 4 e 25 caracteres';
          }else{
              message = 'O campo '+path+' está muito pequeno.';
          }
        }else{
          if(path == 'cpf'){
              message = path+' inválido ou já cadastrado';
          }else{
            message = path+' inválido';
          }
        }

        var error = {'message':capitalizeFirstLetter(message)};

        return res
          .status(500)
          .json(errorUtil.jsonFromError(error, "unable.to.register.entregador", 500));
      }


      var dataParameter = {
        entregadorIdEntregador: user.idEntregador
      }

      models
        .entregadorData
        .insert(dataParameter, function (userData, error) {

          if (error) {
              console.log("aqui 2");
            return res
              .status(500)
              .json(errorUtil.jsonFromError(error, "unable.to.register.entregadorData", 500));
          }
          return res
            .status(200)
            .json(user);
        })

    })
});

/**
 * @swagger
 * /entregadores/auth:
 *   post:
 *     tags:
 *       - Entregador
 *     description: Autenticar entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *       - name: senha
 *         type: string
 *         description: Senha obrigatório
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do entregador cadastrado, token JWT
 *       401:
 *         description: Token Inválido, usuário não encontrado, ou usuário ou senha incorreta
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Autenticacao entregador
router.post('/auth', function (req, res) {
  var parameters = {
    attributes: [
      'idEntregador',
      'nome',
      'imagemUrl',
      'email',
      'senha',
      'role',
      'veiculoNome',
      'veiculoPlaca',
      'veiculoMarca'
    ],
    where: {
      email: req.body.email
    }
  }
  models
    .entregador
    .get(parameters, function (user, error) {
      if (error) {
        return res
          .status(500)
          .json(error);
      }
      if (user) {
        if (user.ativo == false) {
          return res
            .status(401)
            .json({success: false, title: "inactive.user", message: "inactive.user", "code": 150});
        }
        if (user.validatePassword(req.body.senha)) {
          generateToken(res, user);
        } else {
          res
            .status(401)
            .json({success: false, title: "unable.to.login", message: "incorrect.username.password", "code": 52});
        }
      } else {
        res
          .status(401)
          .json({success: false, title: "entregador.not.found", message: "entregador.not.found", "code": 50});
      }
    })
});

/**
 * @swagger
 * /entregadores/{idEntregador}:
 *   put:
 *     tags:
 *       - Entregador
 *     description: Atualizar dados do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: email
 *         type: string
 *         description: Atualizar e-mail
 *         in: formData
 *         required: false
 *       - name: senha
 *         type: string
 *         description: Atualizar a senha
 *         in: formData
 *         required: false
 *       - name: veiculoNome
 *         type: string
 *         description: Nome do Veiculo do entregador
 *         in: formData
 *         required: true
 *       - name: veiculoMarca
 *         type: string
 *         description: Marca do Veiculo do entregador
 *         in: formData
 *         required: true
 *       - name: veiculoPlaca
 *         type: string
 *         description: Placa do Veiculo do entregador
 *         in: formData
 *         required: true
 *       - name: imgCpf
 *         type: string
 *         description: Atualizar a imagem do CPF
 *         in: formData
 *         required: false
 *       - name: imgRg
 *         type: string
 *         description: Atualizar a imagem do RG
 *         in: formData
 *         required: false
 *       - name: imgCnh
 *         type: string
 *         description: Atualizar a imagem da CNH
 *         in: formData
 *         required: false
 *       - name: imgCrv
 *         type: string
 *         description: Atualizar a imagem CRV
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do entregador alterado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Usuario'
 */

//Atualizar dados
router.put('/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res, next) {

  if (!req.params.idEntregador) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador was not sent", "code": 63});
  }
  //Atualizando dados do entregador
  var whereParams = {
    idEntregador: req.params.idEntregador
  }
  var updateFields = {};
  //Atualizar o email
  if (req.body.email) {
    updateFields['email'] = req.body.email;
  }
  //Atualizar a imgCpf
  if (req.body.imgCpf) {
    updateFields['imgCpf'] = req.body.imgCpf;
  }
  //Atualizar a imgRg
  if (req.body.imgRg) {
    updateFields['imgRg'] = req.body.imgRg;
  }
  //Atualizar a imgCnh
  if (req.body.imgCnh) {
    updateFields['imgCnh'] = req.body.imgCnh;
  }
  //Atualizar a imgCrv
  if (req.body.imgCrv) {
    updateFields['imgCrv'] = req.body.imgCrv;
  }
  //Atualizar o veiculoMarca
  if (req.body.veiculoMarca) {
    updateFields['veiculoMarca'] = req.body.veiculoMarca;
  }
  //Atualizar o veiculoNome
  if (req.body.veiculoNome) {
    updateFields['veiculoNome'] = req.body.veiculoNome;
  }
  //Atualizar o veiculoPlaca
  if (req.body.veiculoPlaca) {
    updateFields['veiculoPlaca'] = req.body.veiculoPlaca;
  }
  //Se o entregador quiser atualizar a senha
  if (req.body.senha) {
    var pass = models
      .entregador
      .generateHash(req.body.senha);
    updateFields['senha'] = pass;
  }
  models
    .entregador
    .updateData(whereParams, updateFields, function (data, error) {
      if (error) {
        return res
          .status(400)
          .json(errorUtil.jsonFromError(error, "unable.to.update.entregador", 400));
      }
      return res
        .status(200)
        .json(data);

    })
});

/**
 * @swagger
 * /entregadores/{idEntregador}/data:
 *   put:
 *     tags:
 *       - Entregador
 *     description: Atualizar dados de disponibilidade, e de número de botijões do entregador.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: botijoes
 *         type: integer
 *         description: Atualizar numero de botijoes
 *         in: formData
 *         required: false
 *       - name: disponivel
 *         type: boolean
 *         description: Atualizar status de disponibilidade
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Atualizar status do entregador
router.put('/:idEntregador/data', roles.checkRole("entregador", "idEntregador"), function (req, res, next) {
  console.log("params--------",req.body);
  if (!req.params.idEntregador) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador was not sent", "code": 63});
  }
  var whereParams = {
    entregadorIdEntregador: req.params.idEntregador
  }

  var updateFields = {};
  //Atualizar o número de botijoes
  if (req.body.botijoes) {
    updateFields['botijoes'] = parseInt(req.body.botijoes);
  }

  if (req.body.idCity) {
    updateFields['botijoes'] = parseInt(req.body.idCity);
  }

  //Atualizar se o entregador está disponivel
  if (req.body.disponivel) {
    updateFields['disponivel'] = req.body.disponivel;
  }
  if (req.body.latitude && req.body.longitude) {
    updateFields['point'] = { type: 'Point',
      coordinates: [
      req.body.latitude,
      req.body.longitude ]
    };
  }
  models
    .entregadorData
    .updateData(whereParams, updateFields, function (data, error) {
      if (error) {
        return res
          .status(400)
          .json(errorUtil.jsonFromError(error, "unable.to.update.entregadorData", 400));
      }
      return res
        .status(200)
        .json(data);
    })

});

/**
 * @swagger
 * /entregadores/locations/{idEntregador}:
 *   put:
 *     tags:
 *       - Entregador
 *     description: Atualizar localizaçāo do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: latitude
 *         type: float
 *         description: Atualizar latitude
 *         in: formData
 *         required: true
 *       - name: longitude
 *         type: float
 *         description: Atualizar longitude
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados de localizaçãp do entregador alterados
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Informar localizacao
router.put('/locations/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res, next) {
  if (!req.body.latitude || !req.body.longitude) {
    return res
      .status(400)
      .json({success: false, "message": "latitude or longitude was not sent", "code": 63});
  }
  var whereParams = {
    entregadorIdEntregador: req.params.idEntregador
  }
  var point = {
    type: 'Point',
    coordinates: [req.body.latitude, req.body.longitude]
  };

  var updateFields = {};
  updateFields['point'] = point;

  models
    .entregadorData
    .updateData(whereParams, updateFields, function (data, error) {
      if (error) {
        return res
          .status(400)
          .json(errorUtil.jsonFromError(error, "unable.to.update.entregador.location", 400));
      }
      return res
        .status(200)
        .json(data);
    })
});

/**
 * @swagger
 * /entregadores/forgot:
 *   post:
 *     tags:
 *       - Entregador
 *     description: Esqueci a senha, entregador.
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação, envio de email para o entregador
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido, usuário não encontrado
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Esqueci a senha

router.post('/forgot', function (req, res) {
  if (!req.body.email) {
    return res
      .status(400)
      .json({success: false, "message": "email.was.not.sent", "code": 63});
  }

  var params = {
    attributes: [
      'email', 'idEntregador'
    ],
    where: {
      email: req.body.email
    }
  }
  models
    .entregador
    .get(params, function (user, error) {
      if (error) {
        return res
          .status(500)
          .json(errorUtil.jsonFromError(error, "unable.to.forgot.pass", 500));
      }
	  
      if (!user) {
        return res
          .status(401)
          .json({"message": 'Usuário não encontrado.', "success": false, "code": 70})
      }
      //Criando token
      crypto
        .randomBytes(20, function (err, buf) {
          if (err) {
            return res
              .status(500)
              .json(error);
          }
          var token = buf.toString('hex');
          //Salvando dados no banco para resetar a senha posteriormente
          var timeValid = Date.now() + 3600000; //Tempo no qual o token estará valido' : 1 hora
          models
            .entregador
            .updateData({
              idEntregador: user.idEntregador
            }, {
              resetPasswordToken: token,
              resetPasswordExpires: timeValid
            }, function (data, error) {
              if (error) {
				console.log(error);
                return res
                  .status(500)
                  .json({"message": 'email.not.sent.server.error', "success": false, "code": 71})
              }
              //Mandar email
              emailUtils
                .sendResetPassEmail(token, user.email, req.headers.host, "entregadores", function (status, error) {
                  if (error) {
				    console.log(error);
                    return res
                      .status(500)
                      .json({"message": 'email.not.sent.mgError', "success": false, "code": 72})
                  }
                  return res
                    .status(200)
                    .json({"message": 'email.sent', "success": true});
                })
            })
        })
    })
});

//Mostrar a pagina para resetar a senha
router.get('/reset/:token', function (req, res) {
  var params = {
    attributes: [
      'email', 'resetPasswordToken', 'resetPasswordExpires'
    ],
    where: {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }
  }
  models
    .entregador
    .get(params, function (user, error) {
      if (error) {
        return res.render('index', {
          title: 'Erro ao alterar senha.',
          message: 'O token está incorreto, ou fora do prazo de validade'
        })
      }
      if (!user) {
        return res.render('index', {
          title: 'Erro ao alterar senha.',
          message: 'Entregador nāo encontrado.'
        })
      }
      return res.render('reset', {title: 'Resetar Senha'});
    })
});

//Resetar a senha de fato

router.post('/reset/:token', function (req, res) {
  console.log(req.body.password);
  if (!req.body.password || !req.body.confirm) {
    return res.render('index', {
      title: 'Erro ao alterar senha.',
      message: 'As senhas nāo foram enviadas'
    })
  }
  if (req.body.password != req.body.confirm) {
    return res.render('index', {
      title: 'Erro ao alterar senha.',
      message: 'As senhas digitadas nāo sāo iguais.'
    })
  }

  var params = {
    attributes: [
      'email', 'resetPasswordToken', 'resetPasswordExpires', 'nome'
    ],
    where: {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }
  }
  models
    .entregador
    .get(params, function (user, error) {
      if (error) {
        return res.render('index', {
          title: 'Erro ao alterar senha.',
          message: 'O token está incorreto, ou fora do prazo de validade'
        })
      }
      if (!user) {
        return res.render('index', {
          title: 'Erro ao alterar senha.',
          message: 'Entregador nāo encontrado.'
        })
      }
      var whereParams = {
        email: user.email
      }
      var updateFields = {
        senha: models
          .entregador
          .generateHash(req.body.password),
        resetPasswordToken: ''
      };
	  console.log(updateFields);
      models
        .entregador
        .updateData(whereParams, updateFields, function (data, error) {
          if (error) {
		  console.log(error);
            return res.render('index', {
              title: 'Erro ao alterar senha.',
              message: error.message
            })
          }
          //Renderizar página para sucesso Mandar email
          emailUtils
            .sendResetSucessEmail(user.email, req.headers.host, user.nome, function (status, error) {
              if (error) {
                return res.render('index', {
                  title: 'Senha alterada com sucesso!',
                  message: 'A senha foi alterada.'
                })
              }
              //Renderizar página para sucesso
              return res.render('index', {
                title: 'Senha alterada com sucesso!',
                message: 'Tudo certo, agora você pode fazer login pelo aplicativo.'
              })
            })
        })
    })
});

//Adicionar telefone
/**
 * @swagger
 * /entregadores/telefones/{idEntregador}:
 *   post:
 *     tags:
 *       - Entregador
 *     description: Cadastrar telefone do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Número do telefone com ddd
 *         in: formData
 *         required: true
 *       - name: descricao
 *         type: string
 *         description: descricao do telefone
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do telefone cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

router.post('/telefones/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res) {
  if (!req.params.idEntregador) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador was not sent", "code": 63});
  }
  var parameters = {
    entregadorIdEntregador: req.params.idEntregador,
    numero: req.body.numero,
    descricao: req.body.descricao
  }
  models
    .telefone
    .insert(parameters, function (data, error) {
      if (error) {
        return res
          .status(400)
          .json(errorUtil.jsonFromError(error, "insert.telefone.entregador.error", 400));
      }
      return res
        .status(200)
        .json(data);
    })
});

/**
 * @swagger
 * /entregadores/telefones/{idEntregador}/{idTelefone}:
 *   put:
 *     tags:
 *       - Entregador
 *     description: Atualizar dados de um telefone do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: idTelefone
 *         type: string
 *         description: idTelefone, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Número do telefone com ddd
 *         in: formData
 *         required: true
 *       - name: descricao
 *         type: string
 *         description: descricao do telefone
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Status da atualizacao
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

//Alterar telefone
router.put('/telefones/:idEntregador/:idTelefone', roles.checkRole("entregador", "idEntregador"), function (req, res) {
  if (!req.params.idEntregador || !req.params.idTelefone) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador or idTelefone was not sent", "code": 63});
  }
  //Atualizando dados do user
  var whereParams = {
    entregadorIdEntregador: req.params.idEntregador,
    idTelefone: req.params.idTelefone
  }
  var updateFields = {};

  //Atualizar o numero
  if (req.body.numero) {
    updateFields['numero'] = req.body.numero;
  }
  //Atualizar a descricao
  if (req.body.descricao) {
    updateFields['descricao'] = req.body.descricao;
  }
  models
    .telefone
    .updateData(whereParams, updateFields, function (data, error) {
      if (error) {
        return res
          .status(400)
          .json(errorUtil.jsonFromError(error, "update.telefone.entregador.error", 400));
      }
      return res
        .status(200)
        .json(data);
    })
});

/**
 * @swagger
 * /entregadores/telefones/{idEntregador}/{idTelefone}:
 *   delete:
 *     tags:
 *       - Entregador
 *     description: Deletar dados de um telefone do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: idTelefone
 *         type: string
 *         description: idTelefone, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

//Deletar telefone
router.delete('/telefones/:idEntregador/:idTelefone', function (req, res) {
  if (!req.params.idEntregador || !req.params.idTelefone) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador or idTelefone was not sent", "code": 63});
  }
  //Atualizando dados do user
  var whereParams = {
    entregadorIdEntregador: req.params.idEntregador,
    idTelefone: req.params.idTelefone
  }
  var updateFields = {};
  updateFields['ativo'] = false;

  models
    .telefone
    .updateData(whereParams, updateFields, function (data, error) {
      if (error) {
        return res
          .status(400)
          .json(errorUtil.jsonFromError(error, "delete.telefone.entregador.error", 400));
      }
      return res
        .status(200)
        .json(data);
    })
});

/**
 * @swagger
 * /entregadores/telefones/{idEntregador}:
 *   get:
 *     tags:
 *       - Entregador
 *     description: Buscar telefones do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados dos telefones
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

//Buscar telefones
router.get('/telefones/:idEntregador', function (req, res) {
  if (!req.params.idEntregador) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador was not sent", "code": 63});
  }
  var parameters = {
    where: {
      entregadorIdEntregador: req.params.idEntregador,
      ativo: true
    },
    order: [
      ['createdAt', 'DESC']
    ]
  }
  models
    .telefone
    .getAll(parameters, function (data, error) {
      if (error) {
        return res
          .status(500)
          .json(errorUtil.jsonFromError(error, "get.telefone.entregador.error", 500));
      }
      return res
        .status(200)
        .json(data);
    })
});

/**
 * @swagger
 * /entregadores/profiles/{idEntregador}:
 *   get:
 *     tags:
 *       - Entregador
 *     description: Buscar perfil de um entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do perfil fo entregador
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Perfil do entregador
router.get('/profiles/:idEntregador', passport.authenticate('jwt', {session: false}), function (req, res) {
  if (!req.params.idEntregador) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador was not sent", "code": 63});
  }
  models
    .entregador
    .getProfile(req.params.idEntregador, function (data, error) {
      if (error) {
        return res
          .status(500)
          .json(errorUtil.jsonFromError(error, "get.entregador.profile.error", 500));
      }
      return res
        .status(200)
        .json(data);
    })
});

// /**
//  * @swagger
//  * /entregadores/upload:
//  *   post:
//  *     tags:
//  *       - Entregador
//  *     description: Enviar fotos do entregador
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: Authorization
//  *         type: string
//  *         description: Token de Autenticação
//  *         in: header
//  *         required: true
//  *       - name: file
//  *         type: file
//  *         description: Imagem a ser enviada, formato jpg
//  *         in: formData
//  *         required: true
//  *     responses:
//  *       200:
//  *         description: Status da operação
//  *         schema:
//  *           $ref: '#/definitions/Entregador'  */

/**
 * @swagger
 * /entregadores/upload:
 *   post:
 *     tags:
 *       - Entregador
 *     description: Enviar fotos do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: file
 *         type: file
 *         description: Imagem a ser enviada, formato jpg, nome deve ter o cpf do entregador+tipo da foto(cpf,rg,crv,foto) com compressao 
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Upload de imagens
router.post('/upload', function (req, res) {
  var path = req.headers.host + "/entregadores/images/";
  uploadUtils.handleUpload(req, res, path);
});

/**
 * @swagger
 * /entregadores/upload/base64:
 *   post:
 *     tags:
 *       - Entregador
 *     description: Enviar fotos do entregador em formato string base64
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: fileString
 *         type: string
 *         description: Imagem a ser enviada, formato string.
 *         in: formData
 *         required: true
 *       - name: imageName
 *         type: string
 *         description: Nome da imagem deve ter o cpf do entregador+tipo da foto(cpf,rg,crv,foto).
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

var multer = require('multer');
var upload = multer(
    { 
        limits: {
            fieldNameSize: 999999999,
            fieldSize: 999999999
        },
        dest: 'entregadores/images/' }
    );


router.post('/upload/base64', upload.any(), function (req, res) {

    var result;
    var msg;
    // var path = req.headers.host + '/entregadores/images/';

    //var target_path = req.headers.host + "/entregadores/images/"+req.files[0].originalname;
    try{

      var target_path = req.files[0].path+path.extname(req.files[0].originalname);
      var tmp_path = req.files[0].path;
      console.log(tmp_path);
      var src = fs.createReadStream(tmp_path);
      var dest = fs.createWriteStream(target_path);
      src.pipe(dest);
      src.on('end', function() {
        fs.unlink(tmp_path, function(){});
        console.log('finish ok');
        result = true;
        msg = "1";
      });
      src.on('error', function(err) {
        console.log('finish error');
        result = false;
        msg = "0";
      });
      console.log('before loop');
      while(result === undefined){
        require('deasync').runLoopOnce();
      };

      console.log('after loop');

      return res
        .status(200)
        .json({msg:msg,url:target_path});

    }catch (e){
        console.trace(e);
		return res
        .status(401)
        .json({msg:'error'});
    }

});
/**
 * @swagger
 * /entregadores/images/{fileName}:
 *   get:
 *     tags:
 *       - Entregador
 *     description: Buscar imagem
 *     produces:
 *       - image/jpg
 *     parameters:
 *       - name: fileName
 *         type: string
 *         description: nome do arquivo com extensao jpg, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Imagem
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Download
router.get('/images/:fileName', passport.authenticate('jwt', {session: false}), function (req, res) {
  if (!req.params.fileName) {
    return res
      .status(400)
      .json({success: false, "message": "fileName.was.not.sent", "code": 63});
  }
  uploadUtils.getFile(req, res, req.params.fileName);
});

// __________________________________________ ESTATÍSTICAS
// _____________________________________

/**
 * @swagger
 * /entregadores/{idEntregador}/stats/vendas/{period}:
 *   get:
 *     tags:
 *       - Entregador
 *     description: Estatísticas das vendas do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *       - name: period
 *         type: string
 *         description: Periodo de pesquisa - MES, SEM
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: Dados das vendas
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */
//Dados das vendas do entregador
router.get('/:idEntregador/stats/vendas/:period', roles.checkRole("usuario", "idUsuario"), function (req, res) {
  if (!req.params.idEntregador) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador  was not sent", "code": 63});
  }

  //Periodo de selecao
  var startDate = new Date();
  if (req.params.period == 'MES') {
    startDate = new Date(new Date() - 30 * (24 * 60 * 60 * 1000)); //Mes
  } else if (req.params.period == 'SEM') {
    startDate = new Date(new Date() - 7 * (24 * 60 * 60 * 1000))   //Semana
  }else{
    startDate = new Date(new Date() -  (24 * 60 * 60 * 1000))    //Ultimas 24 horas
  }

  // const query = 'SELECT COUNT(idVenda) AS numVendas FROM "Venda"  WHERE
  // entregadorIdEntregador = :idEntregador'
  var params = {
    attributes: [
      'idVenda',
      [
        models
          .sequelize
          .fn('COUNT', models.sequelize.col('idVenda')),
        'numVendas'
      ]
    ],
    where: {
      entregadorIdEntregador: req.params.idEntregador,
      createdAt: {
        $lt: new Date(),
        $gt: startDate
      }
    },
    group: ['idVenda'],
    raw: true
  }

  models
    .venda
    .get(params, function (data, error) {
      if (error) {
        return res
          .status(500)
          .json(errorUtil.jsonFromError(error, "get.entregador.stats.vendas.error", 500));
      }
      return res
        .status(200)
        .json(data);
    })

})

/**
 * @swagger
 * /entregadores/{idEntregador}/stats/avaliacao:
 *   get:
 *     tags:
 *       - Entregador
 *     description: Estatísticas da avaliação do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: Dados das vendas
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */
//Dados da avaliacao do entregador
router.get('/:idEntregador/stats/avaliacao', roles.checkRole("usuario", "idUsuario"), function (req, res) {
  if (!req.params.idEntregador) {
    return res
      .status(400)
      .json({success: false, "message": "idEntregador  was not sent", "code": 63});
  }

  var params = {
    attributes: [
      'entregadorIdEntregador',
      [
        models
          .sequelize
          .fn('AVG', models.sequelize.col('estrelas')),
        'mediaAval'
      ]
    ],
    where: {
      entregadorIdEntregador: req.params.idEntregador,
    },
    group: ['entregadorIdEntregador'],
    raw: true
  }

  models
    .avaliacao
    .get(params, function (data, error) {
      if (error) {
        return res
          .status(500)
          .json(errorUtil.jsonFromError(error, "get.entregador.stats.avaliacao.error", 500));
      }
      return res
        .status(200)
        .json(data);
    })

})

module.exports = router;
