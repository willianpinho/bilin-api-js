"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var errorUtil = require('../utils/errorUtils');
var roles = require('../utils/roles');



/**
 * @swagger
 * definitions:
 *   Mensagem:
 *     properties:
 *       idMensagem:
 *         type: integer
 *       mensagem:
 *         type: string
 *       ativo:
 *         type: boolean
 */

/**
 * @swagger
 * /mensagens/usuarios/{idUsuario}/pedidos/{idPedido}:
 *   post:
 *     tags:
 *       - Mensagem
 *     description: Mensagem do usuário em relação a um pedido
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: mensagem
 *         type: string
 *         description: Mensagem escrita
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador da mensagem
 *         in: formData
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido da mensagem
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da mensagem
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Mensagem'
 */

router.post('/usuarios/:idUsuario/pedidos/:idPedido', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.body.idEntregador || !req.params.idPedido || !req.params.idUsuario) {
        return res.status(400).json({ success: false, "message": "idEntregador.or.idPedido.idUsuario.was.not.sent", "code": 63 });
    }
    var params = {
        mensagem: req.body.mensagem,
        usuarioIdUsuario: req.params.idUsuario,
        entregadorIdEntregador: req.body.idEntregador,
        from: "usuario",
        pedidoIdPedido: req.params.idPedido
    }
    models.mensagem.insert(params, function (mensagem, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "create.mensagem.pedido.usuario.error", 400));
        }
        return res.status(200).json(mensagem);
    })
});

/**
 * @swagger
 * /mensagens/entregadores/{idEntregador}/pedidos/{idPedido}:
 *   post:
 *     tags:
 *       - Mensagem
 *     description: Mensagem do entregador em relação a um pedido 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: mensagem
 *         type: string
 *         description: Mensagem escrita
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: formData
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador da mensagem
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido da mensagem
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da mensagem
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Mensagem'
 */

router.post('/entregadores/:idEntregador/pedidos/:idPedido', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.body.idUsuario || !req.params.idPedido || !req.params.idEntregador) {
        return res.status(400).json({ success: false, "message": "idUsuario.or.idPedido.or.idEntregador.was.not.sent", "code": 63 });
    }
    var params = {
        mensagem: req.body.mensagem,
        usuarioIdUsuario: req.body.idUsuario,
        entregadorIdEntregador: req.params.idEntregador,
        from: "entregador",
        pedidoIdPedido: req.params.idPedido
    }
    models.mensagem.insert(params, function (mensagem, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "create.mensagem.pedido.entregador.error", 400));
        }
        return res.status(200).json(mensagem);
    })
});


/**
 * @swagger
 * /mensagens/usuarios/{idUsuario}/pedidos/{idPedido}:
 *   get:
 *     tags:
 *       - Mensagem
 *     description: Buscar mensagens de um pedido, (Usuário)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido da mensagem
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10, o Limite é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados da avaliacao do usuário
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */

router.get('/usuarios/:idUsuario/pedidos/:idPedido', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.params.idPedido || !req.params.idUsuario) {
        return res.status(400).json({ success: false, "message": "idEntregador.or.idPedido.or.idUsuario.was.not.sent", "code": 63 });
    }

    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var parameters = {
        attributes : ['idMensagem','mensagem','from','createdAt'],
        where: {
            pedidoIdPedido: req.params.idPedido,
            usuarioIdUsuario : req.params.idUsuario 
        },
        offset: offset,
        limit: limit,
        order: [['createdAt', 'ASC']]
    }

    models.mensagem.getAll(parameters, function (mensagens, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.mensagens.pedido.usuario.error", 500));
        }
        return res.status(200).json(mensagens);
    })
});



/**
 * @swagger
 * /mensagens/entregadores/{idEntregador}/pedidos/{idPedido}:
 *   get:
 *     tags:
 *       - Mensagem
 *     description: Buscar mensagens de uma pedido, (Entregador)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido da mensagem
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10, o Limite é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados da avaliacao do usuário
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */

router.get('/entregadores/:idEntregador/pedidos/:idPedido', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idPedido || !req.params.idEntregador) {
        return res.status(400).json({ success: false, "message": "idUsuario.or.idPedido.or.idEntregador.was.not.sent", "code": 63 });
    }

    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var parameters = {
        attributes : ['idMensagem','mensagem','from','createdAt'],
        where: {
            pedidoIdPedido: req.params.idPedido,
            entregadorIdEntregador : req.params.idEntregador    
        },
        offset: offset,
        limit: limit,
        order: [['createdAt', 'ASC']]
    }

    models.mensagem.getAll(parameters, function (mensagens, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.mensagens.pedido.entregador.error", 500));
        }
        return res.status(200).json(mensagens);
    })
});


module.exports = router;
