"use strict";


//Rotas para a distribuidora

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var roles = require('../utils/roles');
var emailUtils = require('../utils/emails');
var errorUtil = require('../utils/errorUtils');
var nconf = require('nconf');
var path = require("path");
var env = process.env.NODE_ENV || "development";
var config = path.join(__dirname, 'config.json');
nconf.argv().env();
nconf.file({ file: config });


/**
 * @swagger
 * definitions:
 *   Distribuidora:
 *     properties:
 *       idDistribuidora:
 *         type: integer
 *       nome:
 *         type: string
 *       cnpj:
 *         type: string
 *       imagemUrl:
 *         type: string
 *       ativo:
 *         type: boolean
 */


//Gerar Token de Autenticação
var generateToken = function(res, user) {
    var expiresIn = (1*(60 * 60)); //Valido por uma hora //Valido por uma hora
    var currentDate = new Date();
    var expiresDate = new Date(currentDate.getTime() + expiresIn);
    var payload = {
        id: user.dataValues.idDistribuidora,
        mail: user.dataValues.email,
        role: user.dataValues.role
    }

    //Retornar o token
    var token = jwt.sign(payload, nconf.get('secret'), { // Gerando um token baseado nos dados do usuário
        expiresIn: expiresIn // in seconds
    });
    delete user.dataValues["senha"];
    res.status(200).json({ success: true, expires: expiresDate, token: 'JWT ' + token, user: user.dataValues });
}






/**
 * @swagger
 * /distribuidoras/auth:
 *   post:
 *     tags:
 *       - Distribuidora
 *     description: Autenticar Distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         type: string
 *         description: Email obrigatório
 *         in: formData
 *         required: true
 *       - name: senha
 *         type: string
 *         description: Senha obrigatório
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da Distribuidora, token JWT
 *       401:
 *         description: Usuário ou senha incorretas
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Autenticação da distribuidora
router.post('/auth', function(req, res) {
   if (!req.body.email || !req.body.senha) {
    return res.status(400).json({ success: false, "message": "email.or.password.not.sent", "code": 66 });
   }
    var parameters = {
        attributes: ['idDistribuidora', 'nome', 'email', 'senha', 'role', 'ativo'],
        where: {
            email: req.body.email
        }
    }
    models.distribuidora.get(parameters, function(user, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "unable.to.auth", 500));
        }
        if (user) {
            if (user.ativo == false) {
                return res.status(401).json({ success: false, title: "inactive.distribuidora", message: "inactive.distribuidora", "code": 160 });
            }
            if (user.validatePassword(req.body.senha)) {
                generateToken(res, user);
            } else {
                return res.status(401).json({ success: false, title: "unable.to.login", message: "incorrect.username.password", "code": 52 });
            }
        } else {
            return res.status(401).json({ success: false, title: "distribuidora.not.found", message: "distribuidora.not.found", "code": 50 });
        }
    })
});




/**
 * @swagger
 * /distribuidoras/{idDistribuidora}:
 *   put:
 *     tags:
 *       - Distribuidora
 *     description: Alterar dados da  Distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: ativo
 *         type: boolean
 *         description: Status da conta da distribuidora
 *         in: formData
 *         required: false
 *       - name: nome
 *         type: string
 *         description: Nome da distribuidora
 *         in: formData
 *         required: false
 *       - name: senha
 *         type: string
 *         description: Senha da distribuidora, obrigatório
 *         in: formData
 *         required: false
 *       - name: imagemUrl
 *         type: string
 *         description: URL da imagem da distribuidora
 *         in: formData
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */
//Alterar uma distribuidora
router.put('/:idDistribuidora', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var updateFields = {};

    //Se o usuario informou o nome
    if (req.body.nome) {
        updateFields['nome'] = req.body.nome;
    }

    //Se o usuario informou o status
    if (req.body.ativo) {
        updateFields['ativo'] = req.body.ativo;
    }
    //Se o usuario informou a imagemUrl
    if (req.body.imagemUrl) {
        updateFields['imagemUrl'] = req.body.imagemUrl;
    }

   //Se o usuario quiser atualizar a senha
   if(req.body.senha){
     var pass = models.distribuidora.generateHash(req.body.senha);
     updateFields['senha'] = pass;
   }
    models.distribuidora.updateData({ idDistribuidora: req.params.idDistribuidora }, updateFields, function(data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "update.distribuidora.error", 400));
        }
        return res.status(200).json(data);
    })

});






/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/entregadores:
 *   post:
 *     tags:
 *       - Distribuidora
 *     description: Vincular Entregador a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora obrigatório
 *         in: path
 *         required: true
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador obrigatório
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Vincular entregador a distribuidora
router.post('/:idDistribuidora/entregadores', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora || !req.body.idEntregador) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idEntregador.was.not.sent", "code": 63 });
    }
    models.entregador.updateData({ idEntregador: req.body.idEntregador }, { distribuidoraIdDistribuidora: req.params.idDistribuidora }, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "set.distribuidora.entregador.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/entregadores/{idEntregador}:
 *   delete:
 *     tags:
 *       - Distribuidora
 *     description: Desvincular Entregador a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora obrigatório
 *         in: path
 *         required: true
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       204:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Desvincular entregador a distribuidora
router.post('/:idDistribuidora/entregadores/:idEntregador', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora || !req.params.idEntregador) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idEntregador.was.not.sent", "code": 63 });
    }
    models.entregador.updateData({ idEntregador: req.body.idEntregador }, { distribuidoraIdDistribuidora: null }, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "distribuidora.unset.entregador.error", 500));
        }
        return res.status(204).json(data);
    })
});


/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/endereco:
 *   post:
 *     tags:
 *       - Distribuidora
 *     description: Cadastrar endereço do da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idCidade
 *         type: string
 *         description: idCidade da Cidade do Endereço
 *         in: formData
 *         required: true
 *       - name: titulo
 *         type: string
 *         description: Titulo do endereço
 *         in: formData
 *         required: true
 *       - name: endereco
 *         type: string
 *         description: Endereço 
 *         in: formData
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Numero da rua, se houver.
 *         in: formData
 *         required: false
 *       - name: complemento
 *         type: string
 *         description: Complemento do endereço
 *         in: formData
 *         required: false
 *       - name: bairro
 *         type: string
 *         description: Bairro do endereço
 *         in: formData
 *         required: true
 *       - name: cep
 *         type: string
 *         description: CEP do Endereço, Ex - 70130-070
 *         in: formData
 *         required: true
 *       - name: referencia
 *         type: string
 *         description: Referência do Endereço
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do endereço cadastrado
 *       400:
 *         description: Faltaram parametros na requisição, ou houve falha na validação
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Cadastrar endereco da distribuidora
router.post('/:idDistribuidora/endereco', roles.checkRole("dist", "idDistribuidora"), function(req, res, next) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    if (!req.body.idCidade) {
        return res.status(400).json({ success: false, "message": "idCidade.was.not.sent", "code": 63 });
    }
    var parameters = {
        titulo: req.body.titulo,
        endereco: req.body.endereco,
        bairro: req.body.bairro,
        cep: req.body.cep,
        referencia: req.body.referencia,
        cidadeID: req.body.idCidade,
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        numero : req.body.numero,
        complemento : req.body.complemento,
        dono: "distribuidora"
    }

    models.endereco.insert(parameters, function(data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "insert.endereco.error", 400));
        }
        return res.status(200).json(data);
    })

});

/**
* @swagger
* /distribuidoras/{idDistribuidora}/endereco/{idEndereco}:
*   put:
*     tags:
*       - Distribuidora
*     description: Alterar endereço da distribuidora
*     produces:
*       - application/json
*     parameters:
*       - name: idDistribuidora
*         type: string
*         description: idDistribuidora, obrigatório
*         in: path
*         required: true
*       - name: idEndereco
*         type: string
*         description: idEndereco do endereco a ser alterado
*         in: path
*         required: true
*       - name: Authorization
*         type: string
*         description: Token de Autenticação
*         in: header
*         required: true
*       - name: idCidade
*         type: string
*         description: idCidade da Cidade do Endereço
*         in: formData
*         required: false
*       - name: titulo
*         type: string
*         description: Titulo do endereço
*         in: formData
*         required: false
*       - name: endereco
*         type: string
*         description: Endereço 
*         in: formData
*         required: false
*       - name: numero
*         type: string
*         description: Numero da rua, se houver.
*         in: formData
*         required: false
*       - name: complemento
*         type: string
*         description: Complemento do endereço
*         in: formData
*         required: false
*       - name: bairro
*         type: string
*         description: Bairro do endereço
*         in: formData
*         required: false
*       - name: cep
*         type: string
*         description: CEP do Endereço, Ex - 70130-070
*         in: formData
*         required: false
*       - name: referencia
*         type: string
*         description: Referência do Endereço
*         in: formData
*         required: false
*     responses:
*       200:
*         description: Status da operação
*       400:
*         description: Faltaram parametros na requisição
*       401:
*         description: Token Inválido
*       403:
*         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
*       500:
*         description: Erro interno do servidor
*         schema:
*           $ref: '#/definitions/Endereco'
*/

//Alterar endereco da distribuidora
router.put('/:idDistribuidora/endereco/:idEndereco', roles.checkRole("dist", "idDistribuidora"), function(req, res, next) {
    if (!req.params.idDistribuidora || !req.params.idEndereco) {
        return res.status(400).json({ success: false, "message": "idDistribuidora or idEndereco was not sent", "code": 63 });
    }
    var updateFields = {};
    //Se o titulo foi informado
    if (req.body.titulo) {
        updateFields['titulo'] = req.body.titulo;
    }
    //Se o endereco foi passado
    if (req.body.endereco) {
        updateFields['endereco'] = req.body.endereco;
    }
    //Se o bairro foi passado
    if (req.body.bairro) {
        updateFields['bairro'] = req.body.bairro;
    }

    //Se o numero foi passado
    if (req.body.numero) {
     updateFields['numero'] = req.body.numero;
    }

   //Se o complemento foi passado
   if (req.body.complemento) {
     updateFields['complemento'] = req.body.complemento;
   }
    //Se a cidade foi informada
    if (req.body.idCidade) {
        updateFields['cidadeID'] = req.body.idCidade;
    }
    //Se o cep foi passado
    if (req.body.cep) {
        updateFields['cep'] = req.body.cep;
    }
    //Se a referencia foi passada
    if (req.body.referencia) {
        updateFields['referencia'] = req.body.referencia;
    }

    var whereParams = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        idEndereco: req.params.idEndereco,
        ativo: true
    }
    models.endereco.updateData(whereParams, updateFields, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "update.endereco.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/telefones:
 *   post:
 *     tags:
 *       - Distribuidora
 *     description: Cadastrar telefone da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Número do telefone com ddd
 *         in: formData
 *         required: true
 *       - name: descricao
 *         type: string
 *         description: descricao do telefone
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Dados do telefone cadastrado
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */
//Adicionar telefone da distribuidora
router.post('/:idDistribuidora/telefones', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var parameters = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        numero: req.body.numero,
        descricao: req.body.descricao
    }
    models.telefone.insert(parameters, function(data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "insert.telefone.distribuidora.error", 400));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/telefones/{idTelefone}:
 *   put:
 *     tags:
 *       - Distribuidora
 *     description: Atualizar dados de um telefone da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: idTelefone
 *         type: string
 *         description: idTelefone, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: numero
 *         type: string
 *         description: Número do telefone com ddd
 *         in: formData
 *         required: true
 *       - name: descricao
 *         type: string
 *         description: descricao do telefone
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Status da atualizacao
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

//Alterar telefone
router.put('/:idDistribuidora/telefones/:idTelefone', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora || !req.params.idTelefone) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idTelefone.was.not.sent", "code": 63 });
    }
    //Atualizando dados da distribuidora
    var whereParams = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        idTelefone: req.params.idTelefone
    }
    var updateFields = {};

    //Atualizar o numero
    if (req.body.numero) {
        updateFields['numero'] = req.body.numero;
    }
    //Atualizar a descricao
    if (req.body.descricao) {
        updateFields['descricao'] = req.body.descricao;
    }
    models.telefone.updateData(whereParams, updateFields, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "update.telefone.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    })
});


/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/telefones/{idTelefone}:
 *   delete:
 *     tags:
 *       - Distribuidora
 *     description: Deletar dados de um telefone da distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: idTelefone
 *         type: string
 *         description: idTelefone, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       204:
 *         description: Status da atualizacao
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Telefone'
 */

//Deletar telefone da distribuidora
router.delete('/telefones/:idEntregador/:idTelefone', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora || !req.params.idTelefone) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.or.idTelefone.was.not.sent", "code": 63 });
    }
    //Atualizando dados do user
    var whereParams = {
        distribuidoraIdDistribuidora: req.params.idDistribuidora,
        idTelefone: req.params.idTelefone
    }
    var updateFields = {};
    updateFields['ativo'] = false;
   
    models.telefone.updateData(whereParams, updateFields, function(data, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "delete.telefone.distribuidora.error", 400));
        }
        return res.status(204).json(data);
    })
});


/**
 * @swagger
 * /distribuidoras/{cnpj}/entregadores:
 *   get:
 *     tags:
 *       - Distribuidora
 *     description: Buscar Entregadores vinculados a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cnpj
 *         type: string
 *         description: CNPJ da distribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da distribuidora cadastrada
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */

//Buscar entregadores da distribuidora
router.get('/:cnpj/entregadores', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.cnpj) {
        return res.status(400).json({ success: false, "message": "cnpj.was.not.sent", "code": 63 });
    }

    var params = {
        where : {
          cnpj: req.params.cnpj,
        },
        include: [{
            required : false,
            model: models.entregador,
            attributes: ['nome', 'imagemUrl', 'idEntregador', 'email', 'cpf']
        }]
    }

    models.distribuidora.getAll(params, function(user, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "get.distribuidora.entregadores.error", 400));
        }
        return res.status(200).json(user);
    })
});

/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/pedidos:
 *   get:
 *     tags:
 *       - Distribuidora
 *     description: Buscar Pedidos vinculados a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: dataInicio
 *         type: string
 *         description: data de inicio, (Formato Iso) - DEFAULT ONTEM
 *         in: query
 *         required: false
 *       - name: dataFim
 *         type: string
 *         description: data do fim, (Formato Iso) - DEFAULT HOJE
 *         in: query
 *         required: false
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10 o Limite é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados dos pedidos
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */
//Buscar pedidos da distribuidora
router.get('/:idDistribuidora/pedidos', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var startDate = Date.parse(req.query.dataInicio) || Date(new Date() - 24 * 60 * 60 * 1000);
    var endDate = Date.parse(req.query.dataFim) || new Date();
    var params = {
        where: {
            distribuidoraIdDistribuidora: req.params.idDistribuidora,
              createdAt: {
                $lt: endDate,
                $gt: startDate
            },
        },
        include: [
            {
                model: models.endereco,
                required: false,
                attributes: [
                    'endereco', 'bairro', 'cep', 'referencia'
                ],
                include: [
                    {
                        model: models.cidade,
                        attributes: [
                            'nome', 'estadoID'
                        ],
                        required: false,
                        include: [
                            {
                                model: models.estado,
                                attributes: [
                                    'nome', 'acronimo'
                                ],
                                required: false
                            }
                        ]
                    }
                ]
            }, {
                model: models.entregador,
                attributes: ['nome', 'imagemUrl'],
                include : [{
                    model : models.entregadorData,
                    attributes : ['point'],
                    required : false
                }]
            },{
              model: models.usuario,
              attributes : ['nome','idUsuario','email','imagemUrl'],
              required : false
            }
            
        ],
        offset: offset,
        limit: limit
    }
    models.pedido.getAll(params, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.pedidos.distribuidora.error", 500));
        }
        return res.status(200).json(data);
    });
})

/**
 * @swagger
 * /distribuidoras/{idDistribuidora}/vendas:
 *   get:
 *     tags:
 *       - Distribuidora
 *     description: Buscar Vendas vinculadas a uma distribuidora
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idDistribuidora
 *         type: string
 *         description: idDistribuidora, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: dataInicio
 *         type: string
 *         description: data de inicio, (Formato Iso) - DEFAULT ONTEM
 *         in: query
 *         required: false
 *       - name: dataFim
 *         type: string
 *         description: data do fim, (Formato Iso) - DEFAULT HOJE
 *         in: query
 *         required: false
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10 o máximo é 50.
 *         in: query
 *         required: false
 *     responses:
 *       200:
 *         description: Dados das vendas
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Distribuidora'
 */
//Buscar vendas da distribuidora
router.get('/:idDistribuidora/vendas', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idDistribuidora) {
        return res.status(400).json({ success: false, "message": "idDistribuidora.was.not.sent", "code": 63 });
    }
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var startDate = Date.parse(req.query.dataInicio) || Date(new Date() - 24 * 60 * 60 * 1000);
    var endDate = Date.parse(req.query.dataFim) || new Date();
    var params = {
        where: {
            distribuidoraIdDistribuidora: req.params.idDistribuidora,
              createdAt: {
                $lt: endDate,
                $gt: startDate
            },
        },
         include :[{model: models.entregador,
                attributes: ['nome', 'imagemUrl'],
                include : [{
                    model : models.entregadorData,
                    attributes : ['point'],
                    required : false
                }]}],
        offset: offset,
        limit: limit
    }
    models.venda.getAll(params, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.vendas.distribuidora.error", 500));
        }
         return res.status(200).json(data);
    });
})


//____________________________________ ENTREGADOR ____________________________________________________________

/**
 * @swagger
 * /distribuidoras/entregadores:
 *   get:
 *     tags:
 *       - Distribuidora
 *     description: Buscar dados de entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cpf
 *         type: integer
 *         description: CPF do entregador, obrigatório
 *         in: query
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados do entregador
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Buscar dados do entregador pelo cpf

router.get('/entregadores', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.query.cpf) {
        return res.status(400).json({ success: false, "message": "CPF.was.not.sent", "code": 63 });
    }
    var params = {
        where :{
          cpf: req.query.cpf
        }
    }

    models.entregador.get(params, function(entregador, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.entregador.error", 500));
        }
        return res.status(200).json(entregador);
    })

});

/**
 * @swagger
 * /distribuidoras/entregadores/{idEntregador}/activate:
 *   post:
 *     tags:
 *       - Distribuidora
 *     description: Ativar conta do entregador
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: string
 *         description: idEntregador, obrigatório
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Status da operação
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Entregador'
 */

//Ativar conta do entregador
router.post('/entregadores/:idEntregador/activate', roles.checkRole("dist", "idDistribuidora"), function(req, res) {
    if (!req.params.idEntregador || req.body.ativo) {
        return res.status(400).json({ success: false, "message": "idEntregador.or.ativo.was.not.sent", "code": 63 });
    }
    models.entregador.activateEntregador(req.params.idEntregador, function(data, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "activate.entregador.error", 500));
        }
        //Se tudo certo, vamos enviar um email para o entregador
        models.entregador.get({ attributes: ['email', 'nome'], where: { idEntregador: req.params.idEntregador } }, function(data, error) {
            if (error) {
                return res.status(500).json(errorUtil.jsonFromError(error, "activate.entregador.error", 500));
            }
            //Enviar email de ativacao
            emailUtils.sendEntregadorActivatedSucessEmail(data.email, data.nome, function(status, error) {
                if (error) {
                    return res.status(200).json({ success: false, "message": "entregador.activated.email.was.sent" });
                }
                return res.status(200).json({ success: true, "message": " email.was.sent" });
            })
        })
    })
});




module.exports = router;