"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');


/**
 * @swagger
 * definitions:
 *   Estado:
 *     properties:
 *       nome:
 *         type: string
 *       acronimo:
 *         type: string
 *       ID:
 *         type: integer
 */

/**
 * @swagger
 * definitions:
 *   Cidade:
 *     properties:
 *       nome:
 *         type: string
 *       idEstado:
 *         type: integer
 *       ID:
 *         type: integer
 */


/**
 * @swagger
 * /estados:
 *   get:
 *     tags:
 *       - Estado
 *     description: Listar os estados brasileiros
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: Dados dos estados
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Estado'
 */

router.get('/',function(req,res){
  models.estado
    .findAll({
      attributes: ['ID', 'nome','acronimo'],
  }).then(function(estados){
     return res.status(200).json(estados);
  }).catch(function(error){
    return res.status(500).json({ success: false, title: "get.estados.error", message: "internal.server.error" });
  });
})

/**
 * @swagger
 * /estados/{idEstado}/autocomplete:
 *   get:
 *     tags:
 *       - Cidade
 *     description: Listar as cidades de um estado
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEstado
 *         type: integer
 *         description: idEstado, obrigatório
 *         in: path
 *         required: true
 *       - name: nome
 *         type: string
 *         description: Nome a ser buscado
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da cidade
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Cidade'
 */

//Listar cidades de um estado - autocomplete

router.get('/:idEstado/autocomplete',function(req,res){
  //Parametros
  var name =  req.query['nome'];
  if(!name || !req.params['idEstado']){
    return res.status(400).json({message:"Query parameters Missing"});
  }
  if(req.params['idEstado'] < 0 || req.params['idEstado'] > 27){
    return res.status(400).json({message:"Invalid idEstado"});
  }
  models.cidade
    .findAll({
      attributes: ['estadoID', 'nome', 'ID'],
      where: {
        nome: {
          $iLike : '%' + name + '%'
        },
        estadoID : req.params['idEstado']
      },
      limit: 10,
  }).then(function(cidades){
     return res.status(200).json(cidades);
  }).catch(function(error){
    return res.status(500).json({ success: false, title: "cidades.autocomplete.error", message: "internal.server.error" });
  });
})

module.exports = router;
