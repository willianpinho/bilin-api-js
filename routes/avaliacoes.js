"use strict";

var express = require('express');
var router = express.Router();
var models = require('../models/rdb');
var errorUtil = require('../utils/errorUtils');
var roles = require('../utils/roles');



//Para atualizar o status da avaliacao do pedido do usuario

function pedidoRateForUsuario(idPedidoUpdate,callback){
    var params = {
        avaliadoUser : true
    }

    var whereParams = {
            idPedido : idPedidoUpdate
    }

    models.pedido.updateData(whereParams,params,function(data,error){
        callback(data,error);
    })
}

//Para atualizar o status da avaliacao do pedido do entregador

function pedidoRateForEntregador(idPedidoUpdate,callback){
    var params = {
        avaliadoEntregador : true
    }

    var whereParams = {
            idPedido : idPedidoUpdate
    }

    models.pedido.updateData(whereParams,params,function(data,error){
        callback(data,error);
    })
}

/**
 * @swagger
 * definitions:
 *   Avaliacao:
 *     properties:
 *       idAvaliacao:
 *         type: integer
 *       avaliacao:
 *         type: string
 *       estrelas:
 *         type: integer
 *       ativo:
 *         type: boolean
 */

/**
 * @swagger
 * /avaliacoes/usuarios/{idUsuario}:
 *   post:
 *     tags:
 *       - Avaliacao
 *     description: O usuário avalia o pedido
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: avaliacao
 *         type: string
 *         description: Avaliacao escrita
 *         in: formData
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador avaliado
 *         in: formData
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido avaliado
 *         in: formData
 *         required: true
 *       - name: estrelas
 *         type: float
 *         description: Quantidade de estrelas da avaliacao
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da avaliacao
 *       400:
 *         description: Faltaram parametros na requisição
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */

router.post('/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res) {

    if (!req.body.idEntregador || !req.params.idUsuario || !req.body.idPedido || !req.body.estrelas) {
        return res.status(400).json({ success: false, "message": "idEntregador or idUsuario or  idPedido or estrelas was not sents", "code": 63 });
    }

    if (req.body.estrelas < 1 || req.body.estrelas > 5) {
        return res.status(400).json({ success: false, "message": "invalid.estrelas.value", "code": 63 });
    }
    // var params = {
    //     avaliacao: req.body.avaliacao,
    //     estrelas: req.body.estrelas,
    //     usuarioIdUsuario: req.params.idUsuario,
    //     entregadorIdEntregador: req.body.idEntregador,
    //     from: "usuario",
    //     pedidoIdPedido: req.body.idPedido
    // }
    // models.avaliacao.insert(params, function (avaliacao, error) {
    //     if (error) {
    //         return res.status(500).json(errorUtil.jsonFromError(error, "create.avaliacao.usuario.error", 500));
    //     }
    //     pedidoRateForUsuario(req.body.idPedido,function(data,error){
    //         return res.status(200).json(avaliacao);
    //     })
    // })

    var params = {
        avaliacao: req.body.avaliacao,
        estrelas: req.body.estrelas,
        usuarioIdUsuario: req.params.idUsuario,
        entregadorIdEntregador: req.body.idEntregador,
        from: "usuario",
        pedidoIdPedido: req.body.idPedido
    }
    var whereParams = {
       usuarioIdUsuario: req.params.idUsuario,
       from: "usuario",
       pedidoIdPedido: req.body.idPedido
    }
    models.avaliacao.getOrCreate(whereParams,params, function (avaliacao, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "create.avaliacao.usuario.error", 500));
        }
        pedidoRateForUsuario(req.body.idPedido,function(data,error){
            return res.status(200).json(avaliacao);
        })
    })
});

/**
 * @swagger
 * /avaliacoes/entregadores/{idEntregador}:
 *   post:
 *     tags:
 *       - Avaliacao
 *     description: O entregador avalia o pedido
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *       - name: avaliacao
 *         type: string
 *         description: Avaliacao escrita
 *         in: formData
 *         required: true
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador
 *         in: path
 *         required: true
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuario avaliado
 *         in: formData
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido avaliado
 *         in: formData
 *         required: true
 *       - name: estrelas
 *         type: integer
 *         description: Quantidade de estrelas da avaliacao
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da avaliacao
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */

router.post('/entregadores/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idEntregador || !req.body.idUsuario || !req.body.idPedido || !req.body.estrelas) {
        return res.status(400).json({ success: false, "message": "idUsuario or idEntregador or  idPedido or estrelas was not sent", "code": 63 });
    }
    if (req.body.estrelas < 1 || req.body.estrelas > 5) {
        return res.status(400).json({ success: false, "message": "invalid.estrelas.value", "code": 63 });
    }
    // var params = {
    //     avaliacao: req.body.avaliacao,
    //     estrelas: req.body.estrelas,
    //     usuarioIdUsuario: req.body.idUsuario,
    //     entregadorIdEntregador: req.params.idEntregador,
    //     from: "entregador",
    //     pedidoIdPedido: req.body.idPedido
    // }
    // models.avaliacao.insert(params, function (avaliacao, error) {
    //     if (error) {
    //         return res.status(400).json(errorUtil.jsonFromError(error, "create.avaliacao.entregador.error", 500));
    //     }
    //     pedidoRateForEntregador(req.body.idPedido,function(data,error){
    //         return res.status(200).json(avaliacao);
    //     })
    // })
    var params = {
        avaliacao: req.body.avaliacao,
        estrelas: req.body.estrelas,
        usuarioIdUsuario: req.body.idUsuario,
        entregadorIdEntregador: req.params.idEntregador,
        from: "entregador",
        pedidoIdPedido: req.body.idPedido
    }

    var whereParams = {
       entregadorIdEntregador: req.params.idEntregador, 
       from: "entregador",
       pedidoIdPedido: req.body.idPedido
    }
    models.avaliacao.getOrCreate(whereParams,params, function (avaliacao, error) {
        if (error) {
            return res.status(400).json(errorUtil.jsonFromError(error, "create.avaliacao.entregador.error", 500));
        }
        pedidoRateForEntregador(req.body.idPedido,function(data,error){
            return res.status(200).json(avaliacao);
        })
    })
});


/**
 * @swagger
 * /avaliacoes/usuarios/{idUsuario}/pedidos/{idPedido}:
 *   get:
 *     tags:
 *       - Avaliacao
 *     description: Buscar Avaliacao que o usuário fez sobre um pedido específico
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido avaliado
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da avaliacao do usuário
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */

router.get('/usuarios/:idUsuario/pedidos/:idPedido', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.params.idUsuario || !req.params.idPedido) {
        return res.status(400).json({ success: false, "message": "idUsuario or idPedido was not sent", "code": 63 });
    }

    var parameters = {
        attributes: ['idAvaliacao', 'avaliacao', 'estrelas', 'createdAt', 'usuarioIdUsuario', 'pedidoIdPedido', 'entregadorIdEntregador'],
        where: {
            usuarioIdUsuario: req.params.idUsuario,
            pedidoIdPedido: req.params.idPedido,
            from: "usuario"
        }
    }

    models.avaliacao.get(parameters, function (avaliacao, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.avaliacao.pedido.user.error", 500));
        }
        return res.status(200).json(avaliacao);
    })
});

/**
 * @swagger
 * /avaliacoes/entregadores/{idEntregador}/pedidos/{idPedido}:
 *   get:
 *     tags:
 *       - Avaliacao
 *     description: Buscar Avaliacao que um entregador fez sobre um pedido específico
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador 
 *         in: path
 *         required: true
 *       - name: idPedido
 *         type: integer
 *         description: Id do pedido avaliado
 *         in: path
 *         required: true
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da avaliacao do entregador
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */

router.get('/entregadores/:idEntregador/pedidos/:idPedido', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idEntregador || !req.params.idPedido) {
        return res.status(400).json({ success: false, "message": "idEntregador or idPedido was not sent", "code": 63 });
    }
    var parameters = {
        attributes: ['idAvaliacao', 'avaliacao', 'estrelas', 'createdAt', 'usuarioIdUsuario', 'pedidoIdPedido', 'entregadorIdEntregador'],
        where: {
            entregadorIdEntregador: req.params.idEntregador,
            pedidoIdPedido: req.params.idPedido,
            from: "entregador"
        }
    }

    models.avaliacao.get(parameters, function (avaliacao, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.avaliacao.pedido.entregador.error", 500));
        }
        return res.status(200).json(avaliacao);
    })
});

/**
 * @swagger
 * /avaliacoes/entregadores/{idEntregador}:
 *   get:
 *     tags:
 *       - Avaliacao
 *     description: Buscar Avaliacões sobre um entregador, com paginacāo
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idEntregador
 *         type: integer
 *         description: Id do entregador 
 *         in: path
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10 o Limite é 50.
 *         in: query
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Avaliacões do entregador
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */

//Avaliacoes do entregador
router.get('/entregadores/:idEntregador', roles.checkRole("entregador", "idEntregador"), function (req, res) {
    if (!req.params.idEntregador) {
        return res.status(400).json({ success: false, "message": "idEntregador was not sent", "code": 63 });
    }
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var parameters = {
        attributes: ['idAvaliacao', 'avaliacao', 'estrelas', 'createdAt', 'usuarioIdUsuario', 'entregadorIdEntregador'],
        where: {
            entregadorIdEntregador: req.params.idEntregador,
            from: "usuario"
        },
        offset: offset,
        limit: limit,
        order: [['createdAt', 'DESC']],
        include: [{
            model: models.usuario,
            attributes: ['nome', 'email']
        }]
    }
    models.avaliacao.get(parameters, function (avaliacao, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.avaliacoes.entregador.error", 500));
        }
        return res.status(200).json(avaliacao);
    })
});

/**
 * @swagger
 * /avaliacoes/usuarios/{idUsuario}:
 *   get:
 *     tags:
 *       - Avaliacao
 *     description: Buscar Avaliacões sobre um usuário,com paginacāo
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: idUsuario
 *         type: integer
 *         description: Id do usuário
 *         in: path
 *         required: true
 *       - name: offset
 *         type: integer
 *         description: Offset para paginaçāo. o Default é 0
 *         in: query
 *         required: false
 *       - name: limit
 *         type: integer
 *         description: Limite para busca. O Default é 10 o Limite é 50.
 *         in: query
 *         required: false
 *       - name: Authorization
 *         type: string
 *         description: Token de Autenticação
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Dados da avaliacao do usuário
 *       400:
 *         description: Faltaram parametros na requisição, ou erro na validacao
 *       401:
 *         description: Token Inválido
 *       403:
 *         description: Nível de acesso insuficiente, ou acesso a dados de usuário diferente
 *       500:
 *         description: Erro interno do servidor
 *         schema:
 *           $ref: '#/definitions/Avaliacao'
 */
//Avaliacoes do usuário
router.get('/usuarios/:idUsuario', roles.checkRole("usuario", "idUsuario"), function (req, res) {
    if (!req.params.idUsuario) {
        return res.status(400).json({ success: false, "message": "idUsuario was not sent", "code": 63 });
    }
    var offset = parseInt(req.query['offset']) ? parseInt(req.query['offset']) : 0;
    var limit = parseInt(req.query['limit']) ? parseInt(req.query['limit']) : 10;
    if (limit > 50) {
        return res.status(400).json({ success: false, "message": "limit.out.of.bounds", "code": 63 });
    }
    var parameters = {
        attributes: ['idAvaliacao', 'avaliacao', 'estrelas', 'createdAt', 'usuarioIdUsuario', 'entregadorIdEntregador'],
        where: {
            usuarioIdUsuario: req.params.idUsuario,
            from: "entregador"
        },
        offset: offset,
        limit: limit,
        order: [['createdAt', 'DESC']],
        include: [{
            model: models.entregador,
            attributes: ['nome', 'imagemUrl']
        }]
    }
    models.avaliacao.get(parameters, function (avaliacao, error) {
        if (error) {
            return res.status(500).json(errorUtil.jsonFromError(error, "get.avaliacoes.usuario.error", 500));
        }
        return res.status(200).json(avaliacao);
    })
});


module.exports = router;
