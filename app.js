var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swaggerJSDoc = require('swagger-jsdoc');
var cors = require('cors');
var jwt = require('jsonwebtoken');
var passport = require('passport');


var index = require('./routes/index');
var admin = require('./routes/admin');
var avaliacoes = require('./routes/avaliacoes');
var devices = require('./routes/devices');
var distribuidoras = require('./routes/distribuidoras');
var entregadores = require('./routes/entregadores');
var estados = require('./routes/estados');
var mensagens = require('./routes/mensagens');
var pedidos = require('./routes/pedidos');
var produtos = require('./routes/produtos');
var termos  = require('./routes/termos');
var usuarios = require('./routes/usuarios');
var vendas = require('./routes/vendas');

var app = express();

//Incluir cors
app.use(cors());
var rdb = require('./models/rdb');
app.rdb = rdb;

//Passport
app.use(passport.initialize());
require('./config/passport')(passport);

//Swagger
var swaggerDefinition = {
  info: {
    title: 'Bilin API',
    version: '1.0.0',
    description: 'Documentacao da API do Bilin com o Swagger',
  },
  basePath: '/',
};

//Opcoes do swagger docs

var options = {
  // setando a definition
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/*.js'],
};

var swaggerSpec = swaggerJSDoc(options);

//Mostrar o swagger
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/admin', admin);
app.use('/avaliacoes', avaliacoes);
app.use('/devices', devices);
app.use('/distribuidoras', distribuidoras);
app.use('/entregadores', entregadores);
app.use('/estados', estados);
app.use('/mensagens', mensagens);
app.use('/pedidos', pedidos);
app.use('/produtos', produtos);
app.use('/termos', termos);
app.use('/usuarios', usuarios);
app.use('/vendas', vendas);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
